/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.javac.handlers;

import static lombok.core.AST.Kind.*;
import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.javac.Javac.*;
import static lombok.javac.handlers.JavacHandlerUtil.*;
import static lombok.extern.qfs.intern.javac.JavacHandlerUtil.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.sun.source.tree.Tree.Kind;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCIf;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCModifiers;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;

import lombok.ConfigurationKeys;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.core.configuration.IdentifierName;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.LoggingFramework;
import lombok.extern.qfs.intern.LoggingFramework.LogCallArgumentType;
import lombok.extern.qfs.intern.javac.JavacHandlerUtil;
import lombok.javac.Javac;
import lombok.javac.JavacAnnotationHandler;
import lombok.javac.JavacNode;
import lombok.javac.JavacTreeMaker;
import lombok.javac.handlers.JavacHandlerUtil.MemberExistsResult;
import lombok.spi.Provides;

public class HandleQFLog {

	private static Map<Integer, String> annotationCache = new HashMap<Integer, String>();
	private static JavacHandlerUtil.SourceHelper sourceHelper = new JavacHandlerUtil.SourceHelper();

	private HandleQFLog() {
		throw new UnsupportedOperationException();
	}

	public static void processLogAnnotation(LoggingFramework framework,
											AnnotationValues<?> annotation,
											JavacNode annotationNode,
											String loggerTopic,
											boolean ownLogger,
											String[] skipMethods,
											boolean ignoreExistingLogger,
											String loggerTypeName) {
		try {
			JavacNode typeNode = annotationNode.up();
			annotationCache.put(typeNode.hashCode(), annotationNode.get().toString());
			sourceHelper.useSource(((JCCompilationUnit) annotationNode.top().get()).sourcefile);


			lombok.core.AST.Kind nodeKind = typeNode.getKind();
			if(TYPE.equals(nodeKind) || METHOD.equals(nodeKind)) {
				if ("".equals(loggerTypeName)) {
					loggerTypeName = framework.getLoggerTypeName();
				}

				final String logFieldName = addLoggerField(typeNode, annotationNode, loggerTopic, loggerTypeName, ownLogger, ignoreExistingLogger);

				skipMethods = extendSkipMtdNamesFromConfiguration(annotation, annotationNode, skipMethods);
				boolean[] requiresImport = new boolean[] {false};
				traverseSource(framework, typeNode, logFieldName, skipMethods, annotationNode, requiresImport);

				if (requiresImport[0]) {
					JavacHandlerUtil.addImportStatement(framework.getLogTypeName(), typeNode);
				}
			} else {
				annotationNode.addError(annotationNode + " is legal only on types and methods, not on " + nodeKind + " - " + typeNode.getName());
			}
		} finally {
			deleteAnnotationIfNeccessary(annotationNode, framework.getAnnotationClass());
		}
	}

	public static String addLoggerField(final JavacNode typeNode,
										final JavacNode annotationNode,
										final String loggerTopic,
										final String loggerTypeName,
										final boolean ownLogger,
										final boolean ignoreExistingLogger) {
		final String logFieldName = getLogFieldName(annotationNode);
		boolean useStatic = !Boolean.FALSE.equals(annotationNode.getAst().readConfiguration(ConfigurationKeys.LOG_ANY_FIELD_IS_STATIC));

		JavacNode outmostClass = JavacHandlerUtil.getOutmostClass(typeNode);
		boolean isTopLevel = (outmostClass == typeNode);

		JavacNode typeToAddLogger = outmostClass;
		if (ownLogger) {
			typeToAddLogger = JavacHandlerUtil.getEnclosingTypeDeclaration(typeNode);
			isTopLevel = (typeToAddLogger == typeNode);
		}

		final MemberExistsResult fieldExists = fieldExists(logFieldName, typeToAddLogger);

		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
			if (isTopLevel && !ignoreExistingLogger) {
				annotationNode.addWarning("Field '" + logFieldName + "' already exists.");
			}
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			useStatic = useStatic && JavacHandlerUtil.outerClassIsStatic(typeNode);
			boolean usePublic = isInterface(typeToAddLogger);
			createField(typeToAddLogger, annotationNode, logFieldName, loggerTypeName, useStatic, usePublic, loggerTopic);
		}

		return logFieldName;
	}

	private static boolean isInterface(JavacNode typeNode) {
		return (((JCClassDecl) typeNode.get()).mods.flags & Flags.INTERFACE) != 0;
	}

	protected static String getLogFieldName(JavacNode annotationNode) {
		IdentifierName logFieldNameConfig = annotationNode.getAst().readConfiguration(ConfigurationKeys.LOG_ANY_FIELD_NAME);
		String logFieldName = null;
		if (logFieldNameConfig != null) {
			logFieldName = logFieldNameConfig.getName();
		}
		if (logFieldName == null) logFieldName = "logger";
		return logFieldName;
	}

	private static boolean createField(JavacNode typeNode,
									JavacNode source,
									String logFieldName,
									String loggerTypeName,
									boolean useStatic,
									boolean usePublic,
									String loggerTopic) {
		JavacTreeMaker maker = typeNode.getTreeMaker();

		// private static final <loggerType> log = new
		// <loggerType>(<parameter>);
		JCExpression loggerType = chainDotsString(typeNode, loggerTypeName);
		JCExpression loggerInstanceType = chainDotsString(typeNode, loggerTypeName);

		if (loggerTopic == null || loggerTopic.trim().length() == 0) {
			loggerTopic = getFullQualifiedClassName(typeNode);
		}

		JCExpression loggerName = maker.Literal(loggerTopic);

		List<JCExpression> args = null;
		args = List.<JCExpression>of(loggerName);
		JCNewClass newLoggerCall = maker.NewClass(null, List.<JCExpression>nil(), loggerInstanceType, args, null);

		JCModifiers modifiers = maker.Modifiers((usePublic ? Flags.PUBLIC : Flags.PRIVATE) | Flags.FINAL | (useStatic ? Flags.STATIC : 0));
		JCVariableDecl varDef = maker.VarDef(modifiers, typeNode.toName(logFieldName), loggerType, newLoggerCall);
		JCVariableDecl fieldDecl = recursiveSetGeneratedBy(varDef, source);

		if (isRecord(typeNode) && Javac.getJavaCompilerVersion() < 16) {
			// This is a workaround for https://bugs.openjdk.java.net/browse/JDK-8243057

			injectField(typeNode, fieldDecl);
		} else {
			injectFieldAndMarkGenerated(typeNode, fieldDecl);
		}

		return true;
	}

	public static String getFullQualifiedClassName(JavacNode typeNode) {
		return new DynamicWrapping(typeNode, null).getFullQulifiedNameBuilder().toString();
	}

	protected static String[] extendSkipMtdNamesFromConfiguration(AnnotationValues<? extends java.lang.annotation.Annotation> annotation, JavacNode annotationNode, String[] skipMethods) {
		if (skipMethods == null) {
			return null;
		}

		if (skipMethods.length == 0) {
			java.util.List<String> defaultSkipMethodList = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.LOG_QFLOG_DEFAULT_SKIP_METHODS);
			if (defaultSkipMethodList == null) {
				skipMethods = new String[0];
			} else {
				skipMethods = defaultSkipMethodList.toArray(new String[defaultSkipMethodList.size()]);
			}
		}
		return skipMethods;
	}

	protected static void addMethodLogCall(LoggingFramework framework, MethodContext methodContext, final JavacNode annotationNode, final boolean[] requiresImport) {

		JCMethodDecl declaration = methodContext.methodDeclaration;

		String logFieldName;
		logFieldName = getLogFieldName(annotationNode);

		if (declaration.body == null) {
			return;
		}

		requiresImport[0] = true;

		JCStatement logCall = null;
		try {
			JCStatement methodLogCall = generateMtdLogCall(framework, annotationNode.getTreeMaker(), logFieldName, methodContext, annotationNode);
			SimplifiedJavacNode methodNode = new SimplifiedJavacNode(declaration,annotationNode);
			logCall = simplifiedRecursiveSetGeneratedBy(methodLogCall, methodNode);
		} catch (Exception e) {
			annotationNode.addWarning("Exception " + e + " while generating log call for " + declaration);
			return;
		}

		List<JCStatement> statements = declaration.body.stats;

		List<JCStatement> tail = statements;
		List<JCStatement> head = List.nil();
		for (JCStatement stat : statements) {
			if (isConstructorCall(stat)) {
				tail = tail.tail;
				head = head.prepend(stat);
				continue;
			}
			break;
		}

		List<JCStatement> newList = tail.prepend(logCall);
		for (JCStatement stat : head)
			newList = newList.prepend(stat);
		declaration.body.stats = newList;
		annotationNode.getAst().setChanged();
	}

	private static boolean isInSkipMethods(MethodContext methodContext, String[] skipMethods) {
		if (skipMethods == null) {
			return false;
		}

		String methodSignature = methodContext.getMethodSignature();

		for (final String skipMethod : skipMethods) {
			if (skipMethod.startsWith("%") && skipMethod.length() > 1) {
				if (methodSignature != null && methodSignature.matches(skipMethod.substring(1))) {
					return true;
				}
			} else if (skipMethod.equals(methodSignature)) {
				return true;
			}
		}
		return false;
	}

	private static boolean hasSkipMtdAnnotationTrue(LoggingFramework framework, JCTree tree, JavacNode anyNode) {
		return hasSkipMtdAnnotationTrue(JavacHandlerUtil.getAnnotationString(tree, framework.getAnnotationClass(), anyNode));
	}

	private static boolean hasSkipMtdAnnotationTrue(final String annotationString) {
		return annotationString != null && annotationString.indexOf("skipMtd = true") > -1; // quick
																							// fix
	}

	private static boolean hasSkipMtdAnnotationFalse(LoggingFramework framework, JCTree tree, JavacNode anyNode) {
		return hasSkipMtdAnnotationFalse(JavacHandlerUtil.getAnnotationString(tree, framework.getAnnotationClass(), anyNode));
	}

	private static boolean hasSkipMtdAnnotationFalse(final String annotationString) {
		return annotationString != null && annotationString.indexOf("skipMtd = false") > -1; // quick
																								// fix
	}

	protected static boolean hasLogAnnotation(LoggingFramework framework, JCTree tree, final JavacNode anyNode) {
		return JavacHandlerUtil.hasAnnotation(tree, framework.getAnnotationClass(), anyNode);
	}

	public static JCStatement generateMtdLogCall(LoggingFramework framework,
												final JavacTreeMaker maker,
												String logFieldName,
												MethodContext methodContext,
												JavacNode source) {

		final JCMethodDecl declaration = methodContext.methodDeclaration;
		final String methodSignature = methodContext.getMethodSignature();

		JCExpression levelExpression = chainDotsString(source, framework.getMtdLevelConstant());

		LogCallArgumentProvider logCallArgumentProvider = new MethodDeclBasedLogCallArgumentProvider(maker, declaration);

		final int startPosition = declaration.getPreferredPosition();
		return generateQFLogCall(framework, source, startPosition, levelExpression, logCallArgumentProvider, logFieldName, methodSignature);

	}

	protected static String getLogTypeName(LoggingFramework framework) {
		final String fullQualifiedName = framework.getLogTypeName();
		return CommonHandlerUtil.getLastPart(fullQualifiedName);
	}

	private abstract static class Wrapping {
		final Wrapping wrapping;
		boolean hasLogField = false;

		public Wrapping(Wrapping wrapping) {
			super();
			this.wrapping = wrapping;
		}

		public StringBuilder getFullQulifiedNameBuilder() {
			StringBuilder sb = null;
			if (!hasLogField && wrapping != null && !wrapping.hasLogField) {
				sb = wrapping.getFullQulifiedNameBuilder();
			} else {
				sb = new StringBuilder();
			}
			return sb;
		}

		public abstract boolean shouldSkipMethod(final LoggingFramework framework, final JavacNode anyNode);

		protected boolean shouldSkipMethodOnNode(final LoggingFramework framework, final JCTree tree, final JavacNode anyNode) {
			if (hasSkipMtdAnnotationTrue(framework, tree, anyNode)) {
				return true;
			}
			if (hasSkipMtdAnnotationFalse(framework, tree, anyNode)) {
				return false;
			}
			if (wrapping != null) {
				return wrapping.shouldSkipMethod(framework, anyNode);
			}
			return false;
		}
	}

	private static class DynamicWrapping extends Wrapping {
		final JavacNode startNode;
		final String logFieldName;
		String fullQualifiedName; // cache

		public DynamicWrapping(final JavacNode node, final String logFieldName) {
			super(null);
			this.startNode = node;
			this.logFieldName = logFieldName;
		}

		private JavacNode getNextRelevantParent(JavacNode node) {
			while (node != null) {
				if ((node.get() instanceof JCClassDecl) || (node.get() instanceof JCMethodDecl) || (node.get() instanceof JCNewClass)) {
					return node;
				}
				node = node.directUp();
			}
			return null;
		}

		@Override public StringBuilder getFullQulifiedNameBuilder() {
			if (fullQualifiedName != null) {
				return new StringBuilder(fullQualifiedName);
			}
			JavacNode node = getNextRelevantParent(startNode);
			StringBuilder sb = null;
			if (node != null) {
				JCTree astNode = node.get();
				JavacNode parentNode = node.directUp();
				if (astNode instanceof JCMethodDecl) {
					sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
					ClassContext cc = getClassContext(node, logFieldName);
					MethodContext.appendSignature(sb, (JCMethodDecl) astNode, cc == null ? null : cc.classDeclaration);
				} else if (astNode instanceof JCClassDecl) {
					if (logFieldName == null || fieldExists(logFieldName, node) == MemberExistsResult.NOT_EXISTS) {
						final JCNewClass parentAsNewClass = JavacHandlerUtil.getParentAsNewClass(node);
						if (parentAsNewClass != null) {
							parentNode = parentNode.directUp(); // skip one
																// level
						}
						sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
						JCClassDecl td = (JCClassDecl) astNode;
						ClassContext.appendSignature(sb, td, parentAsNewClass);
					}
				} else if (astNode instanceof JCNewClass) {
					sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
					ClassContext.appendSignature(sb, (JCNewClass) astNode);
				}
			}
			if (sb == null) {
				sb = new StringBuilder();
				if (logFieldName == null) {
					final String packageDeclaration = (startNode != null) ? startNode.getPackageDeclaration() : "";
					if (packageDeclaration != null) {
						sb.append(packageDeclaration);
					}
				}
			}
			fullQualifiedName = sb.toString();
			return sb;
		}

		@Override public boolean shouldSkipMethod(LoggingFramework framework, JavacNode anyNode) {
			JavacNode node = getNextRelevantParent(startNode);

			while (node != null) {
				String annotationString = annotationCache.get(node.hashCode());
				if (annotationString == null) {
					annotationString = JavacHandlerUtil.getAnnotationString(node.get(), framework.getAnnotationClass(), anyNode);
				}

				if (hasSkipMtdAnnotationTrue(annotationString)) {
					return true;
				}
				if (hasSkipMtdAnnotationFalse(annotationString)) {
					return false;
				}

				node = getNextRelevantParent(node.directUp());
			}
			return false;
		}
	}

	private static class ClassContext extends Wrapping {
		final JCClassDecl classDeclaration;
		private final ClassContext parentClassContext;
		private final JCNewClass newClass;

		public ClassContext(JCClassDecl classDeclaration, JCNewClass newClass, ClassContext parentClassContext, Wrapping wrapping) {
			super(wrapping);
			this.classDeclaration = classDeclaration;
			this.newClass = newClass;
			this.parentClassContext = parentClassContext;
		}

		@Override public StringBuilder getFullQulifiedNameBuilder() {
			final StringBuilder sb = super.getFullQulifiedNameBuilder();
			appendSignature(sb, classDeclaration, newClass);
			return sb;
		}

		public static void appendSignature(StringBuilder sb, JCClassDecl classDeclaration, JCNewClass newClass) {
			String name = null;
			if (classDeclaration.name.toString().length() == 0) {
				if (newClass == null) {
					name = "<anon>";
				} else {
					appendSignature(sb, newClass);
					return;
				}
			} else {
				name = classDeclaration.name.toString();
			}
			if (name != null) {
				if (sb.length() > 0) {
					sb.append(".");
				}
				sb.append(name);
			}
		}

		public static void appendSignature(StringBuilder sb, JCNewClass newClass) {
			if (sb.length() > 0) {
				sb.append(".");
			}

			sb.append(JavacHandlerUtil.extractConstructorName(newClass));
		}

		@Override public boolean shouldSkipMethod(final LoggingFramework framework, final JavacNode anyNode) {
			return shouldSkipMethodOnNode(framework, classDeclaration, anyNode);
		}
	}

	private static class MethodContext extends Wrapping {
		final JCMethodDecl methodDeclaration;
		final ClassContext classContext;
		private String methodSignature = null;
		private final MethodContext parentMethodContext;

		public MethodContext(JCMethodDecl methodDeclaration, ClassContext classContext, MethodContext parentMethodContext, Wrapping wrapping) {
			super(wrapping);
			this.methodDeclaration = methodDeclaration;
			this.classContext = classContext;
			this.parentMethodContext = parentMethodContext;
		}

		@Override public StringBuilder getFullQulifiedNameBuilder() {
			if (methodSignature == null) {
				final StringBuilder sb = super.getFullQulifiedNameBuilder();
				appendSignature(sb, methodDeclaration, classContext.classDeclaration);
				return sb;
			} else {
				return new StringBuilder(methodSignature);
			}
		}

		public static void appendSignature(StringBuilder sb, JCMethodDecl methodDeclaration, JCClassDecl classDeclaration) {
			if (sb.length() > 0) {
				sb.append(".");
			}

			sb.append(JavacHandlerUtil.getMethodSignature(methodDeclaration, classDeclaration));
		}

		public String getMethodSignature() {
			if (methodSignature == null) {
				methodSignature = getFullQulifiedNameBuilder().toString();
			}
			return methodSignature;
		}

		@Override public boolean shouldSkipMethod(final LoggingFramework framework, final JavacNode anyNode) {
			return shouldSkipMethodOnNode(framework, methodDeclaration, anyNode);
		}
	}

	private static void traverseSource(final LoggingFramework framework,
									final JavacNode typeNode,
									final String logFieldName,
									final String[] skipMethods,
									final JavacNode annotationNode,
									final boolean[] requiresImport) {
		final JCTree jcTree = typeNode.get();

		final TreeTranslator visitor = new TreeTranslator() {
			ClassContext currentClassContext = getClassContext(typeNode, logFieldName);
			MethodContext currentMethodContext = null;
			Wrapping currentWrapping = new DynamicWrapping(typeNode.up(), logFieldName);

			JCNewClass lastNewClass = null;

			@Override
			public void visitVarDef(JCVariableDecl tree) {
				if (currentWrapping instanceof ClassContext && tree.name.toString().equals(logFieldName)) {
					currentWrapping.hasLogField = true;
				}
				super.visitVarDef(tree);
			}

			@Override
			public void visitNewClass(JCNewClass tree) {
				lastNewClass = tree;
				super.visitNewClass(tree);
			}

			@Override
			public void visitClassDef(JCClassDecl tree) {
				currentClassContext = new ClassContext(tree, lastNewClass, currentClassContext, currentWrapping);
				currentWrapping = currentClassContext;
				try {
					if (notOnStartingNodeAndHasLogAnnotation(tree)) {
						result = tree;
						return;
					}

					super.visitClassDef(tree);
				} finally {
					currentWrapping = currentWrapping.wrapping;
					currentClassContext = currentClassContext.parentClassContext;
				}
			}

			protected boolean notOnStartingNodeAndHasLogAnnotation(JCTree tree) {
				boolean onStartingNode = (tree == annotationNode.up().get());
				if (!onStartingNode) {
					boolean hasQFLogAnnotation = hasLogAnnotation(framework, tree, annotationNode);
					if (hasQFLogAnnotation) return true; // is handled directly
				}
				return false;
			}

			@Override
			public void visitMethodDef(JCMethodDecl tree) {
				currentMethodContext = new MethodContext(tree, currentClassContext, currentMethodContext, currentWrapping);
				currentWrapping = currentMethodContext;
				try {
					if (notOnStartingNodeAndHasLogAnnotation(tree)) {
						result = tree;
						return;
					}
					boolean skipMtd = currentWrapping.shouldSkipMethod(framework, annotationNode);

					if (!skipMtd) {
						visitMethodDefForMtdLogCall(framework, currentMethodContext, annotationNode, skipMethods, requiresImport);
					}

					super.visitMethodDef(tree);
				} finally {
					currentWrapping = currentWrapping.wrapping;
					currentMethodContext = currentMethodContext.parentMethodContext;
				}
			}

			@SuppressWarnings("unchecked")
			public <T extends JCTree> T translate(T tree) {
				if (tree == null) {
					return null;
				} else {
					if (tree instanceof JCExpressionStatement) {
						JCExpressionStatement exprStatement = (JCExpressionStatement) tree;
						if (exprStatement.expr instanceof JCMethodInvocation) {
							JCStatement stmt = translateMethodInvocation((JCMethodInvocation) exprStatement.expr);
							if (stmt != null) {
								this.result = null;
								return (T) stmt;// XXX cast
							}
						}
					}
					return super.translate(tree);
				}
			}

			public JCStatement translateMethodInvocation(JCMethodInvocation jmi) {
				if (!isQFLogMethodCall(framework, jmi)) return null;
				JCStatement wrappedQFLogCall = prepareWrappedQFLogCall(framework, typeNode, jmi, logFieldName, currentMethodContext, requiresImport);
				SimplifiedJavacNode sourceNode = new SimplifiedJavacNode(jmi,typeNode);
				final JCStatement statement = simplifiedRecursiveSetGeneratedBy(wrappedQFLogCall, sourceNode);

				if (Javac.getJavaCompilerVersion() > 8) {
					// From Java 9 on, Javac's ArgumentAttr.java has an return type cache which is based on the position of a statement.
					// Since the previous method sets all JCTree elements to the same position, argument attribute resolving
					// will fail with a class cast exception during compiling.
					// Therefore, we assign to every sub-JCTree object an individual position with the next call:
					statement.accept(new IncreaseSourcePosScanner());
				}

				return statement;
			}

		};

		jcTree.accept(visitor);

	}

	private static class IncreaseSourcePosScanner extends TreeScanner {
		private int delta = 0;

		@Override public void scan(JCTree tree) {
			if (tree == null) return;
			tree.pos += ++delta;
			super.scan(tree);
		}
	}

	protected static ClassContext getClassContext(JavacNode typeNode, String logFieldName) {
		while (typeNode != null && !(typeNode.get() instanceof JCClassDecl)) {
			typeNode = typeNode.up();
		}
		if (typeNode == null) {
			return null;
		}
		return new ClassContext((JCClassDecl) typeNode.get(), JavacHandlerUtil.getParentAsNewClass(typeNode), null, new DynamicWrapping(typeNode.up(), logFieldName));
	}

	protected static void visitMethodDefForMtdLogCall(LoggingFramework framework,
													MethodContext methodContext,
													JavacNode annotationNode,
													String[] skipMethods,
													final boolean[] requiresImport) {

		JCMethodDecl methodDeclaration = methodContext.methodDeclaration;

		if (isInSkipMethods(methodContext, skipMethods) && !hasSkipMtdAnnotationFalse(framework, methodDeclaration, annotationNode)) {
			return;
		}

		addMethodLogCall(framework, methodContext, annotationNode, requiresImport);
	}

	private static boolean isQFLogMethodCall(final LoggingFramework framework, JCMethodInvocation jcmi) {

		final String[] validQFLogMethodNames = framework.getValidQFLogMethodNames();

		if (jcmi.meth instanceof JCFieldAccess) {
			JCFieldAccess fa = (JCFieldAccess) jcmi.meth;
			if (!(fa.selected instanceof JCMethodInvocation)) return false;
			if (Arrays.binarySearch(validQFLogMethodNames, fa.name.toString()) < 0) return false;
			return isQFLogMethodCall(framework, (JCMethodInvocation) fa.selected);
		}

		if (!(jcmi.meth instanceof JCIdent)) return false;
		JCIdent jci = (JCIdent) jcmi.meth;
		final String methodName = jci.name.toString();

		if (!methodName.equals(framework.getLogMethodName())) return false;
		if (jcmi.args == null) return false;
		if (jcmi.args.isEmpty()) return false;
		if (isGenerated(jcmi)) return false;
		return true;
	}

	public static JCStatement prepareWrappedQFLogCall(LoggingFramework framework,
													JavacNode source,
													JCMethodInvocation jmi,
													String logFieldName,
													MethodContext methodContext,
													final boolean[] requiresImport) {

		final boolean primaryCall = jmi.meth instanceof JCIdent;

		String methodSignature = "static";
		try {
			methodSignature = methodContext.getMethodSignature();
		} catch (NullPointerException e) {
			// ignore
		}

		requiresImport[0] = true;

		if (primaryCall) {
			return generateWrappedQFLogCall(framework, source, jmi, logFieldName, methodSignature);
		} else {
			// we know what's coming in.
			JCFieldAccess fa = (JCFieldAccess) jmi.meth;
			JCStatement parentCall = prepareWrappedQFLogCall(framework, source, (JCMethodInvocation) fa.selected, logFieldName, methodContext, requiresImport);

			// We constructed the block ourselves, so we know its structure...
			JCStatement ifCall = parentCall;

			if (parentCall instanceof JCBlock) {
				ifCall = ((JCBlock) parentCall).stats.get(1);
			}
			JCFieldAccess logCallFieldAccess = (JCFieldAccess) ((JCMethodInvocation) ((JCExpressionStatement) ((JCIf) ifCall).thenpart).expr).meth;

			final String methodName = fa.name.toString();
			if (framework.isDumpStackCall(methodName)) {
				logCallFieldAccess.name = source.toName("dumpStack");
			} else {

				LogCallArgumentProvider logCallArgumentProvider = null;
				if (framework.isParamsCall(methodName)) {
					try {
						final JCMethodDecl methodDeclaration = methodContext.methodDeclaration;
						logCallArgumentProvider = new MethodDeclBasedLogCallArgumentProvider(source.getTreeMaker(), methodDeclaration);
					} catch (NullPointerException ex) {
						// ignore
					}
				} else {
					final LogCallArgumentType addType = framework.getAddType(methodName);
					logCallArgumentProvider = new MethodInvocationBasedLogCallProvider(jmi, 0, addType);
				}
				if (logCallArgumentProvider != null) {
					final JCExpression builderCall = logCallFieldAccess.selected;
					logCallFieldAccess.selected = addArgumentsToLogCall(source, builderCall, logCallArgumentProvider);
				}
			}
			return parentCall;
		}
	}

	private static class LogCallArgument {
		LogCallArgumentType type;
		Object label;
		JCExpression message;

		public LogCallArgument(final JCExpression message) {
			this.message = message;
		}

		public LogCallArgument(final Object label, final JCExpression message) {
			this(message);
			this.label = label;
		}
	}

	private abstract static class LogCallArgumentProvider implements Iterable<LogCallArgument> {
		final LogCallArgumentType addType;

		public LogCallArgumentProvider(final LogCallArgumentType addType) {
			this.addType = addType;
		}

		int getStartIndex() {
			return 0;
		}

		abstract boolean hasNextExpression(int index);

		abstract JCExpression getNextExpression(int index);

		@Override public Iterator<LogCallArgument> iterator() {
			return new Iterator<LogCallArgument>() {

				int index = getStartIndex();
				Queue<LogCallArgument> pufferedArguments = null;

				@Override
				public boolean hasNext() {
					return (pufferedArguments != null && !pufferedArguments.isEmpty()) || hasNextExpression(index);
				}

				@Override
				public LogCallArgument next() {
					if (pufferedArguments != null) {
						final LogCallArgument derivedArg = pufferedArguments.poll();
						if (derivedArg != null) {
							derivedArg.type = addType;
							return derivedArg;
						}
					}
					pufferedArguments = null;

					if (!hasNext()) {
						return null;
					}

					final LogCallArgument logCallArg = nextFromExpression();
					logCallArg.type = addType;
					return logCallArg;
				}

				protected LogCallArgument nextFromExpression() {
					final boolean isFirstArgument = (index == getStartIndex());

					JCExpression expr = getNextExpression(index);
					index++;

					if (JavacHandlerUtil.isStringLiteral(expr)) {
						if (isFirstArgument && hasNext()) {
							final LogCallArgument arg = nextFromExpression();
							arg.label = expr;
							return arg;
						}
						pufferedArguments = new LinkedList<LogCallArgument>();
						fillPufferedArguments(expr);
						return pufferedArguments.poll();
					} else {
						return new LogCallArgument(expr.toString(), expr);
					}
				}

				private void fillPufferedArguments(final JCExpression arg) {
					assert pufferedArguments != null;

					if (arg == null) {
						return;
					}

					if ((arg instanceof JCBinary)) {
						JCBinary binaryArg = (JCBinary) arg;
						if (binaryArg.getKind() == Kind.PLUS) {
							fillPufferedArguments(binaryArg.lhs);
							fillPufferedArguments(binaryArg.rhs);
							return;
						}
					}
					pufferedArguments.offer(new LogCallArgument(arg));

				}

				@Override
				public void remove() {
					throw new UnsupportedOperationException("Remove not implemented");
				}
			};
		}
	}

	private static final class MethodDeclBasedLogCallArgumentProvider extends LogCallArgumentProvider {
		private final JavacTreeMaker maker;
		private final JCMethodDecl declaration;

		private MethodDeclBasedLogCallArgumentProvider(JavacTreeMaker maker, JCMethodDecl declaration) {
			super(LogCallArgumentType.DETAIL);
			this.maker = maker;
			this.declaration = declaration;
		}

		@Override boolean hasNextExpression(int index) {
			return declaration.params != null && index < declaration.params.size();
		}

		@Override JCExpression getNextExpression(int index) {
			return maker.Ident(declaration.params.get(index).name);
		}
	}

	private static class MethodInvocationBasedLogCallProvider extends LogCallArgumentProvider {
		final JCMethodInvocation methodInvocation;
		final int argumentsToIgnore;

		public MethodInvocationBasedLogCallProvider(final JCMethodInvocation methodInvocation, final int argumentsToIgnore, final LogCallArgumentType addType) {
			super(addType);
			this.methodInvocation = methodInvocation;
			this.argumentsToIgnore = argumentsToIgnore;
		}

		@Override int getStartIndex() {
			return argumentsToIgnore;
		}

		@Override boolean hasNextExpression(int index) {
			return methodInvocation.args != null && index < methodInvocation.args.size();
		}

		@Override JCExpression getNextExpression(int index) {
			return methodInvocation.args.get(index);
		}

	}

	public static JCStatement generateWrappedQFLogCall(LoggingFramework framework, JavacNode source, JCMethodInvocation jmi, String logFieldName, String methodSignature) {
		JCExpression levelExpression = jmi.args.get(0);
		LogCallArgumentProvider logCallArgumentProvider = new MethodInvocationBasedLogCallProvider(jmi, 1, LogCallArgumentType.STANDARD);
		final int startPosition = jmi.getPreferredPosition();
		return generateQFLogCall(framework, source, startPosition, levelExpression, logCallArgumentProvider, logFieldName, methodSignature);
	}

	public static JCStatement generateQFLogCall(LoggingFramework framework,
												JavacNode source,
												int startPosition,
												JCExpression levelExpression,
												LogCallArgumentProvider logCallArgumentProvider,
												String logFieldName,
												String methodSignature) {
		final JavacTreeMaker maker = source.getTreeMaker();

		final JCExpression logCallReceiver = chainDotsString(source, logFieldName + ".lvlBuild");

		final String fullLogTypeName = framework.getLogTypeName();
		final String shortLogTypeName = CommonHandlerUtil.getLastPart(fullLogTypeName);

		boolean usesLogConstant = false;
		if (levelExpression instanceof JCFieldAccess) {
			String firstArgumentAsString = levelExpression.toString();
			usesLogConstant |= firstArgumentAsString.startsWith(fullLogTypeName + ".");
			usesLogConstant |= firstArgumentAsString.startsWith(shortLogTypeName + ".");
		} else if (levelExpression instanceof JCIdent) {
			JCExpression extendedArgument = expandLogLevel(framework, (JCIdent) levelExpression, source);
			if (extendedArgument != null) {
				levelExpression = extendedArgument;
				usesLogConstant = true;
			}
		}

		JCExpression logLevelParameter = levelExpression;
		final Name levelArgumentVarname = source.toName("$level");

		if (!usesLogConstant) {
			logLevelParameter = maker.Ident(levelArgumentVarname);
		}

		JCExpression methodParameter = maker.Literal(methodSignature);

		int lineNumber = sourceHelper.getLineNumber(startPosition);

		final List<JCExpression> parameterList;
		if (lineNumber == 0) {
			parameterList = List.<JCExpression>of(logLevelParameter, methodParameter);
		} else {
			JCExpression lineNumberParameter = maker.Literal(CTC_INT, lineNumber);
			parameterList = List.<JCExpression>of(logLevelParameter, methodParameter, lineNumberParameter);
		}
		JCExpression builderCall = maker.Apply(List.<JCExpression>nil(), logCallReceiver, parameterList);

		builderCall = addArgumentsToLogCall(source, builderCall, logCallArgumentProvider);

		JCMethodInvocation logCall = maker.Apply(List.<JCExpression>nil(), maker.Select(builderCall, source.toName("log")), List.<JCExpression>nil());

		final JCExpression levelReference = chainDotsString(source, logFieldName + ".level");

		final JCIf ifClause = maker.If(maker.Binary(CTC_GREATER_OR_EQUAL, levelReference, cloneType(maker, logLevelParameter, source)), maker.Exec(logCall), null);
		if (usesLogConstant) {
			return ifClause;
		} else {
			JCVariableDecl localLevelVar = maker.VarDef(maker.Modifiers(Flags.FINAL), levelArgumentVarname, maker.TypeIdent(CTC_INT), cloneType(maker,levelExpression,source));
			return maker.Block(0, List.<JCStatement>of(localLevelVar, ifClause));
		}
	}

	protected static JCExpression addArgumentsToLogCall(JavacNode source, JCExpression builderCall, LogCallArgumentProvider logCallArgumentProvider) {

		final JavacTreeMaker maker = source.getTreeMaker();

		for (LogCallArgument arg : logCallArgumentProvider) {

			List<JCExpression> argList = null;
			if (arg.label != null) {

				JCExpression argNameParameter;
				if (arg.label instanceof JCExpression) {
					argNameParameter = (JCExpression) arg.label;
				} else {
					argNameParameter = maker.Literal(arg.label.toString());
				}
				argList = List.<JCExpression>of(argNameParameter, arg.message);

			} else {
				argList = List.<JCExpression>of(arg.message);
			}

			Name addMethodName;
			switch (arg.type) {
			case DETAIL:
				addMethodName = source.toName("addDetail");
				break;
			case ALL:
				addMethodName = source.toName("addAll");
				break;
			default:
				addMethodName = source.toName("add");
			}
			builderCall = maker.Apply(List.<JCExpression>nil(), maker.Select(builderCall, addMethodName), argList);

		}

		return builderCall;
	}

	private static JCExpression expandLogLevel(LoggingFramework framework, JCIdent firstArgument, JavacNode source) {
		final String literal = firstArgument.toString();
		if (Arrays.binarySearch(framework.getLoggingConstants(), literal) < 0) return null;

		final String logTypeName = getLogTypeName(framework);
		return chainDotsString(source, logTypeName + "." + literal);
	}

	/**
	 * Handles the {@link lombok.extern.qfs.QFLogger} annotation for javac.
	 */
	@Provides
	@HandlerPriority(value = 500)
	public static class HandleQFLoggerAnnotation extends JavacAnnotationHandler<lombok.extern.qfs.QFLog> {
		@Override
		public void handle(AnnotationValues<lombok.extern.qfs.QFLog> annotation, JCAnnotation ast, JavacNode annotationNode) {
			handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.LOG_QFLOG_FLAG_USAGE, "@QFLog", ConfigurationKeys.LOG_ANY_FLAG_USAGE, "any @Log");

			final String loggerClass = annotation.getRawExpression("loggerClass");
			final String loggerTypeName = loggerClass == null ? "" : loggerClass.replaceAll("(?:Void)?\\.class", "");

			processLogAnnotation(LoggingFramework.QFLOG,
								annotation,
								annotationNode,
								annotation.getInstance().topic(),
								annotation.getInstance().ownLogger(),
								annotation.getInstance().skipMtdNames(),
								annotation.getInstance().ignoreExistingLogger(),
								loggerTypeName);
		}
	}
}
