/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.javac.handlers;

import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.javac.Javac.CTC_INT;
import static lombok.javac.handlers.JavacHandlerUtil.*;

import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;

import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.NotifierFramework;
import lombok.extern.qfs.intern.javac.JavacHandlerUtil;
import lombok.javac.JavacAnnotationHandler;
import lombok.javac.JavacNode;
import lombok.javac.JavacTreeMaker;
import lombok.javac.handlers.JavacHandlerUtil.MemberExistsResult;
import lombok.spi.Provides;

public class HandleQFNotifierSpec {

	private HandleQFNotifierSpec() {
		throw new UnsupportedOperationException();
	}

	public static void processNotifierSpecAnnotation(NotifierFramework framework, AnnotationValues<?> annotation, JavacNode annotationNode,
			String topicName, final String[] notificationNames) {
		try {
			JavacNode typeNode = annotationNode.up();

			final int postLevel = framework.getLevel(annotation);

			switch (typeNode.getKind()) {
			case TYPE:

				if ((((JCClassDecl) typeNode.get()).mods.flags & Flags.INTERFACE) != 0) {
					annotationNode.addError("@QFNotifierSpec is legal only on classes and enums.");
					return;
				}

				topicName = addNotifierField(framework, typeNode, annotationNode, topicName);
				addPostLevelField(framework, typeNode, annotationNode, postLevel);
				for (final String notificationName: notificationNames) {
					if (notificationName == null) continue;
					if ("".equals(notificationName)) continue;
					addNameField(framework, typeNode, annotationNode, notificationName, topicName);
				}

				if (notificationNames.length > 0) {
					final String notificationNameType = framework.getNotificationNameTypeName();
					JavacHandlerUtil.addImportStatement(notificationNameType, typeNode);
				}

				final String notifierName = framework.getTypeName();
				JavacHandlerUtil.addImportStatement(notifierName, typeNode);

				break;
			default:
				annotationNode.addError(annotationNode + " is legal only on types, not on " + typeNode.getKind() + " - " + typeNode.getName());
				break;
			}
		} finally {
			deleteAnnotationIfNeccessary(annotationNode, framework.getSpecAnnotationClass());
		}
	}

	private static String addNotifierField(final NotifierFramework framework, final JavacNode typeNode, final JavacNode annotationNode, final String topic) {
		final String notifierFieldName = framework.getNotifierConstName();

		JavacNode typeToAddField = JavacHandlerUtil.getEnclosingTypeDeclaration(typeNode);

		final MemberExistsResult fieldExists = fieldExists(notifierFieldName, typeToAddField);

		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
			annotationNode.addWarning("Field '" + notifierFieldName + "' already exists.");
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			return createNotifierField(framework, typeToAddField, annotationNode, notifierFieldName, topic);
		}

		return topic;
	}

	private static String createNotifierField(NotifierFramework framework, JavacNode typeNode, JavacNode source, String notifierFieldName, String topic) {
		JavacTreeMaker maker = typeNode.getTreeMaker();

		JCExpression notifierType = chainDotsString(typeNode, CommonHandlerUtil.getLastPart(framework.getTypeName()));
		JCExpression notifierInstanceType = chainDotsString(typeNode, CommonHandlerUtil.getLastPart(framework.getInstanceTypeName()));

		List<JCExpression> args = null;

		if (topic == null || topic.trim().length() == 0) {
			topic = JavacHandlerUtil.getFullQualifiedClassName(typeNode);
		}

		Name methodName = null;
		methodName = typeNode.toName("instance");
		JCExpression notifierName = maker.Literal(topic);
		args = List.<JCExpression>of(notifierName);

		JCExpression notifierInstanceCall = maker.Apply(List.<JCExpression>nil(), maker.Select(notifierInstanceType, methodName), args);

		JCVariableDecl fieldDecl = recursiveSetGeneratedBy(maker.VarDef(maker.Modifiers(Flags.PUBLIC | Flags.FINAL | Flags.STATIC), typeNode.toName(notifierFieldName), notifierType, notifierInstanceCall), source);

		injectField(typeNode, fieldDecl);
		return topic;
	}

	private static String addPostLevelField(final NotifierFramework framework, final JavacNode typeNode, final JavacNode annotationNode, final int postLevel) {
		final String postLevelFieldName = framework.getPostLevelConstName();

		JavacNode typeToAddField = JavacHandlerUtil.getEnclosingTypeDeclaration(typeNode);

		final MemberExistsResult fieldExists = fieldExists(postLevelFieldName, typeToAddField);

		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
			annotationNode.addWarning("Field '" + postLevelFieldName + "' already exists.");
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			createPostLevelField(framework, typeToAddField, annotationNode.get(), postLevelFieldName, postLevel);
		}

		return postLevelFieldName;
	}

	private static boolean createPostLevelField(NotifierFramework framework, JavacNode typeNode, JCTree source, String postLevelFieldName, int postLevel) {
		JavacTreeMaker maker = typeNode.getTreeMaker();

		final JCExpression levelExpression = expandLevel(framework, postLevel, typeNode);

		final JCVariableDecl varDef = maker.VarDef(maker.Modifiers(Flags.PUBLIC | Flags.FINAL | Flags.STATIC), typeNode.toName(postLevelFieldName), maker.TypeIdent(CTC_INT), levelExpression);
		JCVariableDecl fieldDecl = recursiveSetGeneratedBy(varDef, typeNode);

		injectField(typeNode, fieldDecl);
		return true;
	}

	private static String addNameField(final NotifierFramework framework, final JavacNode typeNode, final JavacNode annotationNode, final String notificationName, final String topic) {

		String notificationNameFieldName = framework.getFieldNameForNotificationName(notificationName);

		JavacNode typeToAddField = JavacHandlerUtil.getEnclosingTypeDeclaration(typeNode);

		final MemberExistsResult fieldExists = fieldExists(notificationNameFieldName, typeToAddField);

		if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			createNameField(framework, typeToAddField, annotationNode, notificationNameFieldName, notificationName, topic);
		} else {
			annotationNode.addWarning("Field '" + notificationNameFieldName + "' already exists.");
		}

		return notificationNameFieldName;
	}

	private static String createNameField(NotifierFramework framework, JavacNode typeNode, JavacNode annotationNode, final String notifierFieldName, String notificationName, String topic) {
		JavacTreeMaker maker = typeNode.getTreeMaker();

		JCExpression stringType = chainDotsString(typeNode, "String");

		notificationName = topic + "." + notificationName;

		JCExpression instantiation = maker.Literal(notificationName);

		final JCVariableDecl varDef = maker.VarDef(maker.Modifiers(Flags.PUBLIC | Flags.FINAL | Flags.STATIC), typeNode.toName(notifierFieldName), stringType, instantiation);

		addAnnotation(varDef.mods, typeNode, annotationNode, CommonHandlerUtil.getLastPart(framework.getNotificationNameTypeName()), null);
		JCVariableDecl fieldDecl = recursiveSetGeneratedBy(varDef, typeNode);

		injectField(typeNode, fieldDecl);
		return topic;
	}

	private static JCExpression expandLevel(NotifierFramework framework, int level, JavacNode source) {
		final String[] levelConstants = framework.getLevelConstants();
		final String levelConstant = (level <= 0) ? framework.getDefaultLevelConstant() : levelConstants[Math.min(Math.max(level, 1), levelConstants.length) - 1];
		return expandLevel(framework, levelConstant, source);
	}

	private static JCExpression expandLevel(NotifierFramework framework, String literal, JavacNode source) {
		if (framework.getLevel(literal) == 0) return null;

		final String typeName = CommonHandlerUtil.getLastPart(framework.getTypeName());
		return chainDotsString(source, typeName + "." + literal);
	}


	/**
	 * Handles the {@link lombok.extern.qfs.QFNotifierSpec} annotation for javac.
	 */
	@Provides
	@HandlerPriority(value = 580) // lower than 610 of HandleQFNotify
	public static class HandleQFNotifyAnnotation extends JavacAnnotationHandler<lombok.extern.qfs.QFNotifierSpec> {
		@Override public void handle(AnnotationValues<lombok.extern.qfs.QFNotifierSpec> annotation, JCAnnotation ast, JavacNode annotationNode) {
			handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFIERSPEC_FLAG_USAGE, "@QFNotifier");
			processNotifierSpecAnnotation(NotifierFramework.QFNOTIFY, annotation, annotationNode, annotation.getInstance().value(), annotation.getInstance().names());
		}
	}
}
