/*
 * Copyright (C) 2016 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.javac.handlers;

import static lombok.core.AST.Kind.METHOD;
import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.javac.handlers.JavacHandlerUtil.*;

import lombok.core.AST.Kind;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.extern.qfs.intern.ConfigurationKeys;
import lombok.extern.qfs.intern.PerformanceLoggingFramework;
import lombok.extern.qfs.intern.javac.JavacHandlerUtil;
import lombok.javac.JavacAnnotationHandler;
import lombok.javac.JavacNode;
import lombok.javac.JavacTreeMaker;
import lombok.spi.Provides;

import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCTry;
import com.sun.tools.javac.util.List;

public class HandlePerfLog {

	private HandlePerfLog() {
		throw new UnsupportedOperationException();
	}

	public static void processAnnotation(AnnotationValues<lombok.extern.qfs.PerfLog> annotation, JavacNode annotationNode) {
		deleteAnnotationIfNeccessary(annotationNode, lombok.extern.qfs.PerfLog.class);

		final Object tagExpression = annotation.getActualExpression("value");
		final boolean reset = annotation.getInstance().reset();

		JavacNode typeNode = annotationNode.up();
		Kind kind = typeNode.getKind();
		if (METHOD.equals(kind)) {
			wrapMethod(annotationNode, (JCExpression) tagExpression, reset);
			annotationNode.getAst().setChanged();
		} else {
			annotationNode.addError("@Perf is legal only on methods or constructors.");
		}
	}

	private static void wrapMethod(final JavacNode annotationNode, final JCExpression tagExpression, final boolean reset) {
		final JavacNode methodNode = annotationNode.up();


		final JCMethodInvocation beginPeriodInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.BEGIN_PERIOD_METHOD_NAME,tagExpression);
		recursiveSetGeneratedBy(beginPeriodInvocation, annotationNode);
		addBeginPerfLogCallToMethodBody(methodNode, beginPeriodInvocation, annotationNode);

		if (reset) {
			final JCMethodInvocation resetInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.RESET_METHOD_NAME);
			recursiveSetGeneratedBy(resetInvocation, annotationNode);
			addBeginPerfLogCallToMethodBody(methodNode, resetInvocation, annotationNode);
		}

		final JCMethodInvocation endPeriodInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.END_PERIOD_METHOD_NAME, tagExpression);
		wrapBodyInTryCatch(annotationNode, methodNode, endPeriodInvocation);
	}

	private static void addBeginPerfLogCallToMethodBody(final JavacNode methodNode, JCMethodInvocation beginPeriodInvocation, final JavacNode source) {
		final JavacTreeMaker maker = methodNode.getTreeMaker();
		final JCMethodDecl methodDeclaration = (JCMethodDecl) methodNode.get();
		methodDeclaration.body.stats = JavacHandlerUtil.prependMethodInvocation(maker, methodDeclaration.body.stats, beginPeriodInvocation, source);
	}

	private static void wrapBodyInTryCatch(final JavacNode annotationNode, final JavacNode methodNode, final JCMethodInvocation endPeriodInvocation) {
		final JavacTreeMaker maker = methodNode.getTreeMaker();
		final JCMethodDecl methodDeclaration = (JCMethodDecl) methodNode.get();

		final List<JCCatch> catchers = List.<JCCatch>nil();

		final List<JCStatement> finallyStatements = List.<JCStatement>of(maker.Exec(endPeriodInvocation));
		final JCBlock finallyBlock = maker.Block(0, finallyStatements);

		final List<List<JCStatement>> constructorSplitStatements = lombok.extern.qfs.intern.javac.JavacHandlerUtil.separateConstructorCalls(methodDeclaration.body.stats);

		final JCBlock tryBody = maker.Block(0, constructorSplitStatements.get(1));
		final JCTry tryBlock = maker.Try(tryBody, catchers, finallyBlock);

		List<JCStatement> newBodyStatements = List.<JCStatement>of(tryBlock);
		for (JCStatement stat: constructorSplitStatements.get(0)) {
			newBodyStatements = newBodyStatements.prepend(stat);
		}
		final JCBlock newBody = maker.Block(0, newBodyStatements);

		// Do not recursively setGenerated by newBody, since this destroys the original source info, but only the new elements:
		setGeneratedBy(tryBody, annotationNode);
		setGeneratedBy(tryBlock, annotationNode);
		recursiveSetGeneratedBy(finallyBlock, annotationNode);
		setGeneratedBy(newBody, annotationNode);

		methodDeclaration.body = newBody;

	}

	private static JCMethodInvocation callPerformanceLogMethod(final JavacNode annotationNode, final String methodName,  final JCExpression... args) {
		final JavacNode methodNode = annotationNode.up();
		final JavacTreeMaker maker = methodNode.getTreeMaker();

		final JCExpression perfLogReceiver = chainDotsString(methodNode, getPerfClassName(annotationNode) + "." + PerformanceLoggingFramework.INSTANCE_METHOD);
		final JCMethodInvocation instanceInvocation = maker.Apply(List.<JCExpression>nil(),perfLogReceiver,List.<JCExpression>nil());

		List<JCExpression> parameterList = List.<JCExpression>nil();
		for (JCExpression arg: args) {
			if (arg == null) {
				arg = maker.Literal("");
			}
			parameterList = parameterList.append(cloneType(maker, arg, annotationNode));
		}

		return maker.Apply(List.<JCExpression>nil(), maker.Select(instanceInvocation, methodNode.toName(methodName)), parameterList);
	}

	private static String getPerfClassName(final JavacNode annotationNode) {
		final String configuredClassName = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.PERFLOG_LOG_CLASS);
		if (configuredClassName != null) return configuredClassName;

		return PerformanceLoggingFramework.CLASS_NAME;
	}

	/**
	 * Handles the {@link lombok.extern.qfs.PerfLog} annotation for javac.
	 */
	@Provides
	@HandlerPriority(value = 2048) // 2^11
	public static class HandlePerfAnnotation extends JavacAnnotationHandler<lombok.extern.qfs.PerfLog> {
		@Override public void handle(AnnotationValues<lombok.extern.qfs.PerfLog> annotation, JCAnnotation ast, JavacNode annotationNode) {
			handleFlagUsage(annotationNode, ConfigurationKeys.PERFLOG_FLAG_USAGE, "@Perf");
			processAnnotation(annotation, annotationNode);
		}
	}
}
