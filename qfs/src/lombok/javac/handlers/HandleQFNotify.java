/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.javac.handlers;

import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.javac.Javac.CTC_GREATER_OR_EQUAL;
import static lombok.javac.handlers.JavacHandlerUtil.*;
import static lombok.extern.qfs.intern.javac.JavacHandlerUtil.*;

import java.util.Set;
import java.util.TreeSet;

import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.List;

import lombok.core.AST.Kind;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.NotifierFramework;
import lombok.extern.qfs.intern.javac.JavacHandlerUtil;
import lombok.extern.qfs.intern.javac.JavacHandlerUtil.SimplifiedJavacNode;
import lombok.javac.JavacAnnotationHandler;
import lombok.javac.JavacNode;
import lombok.javac.JavacTreeMaker;
import lombok.spi.Provides;

public class HandleQFNotify {

	private HandleQFNotify() {
		throw new UnsupportedOperationException();
	}

	public static void processNotifyAnnotation(NotifierFramework framework, JavacNode annotationNode) {
		try {
			JavacNode typeNode = annotationNode.up();

			Kind kind = typeNode.getKind();
			if (Kind.TYPE.equals(kind)) {
				final String autoCompleteSpecs = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFY_AUTOCOMPLETE_SPECS);
				framework.addAutocompleteSpecs(autoCompleteSpecs);
				final Set<String> autoImportNotifierSpecs = new TreeSet<String>();
				traverseSource(framework, typeNode, annotationNode, autoImportNotifierSpecs);
				for (final String notifierSpec: autoImportNotifierSpecs) {
					JavacHandlerUtil.addProbablyStaticImportStatement(notifierSpec, typeNode);
				}
			} else {
				annotationNode.addError(annotationNode + " is legal only on types, not on " + kind + " - " + typeNode.getName());
			}
		} finally {
			deleteAnnotationIfNeccessary(annotationNode, framework.getAnnotationClass());
		}
	}

	private static void traverseSource(final NotifierFramework framework,
									final JavacNode typeNode,
									final JavacNode annotationNode,
									final Set<String> autoImportNotifierSpecs) {
		final JCTree jcTree = typeNode.get();
		final String postMethodName = framework.getPostMethodName();

		final TreeTranslator visitor = new TreeTranslator() {
			@SuppressWarnings("unchecked")
			@Override
			public <T extends JCTree> T translate(T tree) {
				if (tree == null) {
					return null;
				} else {
					if (tree instanceof JCExpressionStatement) {
						JCExpressionStatement exprStatement = (JCExpressionStatement) tree;
						if (exprStatement.expr instanceof JCMethodInvocation) {
							JCStatement stmt = translateMethodInvocation((JCMethodInvocation) exprStatement.expr);
							if (stmt != null) {
								this.result = null;
								return (T) stmt;// XXX cast
							}
						}
					}
					return super.translate(tree);
				}
			}

			public JCStatement translateMethodInvocation(JCMethodInvocation jmi) {
				final boolean isQFNotifyCall = isQFNotifyMethodCall(postMethodName, jmi);

				if (! isQFNotifyCall) return null;

				final JCStatement wrappingBlock = generateQFNotifyCall(framework, typeNode, jmi, autoImportNotifierSpecs);

				SimplifiedJavacNode sourceNode = new SimplifiedJavacNode(jmi,typeNode);
				return simplifiedRecursiveSetGeneratedBy(wrappingBlock, sourceNode);
			}

		};

		jcTree.accept(visitor);
	}

	private static boolean isQFNotifyMethodCall(final String expectedMethodName, JCMethodInvocation jcmi) {

		if (!(jcmi.meth instanceof JCIdent)) return false;
		JCIdent jci = (JCIdent) jcmi.meth;
		final String methodName = jci.name.toString();

		if (!methodName.equals(expectedMethodName)) return false;
		if (jcmi.args == null) return false;
		if (jcmi.args.size() == 0) return false;
		if (isGenerated(jcmi)) return false;
		return true;
	}

	public static JCStatement generateQFNotifyCall(NotifierFramework framework,
												JavacNode source,
												final JCMethodInvocation jmi,
												final Set<String> autoImportNotifierSpecs) {
		final JavacTreeMaker maker = source.getTreeMaker();

		JCExpression firstOriginalArgument = jmi.args.get(0);

		String notifierSpecClass = null;
		int parameterStartIndex = 0;
		if (! JavacHandlerUtil.isStringLiteral(firstOriginalArgument)) {
			notifierSpecClass = firstOriginalArgument.toString();
			final String lastPart = CommonHandlerUtil.getLastPart(notifierSpecClass);
			final boolean isConstant = lastPart.equals(lastPart.toUpperCase());
			if ((firstOriginalArgument instanceof JCFieldAccess) && isConstant) {
				notifierSpecClass = CommonHandlerUtil.getFirstParts(notifierSpecClass);
			} else {
				parameterStartIndex++;
			}
		}
		final String expandedNotifierSpecClass = framework.expandNotifierClassName(notifierSpecClass);

		if (! expandedNotifierSpecClass.equals(notifierSpecClass)) {
			autoImportNotifierSpecs.add(expandedNotifierSpecClass);
			notifierSpecClass = CommonHandlerUtil.getLastPart(expandedNotifierSpecClass);
		}

		final String notifierRef = notifierSpecClass + "." + framework.getNotifierConstName();
		final String postLevelRef = notifierSpecClass + "." + framework.getPostLevelConstName();

		final JCExpression levelParameter = chainDotsString(source, postLevelRef);


		final int argsLength = jmi.args.length();
		final JCExpression[] parameterList = new JCExpression[argsLength + 1 - parameterStartIndex];

		int index = 0;
		parameterList[index++] = levelParameter;
		for (int i = parameterStartIndex; i < jmi.args.length(); i++) {
			parameterList[index++]=jmi.args.get(i);
		}

		final JCExpression callReceiver = chainDotsString(source, notifierRef);
		JCMethodInvocation postCall = maker.Apply(List.<JCExpression>nil(), maker.Select(callReceiver, source.toName("postNotification")), List.from(parameterList));

		final JCExpression levelReference = chainDotsString(source, notifierRef + ".level");

		return maker.If(maker.Binary(CTC_GREATER_OR_EQUAL, levelReference, cloneType(maker,levelParameter,source)), maker.Exec(postCall), null);
	}

	/**
	 * Handles the {@link lombok.extern.qfs.QFNotify} annotation for javac.
	 */
	@Provides
	@HandlerPriority(value = 610)
	public static class HandleQFNotifyAnnotation extends JavacAnnotationHandler<lombok.extern.qfs.QFNotify> {
		@Override public void handle(AnnotationValues<lombok.extern.qfs.QFNotify> annotation, JCAnnotation ast, JavacNode annotationNode) {
			handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFY_FLAG_USAGE, "@QFNotify");
			processNotifyAnnotation(NotifierFramework.QFNOTIFY, annotationNode);
		}
	}
}
