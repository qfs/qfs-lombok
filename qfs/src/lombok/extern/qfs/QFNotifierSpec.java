/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Causes lombok to generate a NOTIFIER and a POST_LEVEL constant based on the information given in arguments.
 * The value() defaults to the annotated class' name and the level defaults to FUNDAMENTAL.
 * The name of a class annotated with @QFNotifierSpec can be easily used as first parameter of a qfnotify() call.
 * <p>
 * Example:
 * <pre>
package bla.blubb;

import lombok.extern.qfs.QFNotifierSpec;

@QFNotifierSpec(value="de.qfs.notification",level=de.qfs.lib.notifications.Notifier.BASIC)
class User {
  public static final String XYZ = "xyz";
}

@QFNotifierSpec(names={"bla!?!bla.bla","0BlubberBla1"})
class RecordReplay {}
 * </pre>
 *
 * will generate:
 *
 * <pre>
package bla.blubb;

import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;

class User {
  public static final Notifier NOTIFIER = Notifier.instance("de.qfs.notification");
  public static final int POST_LEVEL = Notifier.BASIC;
  public static final String XYZ = "xyz";
}

class RecordReplay {
  public static final Notifier NOTIFIER = Notifier.instance("bla.blubb.RecordReplay");
  public static final int POST_LEVEL = Notifier.FUNDAMENTAL;

  @NotificationName public static final String BLA_BLA_BLA = "bla.blubb.RecordReplay.bla!?!bla.bla";
  @NotificationName public static final String _BLUBBER_BLA1 = "bla.blubb.RecordReplay.0BlubberBla1";
}
 * </pre>
 *
 * This annotation is valid for classes and enums.<br />
 *
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface QFNotifierSpec {
	String value() default "";
	int level() default 0;
	String[] names() default {};
}
