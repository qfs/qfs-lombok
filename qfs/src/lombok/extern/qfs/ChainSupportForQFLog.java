// {{{ copyright

/********************************************************************
 *
 * Copyright (C) 2022 Quality First Software GmbH
 * All rights reserved
 *
 *******************************************************************/

// }}}

package lombok.extern.qfs;

/**
 * This class describes all simulated methods callable after qflog(...)
 *
 * @author Pascal Bihler
 *
 */
public interface ChainSupportForQFLog {

	/**
	 * Adds one or multiple objects to the logging. Labels are auto generated.
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog add(Object... objects);

	/**
	 * Adds one or multiple objects to the logging.
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog add(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level. Labels are auto generated.
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addDetail(Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level.
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addDetail(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level. Labels are auto generated.
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addDetails(Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level.
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addDetails(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging without list/array truncation. Labels are auto generated.
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addAll(Object... objects);

	/**
	 * Adds one or multiple objects to the logging without list/array truncation.
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog addAll(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level. Labels are auto generated.
	 * Synonym for {@link ChainSupportForQFLog#addDetail(Object...)}
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog detail(Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level.
	 * Synonym for {@link ChainSupportForQFLog#addDetail(String,Object...)}
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog detail(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level. Labels are auto generated.
	 * Synonym for {@link ChainSupportForQFLog#addDetail(Object...)}
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog details(Object... objects);

	/**
	 * Adds one or multiple objects to the logging on detail level.
	 * Synonym for {@link ChainSupportForQFLog#addDetail(String,Object...)}
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog details(String label, Object... objects);

	/**
	 * Adds one or multiple objects to the logging without list/array truncation. Labels are auto generated.
	 * Synonym for {@link ChainSupportForQFLog#addAll(Object...)}
	 *
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog all(Object... objects);

	/**
	 * Adds one or multiple objects to the logging without list/array truncation.
	 * Synonym for {@link ChainSupportForQFLog#addAll(String, Object...)}
	 *
	 * @param label The label for the first object
	 * @param objects the objects to log.
	 */
	ChainSupportForQFLog all(String label, Object... objects);

	/**
	 * Adds all parameters of the current method to the log call.
	 */
	ChainSupportForQFLog params();

	/**
	 * Logs the command. Effective noop, it logs anyway.
	 */
	void log();

	/**
	 * Adds a stacktrace of the log call position to the log.
	 */
	void dump();

	/**
	 * Adds a stacktrace of the log call position to the log.
	 */
	void dumpStack();

	/**
	 * Adds a stacktrace of the log call position to the log.
	 */
	void dumpstack();

}
