/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Causes lombok to g encapsulate every call of
 * qfnotify(Classname, String, ...) to call classnameNotifier.postNotification(String, ...).
 * If the Classname is <code>Default</code>, then the Notifier$Default class is imported.
 * This is equal to a missing specification of a Classname
 * <p>
 * Example:
 * <pre>
import bla.blubb.*;
import lombok.extern.qfs.QFNotify;

@QFNotify
class Bla {
  public void blubb() {
    qfnotify(Default,"web.perform.text.input", this, ...);
    qfnotify("bla.bla", this, ...);
    qfnotify(RecordReplay,"window.raise", this, ...);
    qfnotify(User,User.XYZ, this, ...);
  }
}
 * </pre>
 *
 * will generate:
 *
 * <pre>
import bla.blubb.*;
import de.qfs.lib.notifications.Notifier.Default;

class Bla {
  public void blubb() {
    if (Default.NOTIFIER.level >= Default.POST_LEVEL) {
        Default.NOTIFIER.postNotification(Default.POST_LEVEL, "web.perform.text.input", this, ...);
    }
    if (Default.NOTIFIER.level >= Default.POST_LEVEL) {
        Default.NOTIFIER.postNotification(Default.POST_LEVEL, "bla.bla", this, ...);
    }
    if (RecordReplay.NOTIFIER.level >= RecordReplay.POST_LEVEL) {
        RecordReplay.NOTIFIER.postNotification(RecordReplay.POST_LEVEL, "window.raise", this, ...);
    }
    if (User.NOTIFIER.level >= User.POST_LEVEL) {
        User.NOTIFIER.postNotification(User.POST_LEVEL, User.XYZ, this, ...);
    }
  }
}
 * </pre>
 *
 * This annotation is valid for classes and enums.<br />
 *
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface QFNotify {
}
