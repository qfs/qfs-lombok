/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Causes lombok to generate a logger field and encapsulate every call of qflog(int, Object...).
 * <p>
 * Currently the first qflog Object-parameter goes into add(), the rest into addDetail()
 *
 * Example:
 * <pre>
 * &#64;QFLog
 * public class LogExample {
 *
 *     public void main(String... args) {
 *         qflog(DBG,"Log Message", args);
 *     }
 * }
 * </pre>
 *
 * will generate:
 *
 * <pre>
 * import de.qfs.lib.log.Log;
 * public class LogExample {
 *     private static final de.qfs.lib.log.QFLogger log = new de.qfs.lib.log.QFLogger("LogExample");
 *
 *     public void main(String... args) {
 *         if (logger.level >= de.qfs.lib.log.Log.DBG) {
 *             logger.lvlBuild(de.qfs.lib.log.Log.DBG, "main(String[]"))
 *             .add("Log Message")
 *             .addDetail("args",args).log();
 *         }
 *     }
 * }
 * </pre>
 *
 * This annotation is valid for classes and enums.<br />
 *
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface QFLog {

	/**
	 * Sets the category of the constructed Logger. By default, it will use the type where the annotation is placed.
	 */
	String topic() default "";

	/**
	 * When set to true on an inner class, this class will get a dedicated logger field
	 */
	boolean ownLogger() default false;

	/**
	 * When set to true, there is not method log statement inserted in the annotated method/ enclosed methods
	 */
	boolean skipMtd() default false;

	/**
	 * Matching method names will not get a mtd log statement inserted
	 */
	String[] skipMtdNames() default "";

	/**
	 * If set on methods, those methods have a predefined method log level
	 */
	int level() default 0;

	/**
	 * When set to true, an existing "logger" field will not trigger a warning
	 */
	boolean ignoreExistingLogger() default false;

	/**
	 * Class used for the logger. Should be a subclass of QFLogger or support at least the same API.
	 */
	Class<?> loggerClass() default Void.class;
}
