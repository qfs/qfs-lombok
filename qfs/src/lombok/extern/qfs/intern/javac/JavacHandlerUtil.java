/*
 * Copyright (C) 2019 The Project Lombok Authors & Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern.javac;

import static com.sun.tools.javac.util.LayoutCharacters.*;
import static lombok.javac.Javac.storeEnd;
import static lombok.javac.JavacAugments.JCTree_generatedNode;
import static lombok.javac.handlers.JavacHandlerUtil.*;

import java.io.IOException;
import java.nio.CharBuffer;

import javax.lang.model.element.Modifier;
import javax.tools.JavaFileObject;

import com.sun.source.tree.Tree.Kind;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCImport;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.JCTree.JCReturn;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Position;

import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.javac.JavacNode;
import lombok.javac.JavacTreeMaker;

public class JavacHandlerUtil {

	public static class SourceHelper { // extracted from
										protected static char[] charBufferToArray(CharBuffer buffer) {
			if (buffer.hasArray()) return ((CharBuffer) buffer.compact().flip()).array();
			else
				return buffer.toString().toCharArray();
		}
		protected static char[] getCharContent(JavaFileObject fileObject) throws IOException {
			CharSequence cs = fileObject.getCharContent(true);
			if (cs instanceof CharBuffer) {
				return charBufferToArray((CharBuffer) cs);
			} else {
				return cs.toString().toCharArray();
			}
		}
		// com.sun.tools.javac.util.Log
		JavaFileObject source;
		private char[] buf = null;
		private int bp;

		private int line;

		private int lineStart;

		public JavaFileObject currentSource() {
			return source;
		}

		private boolean findLine(int pos) {
			if (pos == Position.NOPOS || currentSource() == null) return false;
			try {
				if (buf == null) {
					buf = getCharContent(currentSource());
					lineStart = 0;
					line = 1;
				} else if (lineStart > pos) { // messages don't come in order
					lineStart = 0;
					line = 1;
				}
				bp = lineStart;
				while (bp < buf.length && bp < pos) {
					switch (buf[bp++]) {
					case CR:
						if (bp < buf.length && buf[bp] == LF) bp++;
						line++;
						lineStart = bp;
						break;
					case LF:
						line++;
						lineStart = bp;
						break;
					}
				}
				return bp <= buf.length;
			} catch (IOException e) {
				e.printStackTrace();
				buf = new char[0];
			}
			return false;
		}

		public int getLineNumber(int pos) {
			if (findLine(pos)) return line;
			return 0;
		}

		public JavaFileObject useSource(final JavaFileObject name) {
			JavaFileObject prev = currentSource();
			if (name != prev) {
				source = name;
				buf = null;
			}
			return prev;
		}
	}

	public static void addImportStatement(String typeName, JavacNode typeNode) {
		final JavacNode compilationUnitNode = typeNode.top();

		JCTree.JCCompilationUnit compilationUnit = (JCTree.JCCompilationUnit) compilationUnitNode.get();

		JCFieldAccess logTypeExpr = (JCFieldAccess) chainDotsString(compilationUnitNode, typeName);
		JCImport logImport = compilationUnitNode.getTreeMaker().Import(logTypeExpr, false);

		String logImportStatement = logImport.toString();
		for (JCImport jcImport : compilationUnit.getImports()) {
			if (logImportStatement.equals(jcImport.toString())) {
				return;
			}
		}

		List<JCTree> defs = compilationUnit.defs;
		List<JCTree> newDefs = List.<JCTree>of(recursiveSetGeneratedBy(logImport, compilationUnitNode));

		if ("JCPackageDecl".equals(defs.head.getClass().getSimpleName())) { // Required for Java 9
			newDefs.tail = defs.tail;
			defs.tail = newDefs;
			newDefs = defs;
		} else {
			newDefs.tail = defs;
		}
		compilationUnit.defs = newDefs;
	}

	public static String extractConstructorName(JCNewClass newClass) {
		// String based hack..
		String expression = newClass.toString();
		int spaceIdx = expression.indexOf(' ');
		int bracketIdx = expression.indexOf('(');
		if (spaceIdx < 0 || bracketIdx < 0 || bracketIdx < spaceIdx) return "";
		return expression.substring(spaceIdx + 1, bracketIdx);
	}

	public static JCAnnotation getAnnotation(JCTree tree, final Class<? extends java.lang.annotation.Annotation> annotationType, JavacNode anyNode) {
		List<JCAnnotation> annotations = null;
		if (tree instanceof JCMethodDecl) {
			annotations = ((JCMethodDecl) tree).mods.annotations;
		} else if (tree instanceof JCClassDecl) {
			annotations = ((JCClassDecl) tree).mods.annotations;
		}
		if (annotations == null) {
			return null;
		}
		for (JCAnnotation annot : annotations) {
			if (lombok.javac.handlers.JavacHandlerUtil.typeMatches(annotationType, anyNode, annot.annotationType)) {
				return annot;
			}
		}
		return null;
	}

	public static String getAnnotationString(JCTree tree, final Class<? extends java.lang.annotation.Annotation> annotationType, JavacNode anyNode) {
		JCAnnotation annot = getAnnotation(tree, annotationType, anyNode);
		if (annot == null) {
			return "";
		}
		final String annotationAsString = annot.toString();
		return annotationAsString;
	}

	public static JavacNode getEnclosingTypeDeclaration(JavacNode typeNode) {
		while (typeNode != null && !(typeNode.get() instanceof JCClassDecl)) {
			typeNode = typeNode.up();
		}
		return typeNode;
	}

	public static JCVariableDecl getField(final String fieldName, JavacNode node) {
		node = upToTypeNode(node);

		if (node != null && node.get() instanceof JCClassDecl) {
			for (JCTree def : ((JCClassDecl) node.get()).defs) {
				if (def instanceof JCVariableDecl) {
					if (((JCVariableDecl) def).name.contentEquals(fieldName)) {
						return (JCVariableDecl) def;
					}
				}
			}
		}
		return null;
	}

	public static String getFullQualifiedClassName(JavacNode typeNode) {
		StringBuilder sb = new StringBuilder();
		prefixFullQualifiedClassName(typeNode, sb);
		return sb.toString();
	}

	public static String getMethodSignature(JavacNode typeNode) {
		JCMethodDecl declaration = (JCMethodDecl) typeNode.get();

		StringBuilder sb = new StringBuilder();

		String name = declaration.name.toString();
		if ("<init>".equals(name)) {
			JCTree parent = typeNode.up().get();
			if (parent instanceof JCClassDecl) {
				name = ((JCClassDecl) parent).name.toString();
			}
		}

		sb.append(name);
		sb.append("(");
		int i = 0;
		final List<JCVariableDecl> arguments = declaration.params;
		if (arguments != null) {
			for (JCVariableDecl arg: arguments) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append(CommonHandlerUtil.getLastPart(arg.vartype.toString()));
				i++;
			}
		}
		sb.append(")");

		return sb.toString();
	}

	public static String getMethodSignature(JCMethodDecl declaration, JCClassDecl parent) {
		String name = declaration.name.toString();

		if ("<init>".equals(name)) {
			if (parent != null) {
				name = parent.name.toString();
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("(");
		int i = 0;
		final List<JCVariableDecl> arguments = declaration.params;
		if (arguments != null) {
			for (JCVariableDecl arg : arguments) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append(CommonHandlerUtil.getLastPart(arg.vartype.toString()));
				i++;
			}
		}
		sb.append(")");

		return sb.toString();
	}

	public static JavacNode getOutmostClass(JavacNode typeNode) {
		final JavacNode compilationUnit = typeNode.top();
		JavacNode outmostClass = typeNode;
		while (outmostClass.up() != compilationUnit) {
			outmostClass = outmostClass.up();
		}
		return outmostClass;
	}

	public static JCNewClass getParentAsNewClass(JavacNode node) {
		JCTree astNode = node.directUp().get();
		if (astNode instanceof JCReturn) {
			astNode = ((JCReturn) astNode).expr;
		} else if (astNode instanceof JCExpressionStatement) { // direct call of
																// method of the
																// anonymous
																// class
			astNode = ((JCExpressionStatement) astNode).expr;
			if (astNode instanceof JCMethodInvocation) {
				astNode = ((JCMethodInvocation) astNode).meth;
			}
			while (astNode instanceof JCFieldAccess) {
				astNode = ((JCFieldAccess) astNode).selected;
			}
		}
		if (astNode instanceof JCNewClass) {
			return (JCNewClass) astNode;
		}
		return null;
	}

	public static boolean hasAnnotation(JCTree tree, final Class<? extends java.lang.annotation.Annotation> annotationType, final JavacNode anyNode) {
		return getAnnotation(tree, annotationType, anyNode) != null;
	}

	public static boolean outerClassIsStatic(JavacNode typeNode) {
		while (typeNode != null) {
			if (typeNode.get() instanceof JCTree.JCClassDecl) {
				boolean staticDeclared = ((JCTree.JCClassDecl) typeNode.get()).getModifiers().getFlags().contains(Modifier.STATIC);
				boolean topLevelClass = (typeNode.up() == typeNode.top());
				if (!(staticDeclared || topLevelClass)) {
					return false;
				}
			}
			typeNode = typeNode.up();
		}
		return true;
	}

	public static void prefixFullQualifiedClassName(JavacNode typeNode, StringBuilder sb) {
		String name = null;
		final JCTree astNode = typeNode.get();
		if (astNode instanceof JCClassDecl) {
			name = ((JCClassDecl)astNode).name.toString();
		} else if (astNode instanceof JCMethodDecl) {
			name = getMethodSignature(typeNode);
		} else if (astNode instanceof JCNewClass) {
			name = "new<" + ((JCNewClass) astNode).type.toString() + ">";
		} else {
			String packageDeclaration = typeNode.getPackageDeclaration();
			if (packageDeclaration != null) {
				sb.append(packageDeclaration);
			}
			return;
		}

		prefixFullQualifiedClassName(typeNode.up(), sb);
		if (sb.length() > 0) {
			sb.append(".");
		}

		if (name != null && name.length() > 0) {
			sb.append(name);
		} else {
			sb.append("?");
		}
	}

	public static List<JCStatement> prependMethodInvocation(final JavacTreeMaker maker, final List<JCStatement> statements, final JCMethodInvocation methodInvocation, final JavacNode source) {
		final List<List<JCStatement>> constructorSplitStatements = separateConstructorCalls(statements);

		final List<JCStatement> head = constructorSplitStatements.get(0);
		final List<JCStatement> tail = constructorSplitStatements.get(1);

		List<JCStatement> newList = tail.prepend(setGeneratedBy(maker.Exec(methodInvocation), source));
		for (JCStatement stat : head)
			newList = newList.prepend(stat);
		return newList;
	}

	public static List<List<JCStatement>> separateConstructorCalls(final List<JCStatement> statements) {
		List<JCStatement> head = List.nil();
		List<JCStatement> tail = statements;
		for (JCStatement stat : statements) {
			if (isConstructorCall(stat)) {
				tail = tail.tail;
				head = head.prepend(stat);
				continue;
			}
			break;
		}

		return List.<List<JCStatement>>of(head,tail);
	}

	public static boolean isStringLiteral(final JCExpression arg) {
		if (arg == null) {
			return false;
		}
		if (arg instanceof JCLiteral) {
			return ((JCLiteral) arg).getKind() == Kind.STRING_LITERAL;
		}
		if (!(arg instanceof JCBinary)) {
			return false;
		}
		JCBinary binaryArg = (JCBinary) arg;
		if (binaryArg.getKind() != Kind.PLUS) {
			return false;
		}
		;
		return isStringLiteral(binaryArg.lhs) || isStringLiteral(binaryArg.rhs);
	}


	public static void addProbablyStaticImportStatement(String typeName, JavacNode typeNode) {
		final JavacNode compilationUnitNode = typeNode.top();

		JCTree.JCCompilationUnit compilationUnit = (JCTree.JCCompilationUnit) compilationUnitNode.get();

		JCFieldAccess logTypeExpr = (JCFieldAccess)  chainDotsString(compilationUnitNode, typeName);
		JCImport logImport = compilationUnitNode.getTreeMaker().Import(logTypeExpr, false);

		String logImportStatement = logImport.toString();
		for (JCImport jcImport : compilationUnit.getImports()) {
			if (logImportStatement.equals(jcImport.toString())) {
				return;
			}
		}

		// Guess: If two package names start with an uppercase letter, it requires a static import...
		int numberOfUppercaseNames = 0;
		for (final String part: typeName.split("\\.")) {
			if (part.length() == 0) continue;
			final char c = part.charAt(0);
			if (c >= 'A' && c <= 'Z') {
				numberOfUppercaseNames++;
			}
		}
		boolean requiresStatic = numberOfUppercaseNames > 1;

		logImport.staticImport = requiresStatic;

		List<JCTree> defs = compilationUnit.defs;
		List<JCTree> newDefs = List.<JCTree>of(recursiveSetGeneratedBy(logImport,compilationUnitNode));

		if ("JCPackageDecl".equals(defs.head.getClass().getSimpleName())) { // Required for Java 9
			newDefs.tail = defs.tail;
			defs.tail = newDefs;
			newDefs = defs;
		} else {
			newDefs.tail = defs;
		}
		compilationUnit.defs = newDefs;
	}

	private JavacHandlerUtil() {
		//Prevent instantiation
	}


	// Copies for situations where you do not have a full JavacNode as source reference

	public static <T extends JCTree> T simplifiedRecursiveSetGeneratedBy(T node, SimplifiedJavacNode source) {
		if (node == null) return null;
		simplifiedSetGeneratedBy(node, source);
		node.accept(new SimplifiedMarkingScanner(source));
		return node;
	}

	public static <T extends JCTree> T simplifiedSetGeneratedBy(T node, SimplifiedJavacNode sourceNode) {
		if (node == null) return null;
		if (sourceNode == null) {
			JCTree_generatedNode.clear(node);
			return node;
		}
		JCTree_generatedNode.set(node, sourceNode.tree);

		if (!inNetbeansEditor(sourceNode.astContext) || isParameter(node)) {
			node.pos = sourceNode.startPos;
			storeEnd(node, sourceNode.endPos, sourceNode.compilationUnit);
		}
		return node;
	}

	private static class SimplifiedMarkingScanner extends TreeScanner {
		private final SimplifiedJavacNode source;

		SimplifiedMarkingScanner(SimplifiedJavacNode source) {
			this.source = source;
		}

		@Override public void scan(JCTree tree) {
			if (tree == null) return;
			simplifiedSetGeneratedBy(tree, source);
			super.scan(tree);
		}
	}

	public static class SimplifiedJavacNode {
		public JCTree tree;
		public int startPos;
		public int endPos;
		public JCCompilationUnit compilationUnit;
		public Context astContext;

		public SimplifiedJavacNode(JCTree tree, JavacNode globalReference) {
			super();
			this.tree = tree;
			this.startPos = tree.pos;
			this.endPos = tree.pos; // simplified
			this.compilationUnit = (JCCompilationUnit) globalReference.top().get();
			this.astContext = globalReference.getContext();
		}
	}
}
