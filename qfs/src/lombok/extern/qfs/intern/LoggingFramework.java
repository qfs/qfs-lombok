/*
 * Copyright (C) 2019 Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum LoggingFramework {
	QFLOG(lombok.extern.qfs.QFLog.class,
			"de.qfs.lib.log.QFLogger",
			"de.qfs.lib.log.Log",
			"MTD", new String[] {"ERR", "ERRDETAIL", "WRN", "WRNDETAIL", "MSG", "MSGDETAIL", "MTD", "MTDDETAIL", "DBG", "DBGDETAIL"},
			"qflog", new String[] {"add", "addDetail", "addDetails", "detail", "details", "addAll", "all", "dump", "dumpStack", "dumpstack", "log", "params"});

	private final Class<? extends Annotation> annotationClass;
	private final String loggerTypeName;
	private final String logTypeName;
	private final String mtdLevelConstant;
	private final String logMethodName;
	private final String[] loggingConstants;
	private final String[] validQFLogMethodNames;

	public static enum LogCallArgumentType {
		STANDARD,
		DETAIL,
		ALL
	}

	LoggingFramework(Class<? extends Annotation> annotationClass,
			String loggerTypeName,
			String logTypeName,
			String mtdLevelConstant, String[] loggingConstants,
			String logMethodName, String[] validQFLogMethodNames) {

		this.annotationClass = annotationClass;
		this.loggerTypeName = loggerTypeName;
		this.logTypeName = logTypeName;
		this.mtdLevelConstant = mtdLevelConstant;
		this.loggingConstants = sortStringArray(loggingConstants);
		this.logMethodName = logMethodName;
		this.validQFLogMethodNames = sortStringArray(validQFLogMethodNames);
	}

	private String[] sortStringArray(String[] input) {
		List<String> inputAsList = Arrays.asList(input);
		Collections.sort(inputAsList);
		return inputAsList.toArray(new String[inputAsList.size()]);
	}


	public final Class<? extends Annotation> getAnnotationClass() {
		return annotationClass;
	}

	public String[] getValidQFLogMethodNames() {
		return validQFLogMethodNames;
	}

	public final String getLoggerTypeName() {
		return loggerTypeName;
	}

	public final String getMtdLevelConstant() {
		return mtdLevelConstant;
	}

	public String[] getLoggingConstants() {
		return loggingConstants;
	}

	public final String getLogMethodName() {
		return logMethodName;
	}

	public final String getLogTypeName() {
		return logTypeName;
	}

	public LogCallArgumentType getAddType(String methodName) {
		if (methodName != null) {
			if (methodName.startsWith("de") || methodName.startsWith("addD")) return LogCallArgumentType.DETAIL;
			if (methodName.startsWith("all") || methodName.startsWith("addA")) return LogCallArgumentType.ALL;
		}
		return LogCallArgumentType.STANDARD;
	}

	public boolean isDumpStackCall(String methodName) {
		return (methodName != null) && (methodName.startsWith("dump"));
	}

	public boolean isParamsCall(String methodName) {
		return (methodName != null) && (methodName.startsWith("param"));
	}
}