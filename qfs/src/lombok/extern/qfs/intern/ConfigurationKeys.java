/*
 * Copyright (C) 2019 The Project Lombok Authors & Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern;

import java.util.List;

import lombok.core.configuration.ConfigurationKey;
import lombok.core.configuration.FlagUsageType;

public class ConfigurationKeys {

	public static final ConfigurationKey<FlagUsageType> LOG_QFLOG_FLAG_USAGE = new ConfigurationKey<FlagUsageType>("lombok.log.qflog.flagUsage", "Emit a warning or error if @QFLog is used.") {};
	public static final ConfigurationKey<List<String>> LOG_QFLOG_DEFAULT_SKIP_METHODS = new ConfigurationKey<List<String>>("lombok.log.qflog.defaultSkipMtdNames", "List of methods to skip mtd debug call generation by default.") {};

	public static final ConfigurationKey<FlagUsageType> PERFLOG_FLAG_USAGE = new ConfigurationKey<FlagUsageType>("lombok.perfLog.flagUsage", "Emit a warning or error if @Perf is used.") {};
	public static final ConfigurationKey<String> PERFLOG_LOG_CLASS = new ConfigurationKey<String>("lombok.perfLog.logClass", "Specify the name of a class from which is singleton is used as receiver of the performance log method calls.") {};

	public static final ConfigurationKey<FlagUsageType> QFNOTIFY_FLAG_USAGE = new ConfigurationKey<FlagUsageType>("lombok.qfnotify.flagUsage", "Emit a warning or error if @QFNotify is used.") {};
	public static final ConfigurationKey<String> QFNOTIFY_AUTOCOMPLETE_SPECS = new ConfigurationKey<String>("lombok.qfnotify.autoCompleteSpecs", "Commy-separated list of auto-completed Notifier-Specification") {};
	public static final ConfigurationKey<FlagUsageType> QFNOTIFIERSPEC_FLAG_USAGE = new ConfigurationKey<FlagUsageType>("lombok.qfnotifierspec.flagUsage", "Emit a warning or error if @QFNotifierSpec is used.") {};

}