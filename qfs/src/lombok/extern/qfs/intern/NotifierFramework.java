/*
 * Copyright (C) 2019 Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import lombok.core.AnnotationValues;

public enum NotifierFramework {
	QFNOTIFY(lombok.extern.qfs.QFNotify.class,lombok.extern.qfs.QFNotifierSpec.class,
			"de.qfs.lib.notifications.Notifier", "de.qfs.lib.notifications.Notifier",
			"de.qfs.lib.notifications.NotificationName",
			"FUNDAMENTAL",
			new String[] {"FUNDAMENTAL",
						"FUNDAMENTAL_DETAILS",
						"BASIC",
						"BASIC_DETAILS",
						"MESSAGE",
						"MESSAGE_DETAILS",
						"CONFIG",
						"CONFIG_DETAILS",
						"DEBUG",
						"DEBUG_DETAILS"},
			"qfnotify",
			"NOTIFIER",
			"POST_LEVEL",
			"de.qfs.lib.notifications.Notifier.Default");

	private final Class<? extends Annotation> annotationClass;
	private final Class<? extends Annotation> specAnnotationClass;
	private final String notifierTypeName;
	private final String notifierInstanceTypeName;
	private final String notificationNameTypeName;
	private final String defaultLevelConstant;
	private final String[] levelConstants;
	private final String postMethodName;
	private final String notifierConstName;
	private final String postLevelConstName;
	private final Map<String, String> autocompleteSpecs;

	NotifierFramework(Class<? extends Annotation> annotationClass,
			Class<? extends Annotation> specAnnotationClass,
			String notifierTypeName, String notifierInstanceTypeName,
			String notificationNameTypeName,
			String defaultLevelConstant, String[] levelConstants,
			String postMethodName,
			String notifierConstName,
			String postLevelConstName,
			String defaultNotifierSpecClass) {

		this.annotationClass = annotationClass;
		this.specAnnotationClass = annotationClass;
		this.notifierTypeName = notifierTypeName;
		this.notifierInstanceTypeName = notifierInstanceTypeName;
		this.notificationNameTypeName = notificationNameTypeName;
		this.defaultLevelConstant = defaultLevelConstant;
		this.levelConstants = levelConstants;
		this.postMethodName = postMethodName;
		this.notifierConstName = notifierConstName;
		this.postLevelConstName = postLevelConstName;
		this.autocompleteSpecs = new HashMap<String, String>();
		autocompleteSpecs.put(null, defaultNotifierSpecClass);
		autocompleteSpecs.put("", defaultNotifierSpecClass);
		addAutocompleteSpecs(defaultNotifierSpecClass);
	}

	public final Class<? extends Annotation> getAnnotationClass() {
		return annotationClass;
	}

	public final Class<? extends Annotation> getSpecAnnotationClass() {
		return specAnnotationClass;
	}

	public final String getTypeName() {
		return notifierTypeName;
	}

	public final String getInstanceTypeName() {
		return notifierInstanceTypeName;
	}

	public final String getNotificationNameTypeName() {
		return notificationNameTypeName;
	}

	public final String getDefaultLevelConstant() {
		return defaultLevelConstant;
	}

	public String[] getLevelConstants() {
		return levelConstants;
	}

	public final String getPostMethodName() {
		return postMethodName;
	}

	public final String getNotifierConstName() {
		return notifierConstName;
	}

	public final String getPostLevelConstName() {
		return postLevelConstName;
	}

	public int getLevel(final String levelString) {
		try {
			return Integer.valueOf(levelString);
		} catch (final NumberFormatException e) {
		}

		int i = 1;
		for (final String constant : getLevelConstants()) {
			if (levelString.endsWith(constant)) {
				return i;
			}
			i++;
		}

		return 0;
	}

	public int getLevel(final AnnotationValues<? extends java.lang.annotation.Annotation> annotation) {
		String levelString = annotation.getRawExpression("level");
		if (levelString == null) {
			levelString = getDefaultLevelConstant();
		}

		return getLevel(levelString);
	}

	public void addAutocompleteSpecs(final String specsString) {
		if (specsString == null) return;
		final String[] specs = specsString.split(",");
		for (String spec: specs) {
			spec = spec.trim();
			if ("".equals(spec)) continue;

			final String name = CommonHandlerUtil.getLastPart(spec);
			if ("".equals(spec)) continue;

			autocompleteSpecs.put(name, spec);
		}
	}

	public String expandNotifierClassName(final String notifierClassName) {
		final String expandedName = autocompleteSpecs.get(notifierClassName);
		return expandedName == null ? notifierClassName : expandedName;
	}

	public String getFieldNameForNotificationName(String name) {
		name = name.substring(0,1).replaceAll("[^A-Za-z]", "_") + name.substring(1).replaceAll("[^A-Za-z0-9]", "_");
		name = name.replaceAll("([a-z])([A-Z])", "$1_$2");
		name = name.replaceAll("_+", "_");
		name = name.toUpperCase();
		return name;
	}
}