/*
 * Copyright (C) 2019 Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern.eclipse;

import org.eclipse.jdt.internal.compiler.ASTVisitor;
import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.Block;
import org.eclipse.jdt.internal.compiler.ast.ConstructorDeclaration;
import org.eclipse.jdt.internal.compiler.ast.DoStatement;
import org.eclipse.jdt.internal.compiler.ast.ForStatement;
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement;
import org.eclipse.jdt.internal.compiler.ast.IfStatement;
import org.eclipse.jdt.internal.compiler.ast.LabeledStatement;
import org.eclipse.jdt.internal.compiler.ast.MethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.SwitchStatement;
import org.eclipse.jdt.internal.compiler.ast.WhileStatement;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.ClassScope;

public class StatementModifiyingVisitor extends ASTVisitor {
	public Statement visitStatement(Statement messageSend) {
		return null;
	}

	@Override public boolean visit(MethodDeclaration methodDeclaration, ClassScope scope) {
		traverseStatements(methodDeclaration, methodDeclaration.statements, methodDeclaration.scope);
		return false;
	}

	@Override public boolean visit(Block block, BlockScope scope) {
		traverseStatements(block, block.statements, scope);
		return false;
	}

	@Override public boolean visit(ConstructorDeclaration constructorDeclaration, ClassScope scope) {
		traverseStatements(constructorDeclaration, constructorDeclaration.statements, constructorDeclaration.scope);
		return false;
	}

	@Override public boolean visit(DoStatement doStatement, BlockScope scope) {
		if (doStatement.action != null) {
			Statement replacement = visitStatement(doStatement.action);
			if (replacement != null) {
				doStatement.action = replacement;
			} else {
				doStatement.action.traverse(this, scope);
			}
		}
		doStatement.condition.traverse(this, scope);

		return false;
	}

	@Override public boolean visit(ForeachStatement forEachStatement, BlockScope scope) {
		forEachStatement.elementVariable.traverse(this, scope);
		if (forEachStatement.collection != null) {
			forEachStatement.collection.traverse(this, scope);
		}

		if (forEachStatement.action != null) {
			Statement replacement = visitStatement(forEachStatement.action);
			if (replacement != null) {
				forEachStatement.action = replacement;
			} else {
				forEachStatement.action.traverse(this, scope);
			}
		}
		return false;
	}

	@Override public boolean visit(ForStatement forStatement, BlockScope scope) {

		if (forStatement.initializations != null) {
			int initializationsLength = forStatement.initializations.length;
			for (int i = 0; i < initializationsLength; i++)
				forStatement.initializations[i].traverse(this, scope);
		}

		if (forStatement.condition != null)
			forStatement.condition.traverse(this, scope);

		if (forStatement.increments != null) {
			int incrementsLength = forStatement.increments.length;
			for (int i = 0; i < incrementsLength; i++)
				forStatement.increments[i].traverse(this, scope);
		}

		if (forStatement.action != null) {
			Statement replacement = visitStatement(forStatement.action);
			if (replacement != null) {
				forStatement.action = replacement;
			} else {
				forStatement.action.traverse(this, scope);
			}
		}
		return false;
	}

	@Override public boolean visit(IfStatement ifStatement, BlockScope scope) {
		ifStatement.condition.traverse(this, scope);

		if (ifStatement.thenStatement != null) {
			Statement replacement = visitStatement(ifStatement.thenStatement);
			if (replacement != null) {
				ifStatement.thenStatement = replacement;
			} else {
				ifStatement.thenStatement.traverse(this, scope);
			}
		}

		if (ifStatement.elseStatement != null) {
			Statement replacement = visitStatement(ifStatement.elseStatement);
			if (replacement != null) {
				ifStatement.elseStatement = replacement;
			} else {
				ifStatement.elseStatement.traverse(this, scope);
			}
		}

		return false;
	}

	@Override public boolean visit(LabeledStatement labeledStatement, BlockScope scope) {
		if (labeledStatement.statement != null) {
			Statement replacement = visitStatement(labeledStatement.statement);
			if (replacement != null) {
				labeledStatement.statement = replacement;
			} else {
				labeledStatement.statement.traverse(this, scope);
			}
		}
		return false;
	}

	@Override public boolean visit(SwitchStatement switchStatement, BlockScope scope) {
		switchStatement.expression.traverse(this, scope);

		traverseStatements(switchStatement, switchStatement.statements, scope);

		return false;
	}

	@Override public boolean visit(WhileStatement whileStatement, BlockScope scope) {
		whileStatement.condition.traverse(this, scope);

		if (whileStatement.action != null) {
			Statement replacement = visitStatement(whileStatement.action);
			if (replacement != null) {
				whileStatement.action = replacement;
			} else {
				whileStatement.action.traverse(this, scope);
			}
		}
		return false;
	}

	private void traverseStatements(ASTNode node, Statement[] statements, BlockScope scope) {
		if (statements != null) {
			for (int i = 0; i < statements.length; i++) {
				Statement replacement = visitStatement(statements[i]);
				if (replacement != null) {
					statements[i] = replacement;
				} else {
					statements[i].traverse(this, scope);
				}
			}
		}
	}
}