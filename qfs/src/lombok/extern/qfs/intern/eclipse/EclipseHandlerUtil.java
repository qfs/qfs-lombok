/*
 * Copyright (C) 2019 The Project Lombok Authors & Quality First Software GmbH.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.extern.qfs.intern.eclipse;

import static lombok.eclipse.Eclipse.fromQualifiedName;
import static lombok.eclipse.handlers.EclipseHandlerUtil.*;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.AllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.Argument;
import org.eclipse.jdt.internal.compiler.ast.BinaryExpression;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.MarkerAnnotation;
import org.eclipse.jdt.internal.compiler.ast.MemberValuePair;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.NameReference;
import org.eclipse.jdt.internal.compiler.ast.NormalAnnotation;
import org.eclipse.jdt.internal.compiler.ast.OperatorIds;
import org.eclipse.jdt.internal.compiler.ast.QualifiedAllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.QualifiedNameReference;
import org.eclipse.jdt.internal.compiler.ast.QualifiedTypeReference;
import org.eclipse.jdt.internal.compiler.ast.ReturnStatement;
import org.eclipse.jdt.internal.compiler.ast.SingleTypeReference;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.StringLiteral;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.jdt.internal.compiler.util.Util;

import lombok.core.AST.Kind;
import lombok.eclipse.Eclipse;
import lombok.eclipse.EclipseNode;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.NotifierFramework;

public class EclipseHandlerUtil {

	public static int ASTNode_OperatorMASK;
	public static int ASTNode_OperatorSHIFT;

	static {
		// bit mask changed in May 2020 by https://git.eclipse.org/r/c/jdt/eclipse.jdt.core/+/162886, so we fetch it dynamically
		try {
			ASTNode_OperatorMASK = ASTNode.class.getDeclaredField("OperatorMASK").getInt(null);
			ASTNode_OperatorSHIFT = ASTNode.class.getDeclaredField("OperatorSHIFT").getInt(null);
		} catch (final Exception ex) {
			ASTNode_OperatorMASK = ASTNode.OperatorMASK;
			ASTNode_OperatorSHIFT = ASTNode.OperatorSHIFT;
		}
	}

	public static Annotation[] addNormalAnnotation(ASTNode source, Annotation[] originalAnnotationArray, char[][] annotationTypeFqn, MemberValuePair[] memberValuePairs) {
		char[] simpleName = annotationTypeFqn[annotationTypeFqn.length - 1];

		if (originalAnnotationArray != null) for (Annotation ann : originalAnnotationArray) {
			char[] lastToken = null;

			if (ann.type instanceof QualifiedTypeReference) {
				char[][] t = ((QualifiedTypeReference) ann.type).tokens;
				lastToken = t[t.length - 1];
			} else if (ann.type instanceof SingleTypeReference) {
				lastToken = ((SingleTypeReference) ann.type).token;
			}

			if (lastToken != null && Arrays.equals(simpleName, lastToken)) return originalAnnotationArray;
		}

		int pS = source.sourceStart, pE = source.sourceEnd;
		long p = (long)pS << 32 | pE;
		long[] poss = new long[annotationTypeFqn.length];
		Arrays.fill(poss, p);
		QualifiedTypeReference qualifiedType = new QualifiedTypeReference(annotationTypeFqn, poss);
		setGeneratedBy(qualifiedType, source);
		NormalAnnotation ann = new NormalAnnotation(qualifiedType, pS);
		ann.declarationSourceEnd = pE;
		ann.memberValuePairs = memberValuePairs;

		setGeneratedBy(ann, source);
		if (originalAnnotationArray == null) return new Annotation[] { ann };
		Annotation[] newAnnotationArray = new Annotation[originalAnnotationArray.length + 1];
		System.arraycopy(originalAnnotationArray, 0, newAnnotationArray, 0, originalAnnotationArray.length);
		newAnnotationArray[originalAnnotationArray.length] = ann;
		return newAnnotationArray;
	}

	public static void addPostDietAnnotation(EclipseNode annotationNode,
			final Class<? extends java.lang.annotation.Annotation> annotationClass) {
		final ASTNode astNode = annotationNode.up().get();
		if (astNode instanceof TypeDeclaration) {
			final TypeDeclaration typeDeclaration = (TypeDeclaration) astNode;
			Annotation[] annotations = typeDeclaration.annotations;
			typeDeclaration.annotations = addPostDietAnnotation(annotationNode, annotationClass, annotations);
		} else {
			final AbstractMethodDeclaration methodDeclaration = (AbstractMethodDeclaration) astNode;
			Annotation[] annotations = methodDeclaration.annotations;
			methodDeclaration.annotations = addPostDietAnnotation(annotationNode, annotationClass, annotations);
		}
	}

	public static Annotation[] addPostDietAnnotation(EclipseNode annotationNode,
				final Class<? extends java.lang.annotation.Annotation> annotationClass,
				Annotation[] originalAnnotationArray) {
			ASTNode source = annotationNode.up().get();

			final ASTNode srcAnnotation = annotationNode.get();
			final char[][] fromQualifiedName = Eclipse.fromQualifiedName(annotationClass.getCanonicalName());

			if (srcAnnotation instanceof MarkerAnnotation) {
				return addAnnotation(source, originalAnnotationArray, fromQualifiedName, null);
	//		} else if (srcAnnotation instanceof SingleMemberAnnotation) { Does not exists in QF@Log
			}

			MemberValuePair[] srcPairs = null;
			if (srcAnnotation instanceof NormalAnnotation) {
				srcPairs = ((NormalAnnotation) srcAnnotation).memberValuePairs;
			}
			// double usage of srcPairs okay, since it's removed before compile time
			return EclipseHandlerUtil.addNormalAnnotation(source, originalAnnotationArray, fromQualifiedName, srcPairs);
		}

	public static boolean annotationStringContains(ASTNode astNode, final Class<? extends java.lang.annotation.Annotation> annotationType, final String expectedAnnotationPart, final EclipseNode anyNode) {
		Annotation annot = getAnnotation(astNode, annotationType, anyNode);
		if (annot == null) {
			return false;
		}
		final String annotationAsString = annot.toString();
		return annotationAsString.indexOf(expectedAnnotationPart) > -1;
	}

	public static NameReference createNameReference(String name, ASTNode source) {
		int pS = source.sourceStart, pE = source.sourceEnd;
		long p = (long) pS << 32 | pE;

		char[][] nameTokens = fromQualifiedName(name);
		long[] pos = new long[nameTokens.length];
		Arrays.fill(pos, p);

		QualifiedNameReference nameReference = new QualifiedNameReference(nameTokens, pos, pS, pE);
		nameReference.statementEnd = pE;

		setGeneratedBy(nameReference, source);
		return nameReference;
	}

	public static TypeReference createTypeReference(String typeName, ASTNode source) {
		int pS = source.sourceStart, pE = source.sourceEnd;
		long p = (long) pS << 32 | pE;

		TypeReference typeReference;
		if (typeName.contains(".")) {

			char[][] typeNameTokens = fromQualifiedName(typeName);
			long[] pos = new long[typeNameTokens.length];
			Arrays.fill(pos, p);

			typeReference = new QualifiedTypeReference(typeNameTokens, pos);
		} else {
			typeReference = new SingleTypeReference(typeName.toCharArray(), p);
		}

		setGeneratedBy(typeReference, source);
		return typeReference;
	}

	public static String extractConstructorName(AllocationExpression allocationExpression) {
		// String based hack..
		String expression = allocationExpression.toString();
		int spaceIdx = expression.indexOf(' ');
		int bracketIdx = expression.indexOf('(');
		if (spaceIdx < 0 || bracketIdx < 0 || bracketIdx < spaceIdx) return "";
		return expression.substring(spaceIdx+1,bracketIdx);
	}

	public static Annotation getAnnotation(ASTNode astNode, final Class<? extends java.lang.annotation.Annotation> annotationType, final EclipseNode anyNode) {
		Annotation[] annotations = null;
		if (astNode instanceof AbstractMethodDeclaration) {
			annotations = ((AbstractMethodDeclaration) astNode).annotations;
		} else if (astNode instanceof TypeDeclaration) {
			annotations = ((TypeDeclaration) astNode).annotations;
		}
		if (annotations == null) {
			return null;
		}
		for (Annotation annot: annotations) {
			if (typeMatches(annotationType, anyNode, annot.type)) {
				return annot;
			}
		}
		return null;
	}

	public static EclipseNode getEnclosingTypeDeclaration(EclipseNode node) {
		while (node != null && !(node.get() instanceof TypeDeclaration)) {
			node = node.up();
		}
		return node;
	}

	public static FieldDeclaration getField(final String fieldName, EclipseNode node) {
		while (node != null && !(node.get() instanceof TypeDeclaration)) {
			node = node.up();
		}

		if (node != null && node.get() instanceof TypeDeclaration) {
			TypeDeclaration typeDecl = (TypeDeclaration) node.get();
			if (typeDecl.fields != null) for (FieldDeclaration def : typeDecl.fields) {
				char[] fName = def.name;
				if (fName == null) continue;
				if (fieldName.equals(new String(fName))) {
					return def;
				}
			}
		}

		return null;
	}

	public static String getFullQualifiedClassName(EclipseNode owner) {
		StringBuilder sb = new StringBuilder();
		prefixFullQualifiedClassName(owner, sb);
		return sb.toString();
	}

	public static int getLineNumber(int pS, EclipseNode annotationNode) {
		int[] lineEnds = null;
		CompilationUnitDeclaration cud = (CompilationUnitDeclaration) annotationNode.top().get();
		return pS >= 0 ? Util.getLineNumber(pS, lineEnds = cud.compilationResult.getLineSeparatorPositions(), 0, lineEnds.length-1) : 0;
	}

	public static String getMethodSignature(AbstractMethodDeclaration declaration) {
		StringBuilder sb = new StringBuilder();
		sb.append(declaration.selector);
		sb.append("(");
		int i = 0;
		final Argument[] arguments = declaration.arguments;
		if (arguments != null) {
			for (Argument arg : arguments) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append(arg.type.getLastToken());
				for (int j = 0; j < arg.type.dimensions(); j++) {
					sb.append("[]");
				}
				i++;
			}
		}
		sb.append(")");

		return sb.toString();
	}

	public static EclipseNode getOutmostClass(EclipseNode owner) {
		final EclipseNode compilationUnit = owner.top();
		EclipseNode outmostClass = owner;
		while (outmostClass.up() != compilationUnit) {
			outmostClass = outmostClass.up();
		}
		return outmostClass;
	}

	public static AllocationExpression getParentAsAllocationExpression(EclipseNode node) {
		ASTNode astNode = node.directUp().get();
		if (astNode instanceof ReturnStatement) {
			astNode = ((ReturnStatement) astNode).expression;
			if (astNode instanceof QualifiedAllocationExpression) {
				return (QualifiedAllocationExpression) astNode;
			}
		}
		while (astNode instanceof MessageSend) { // direct call of method of anonymous class
			astNode = ((MessageSend) astNode).receiver;
		}
		if (astNode instanceof AllocationExpression) {
			return (AllocationExpression) astNode;
		}
		return null;
	}

	public static String getTypeName(NotifierFramework framework, EclipseNode sourceNode) {
		final String fullQualifiedName = framework.getTypeName();
		final String simpleName = CommonHandlerUtil.getLastPart(fullQualifiedName);
		if (fullQualifiedName.equals(sourceNode.getImportList().getFullyQualifiedNameForSimpleName(simpleName))) {
			// is in imports
			return simpleName;
		} else {
			return fullQualifiedName;
		}
	}

	public static boolean hasAnnotation(ASTNode astNode, final Class<? extends java.lang.annotation.Annotation> annotationType, final EclipseNode anyNode) {
		return EclipseHandlerUtil.getAnnotation(astNode, annotationType, anyNode) != null;
	}

	public static boolean onInterface(final EclipseNode owner, final EclipseNode annotationNode, final String annotationName) {
		if (isInterface(owner)) {
			annotationNode.addError(annotationName + " is legal only on classes, enums, methods and constructors.");
			return true;
		}
		return false;
	}

	public static boolean isInterface(final EclipseNode owner) {
		TypeDeclaration typeDecl = null;
		if (owner.get() instanceof TypeDeclaration) typeDecl = (TypeDeclaration) owner.get();
		int modifiers = typeDecl == null ? 0 : typeDecl.modifiers;

		boolean notAClass = (modifiers & (ClassFileConstants.AccInterface | ClassFileConstants.AccAnnotation)) != 0;

		return typeDecl == null || notAClass;
	}

	public static boolean outerClassIsStatic(EclipseNode owner) {
		while (owner != null && (owner.get() instanceof TypeDeclaration)) {
			boolean staticDeclared = (((TypeDeclaration) owner.get()).modifiers & Modifier.STATIC) != 0;
			boolean topLevelClass = (owner.up() == owner.top());
			if (!(staticDeclared || topLevelClass)) {
				return false;
			}
			owner = owner.up();
		}
		return true;
	}

	public static void prefixFullQualifiedClassName(EclipseNode owner, StringBuilder sb) {
		String name = null;
		final ASTNode astNode = owner.get();
		if (astNode instanceof TypeDeclaration) {
			name = new String(((TypeDeclaration)astNode).name);
		} else if (astNode instanceof AbstractMethodDeclaration) {
			name = getMethodSignature((AbstractMethodDeclaration) astNode);
		} else {
			String packageDeclaration = owner.getPackageDeclaration();
			if (packageDeclaration != null) {
				sb.append(packageDeclaration);
			}
			return;
		}

		prefixFullQualifiedClassName(owner.up(), sb);
		if (sb.length() > 0) {
			sb.append(".");
		}

		if (name != null && name.length() > 0) {
			sb.append(name);
		} else {
			sb.append("?");
		}
	}

	public static Statement[] prependStatement(final Statement statement, final Statement[] currentStatements) {
		if (currentStatements == null) {
			return new Statement[] {statement};
		} else {
			Statement[] newStatements = new Statement[currentStatements.length + 1];
			System.arraycopy(currentStatements, 0, newStatements, 1, currentStatements.length);
			newStatements[0] = statement;
			return newStatements;
		}
	}

	//actually function is used nowhere
	public static void removeAnnotationParameter(final EclipseNode annotationNode, final String paramName) {
		ASTNode astNode = annotationNode.get();
		if (! (astNode instanceof NormalAnnotation)) {
			annotationNode.addWarning("Cannot remove parameter " + paramName + " from " + astNode.getClass());
			return;
		}
		if (paramName == null || "value".equals(paramName)) {
			annotationNode.addWarning("Cannot remove parameter " + paramName);
			return;
		}
		final NormalAnnotation annotation = (NormalAnnotation) astNode;
		final MemberValuePair[] originalMemberValuePairs = annotation.memberValuePairs;
		int paramIndex = -1;
		final int numberOfPairs = originalMemberValuePairs.length;
		for (int i = 0; i < numberOfPairs; i++) {
			if (paramName.equals(new String(originalMemberValuePairs[i].name))) {
				paramIndex = i;
				break;
			}
		}
		if (paramIndex == -1) {
			return;
		}
		final MemberValuePair[] newMemberValuePairs = new MemberValuePair[numberOfPairs-1];

		if (paramIndex > 0) System.arraycopy(originalMemberValuePairs, 0, newMemberValuePairs, 0, paramIndex);
		if (paramIndex < numberOfPairs - 1) System.arraycopy(originalMemberValuePairs, paramIndex + 1, newMemberValuePairs, paramIndex, numberOfPairs - paramIndex - 1);

		annotation.memberValuePairs = newMemberValuePairs;
	}

	public static Annotation[] removePostDietAnnotation(final Class<? extends java.lang.annotation.Annotation> annotationClass, Annotation[] originalAnnotationArray) {

		String annotationName = annotationClass.getCanonicalName();

		List<Annotation> annotations = new ArrayList<Annotation>();

		for (Annotation ann : originalAnnotationArray) {
			if (!annotationName.equals(ann.type.toString())) {
				annotations.add(ann);
			}
		}
		return annotations.toArray(new Annotation[annotations.size()]);
	}

	public static void removePostDietAnnotation(EclipseNode annotationNode, final Class<? extends java.lang.annotation.Annotation> annotationClass) {
		final ASTNode astNode = annotationNode.up().get();
		if (astNode instanceof TypeDeclaration) {
			final TypeDeclaration typeDeclaration = (TypeDeclaration) astNode;
			Annotation[] annotations = typeDeclaration.annotations;
			typeDeclaration.annotations = removePostDietAnnotation(annotationClass, annotations);
		} else {
			final AbstractMethodDeclaration methodDeclaration = (AbstractMethodDeclaration) astNode;
			Annotation[] annotations = methodDeclaration.annotations;
			methodDeclaration.annotations = removePostDietAnnotation(annotationClass, annotations);
		}

	}

	public static boolean isStringLiteral(final Expression arg) {
		if (arg == null) {
			return false;
		}
		if (arg instanceof StringLiteral) {
			return true;
		}
		if (! (arg instanceof BinaryExpression)) {
			return false;
		}
		if (! isPlusExpression(arg)) {
			return false;
		}

		final BinaryExpression binaryArg = (BinaryExpression) arg;
		return isStringLiteral(binaryArg.left) || isStringLiteral(binaryArg.right);
	}

	public static boolean isPlusExpression(final Expression arg) {
		final int operatorId = ((arg.bits & ASTNode_OperatorMASK) >> ASTNode_OperatorSHIFT);
		return operatorId == OperatorIds.PLUS;
	}

	public static boolean isTypeOrMethod(Kind kind) {
		return Kind.TYPE.equals(kind) || Kind.METHOD.equals(kind);
	}

	private EclipseHandlerUtil() {
		//Prevent instantiation
	}
}