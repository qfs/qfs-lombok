/*
 * Copyright (C) 2016 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.eclipse.handlers;

import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createNameReference;

import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.Block;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.LocalDeclaration;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.NameReference;
import org.eclipse.jdt.internal.compiler.ast.QualifiedTypeReference;
import org.eclipse.jdt.internal.compiler.ast.SingleNameReference;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.StringLiteral;
import org.eclipse.jdt.internal.compiler.ast.TryStatement;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.MethodScope;
import org.eclipse.jdt.internal.compiler.lookup.TypeConstants;

import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.eclipse.DeferUntilPostDiet;
import lombok.eclipse.EclipseAnnotationHandler;
import lombok.eclipse.EclipseNode;
import lombok.extern.qfs.intern.ConfigurationKeys;
import lombok.extern.qfs.intern.PerformanceLoggingFramework;
import lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil;
import lombok.spi.Provides;

/**
 * Handles the {@code lombok.Cleanup} annotation for eclipse.
 */
public class HandlePerfLog {

	private HandlePerfLog() {
		throw new UnsupportedOperationException();
	}

	public static void processAnnotation(final AnnotationValues<lombok.extern.qfs.PerfLog>  annotation, final EclipseNode annotationNode) {
		final EclipseNode owner = annotationNode.up();

		final Object tagExpression = annotation.getActualExpression("value");
		final boolean reset = annotation.getInstance().reset();

		switch (owner.getKind()) {
		case TYPE:
		case METHOD:
			wrapMethod(annotationNode, (Expression) tagExpression, reset);
			owner.rebuild();
			break;
		default:
			annotationNode.addError("@Perf is legal only on methods or constructors.");
			break;
		}
	}

	private static void wrapMethod(final EclipseNode annotationNode, Expression tagExpression, final boolean reset) {
		final EclipseNode methodNode = annotationNode.up();
		final AbstractMethodDeclaration declaration = (AbstractMethodDeclaration) methodNode.get();

		int pS = annotationNode.get().sourceStart, pE = annotationNode.get().sourceEnd;
		long p = (long) pS << 32 | pE;

		if (tagExpression == null) {
			tagExpression = new StringLiteral("".toCharArray(), pS, pE, 0);
		}

		final char[] tagVarName = "$performanceLogTagName".toCharArray();

		LocalDeclaration localLevelArgumentDeclaration = new LocalDeclaration(tagVarName, pS, pE);
		localLevelArgumentDeclaration.modifiers |= ClassFileConstants.AccFinal;
		localLevelArgumentDeclaration.type = new QualifiedTypeReference(TypeConstants.JAVA_LANG_STRING, new long[] {p, p, p});
		localLevelArgumentDeclaration.initialization = tagExpression;



		final Statement beginPeriodInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.BEGIN_PERIOD_METHOD_NAME, new SingleNameReference(tagVarName, p));
		addBeginPerfLogCallToMethodBody(declaration, beginPeriodInvocation);

		if (reset) {
			final Statement resetInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.RESET_METHOD_NAME);
			addBeginPerfLogCallToMethodBody(declaration, resetInvocation);
		}

		final Statement endPeriodInvocation = callPerformanceLogMethod(annotationNode, PerformanceLoggingFramework.END_PERIOD_METHOD_NAME, new SingleNameReference(tagVarName, p));
		wrapBodyInTryCatch(annotationNode, declaration, localLevelArgumentDeclaration, endPeriodInvocation);
	}

	private static void addBeginPerfLogCallToMethodBody(final AbstractMethodDeclaration declaration, final Statement beginPeriodInvocation) {
		declaration.statements = EclipseHandlerUtil.prependStatement(beginPeriodInvocation, declaration.statements);
	}

	private static void wrapBodyInTryCatch(final EclipseNode annotationNode, final AbstractMethodDeclaration declaration, final Statement localLevelArgumentDeclaration, final Statement endPeriodInvocation) {
		final ASTNode source = annotationNode.get();
		final SetGeneratedByVisitor setGeneratedByVisitor = new SetGeneratedByVisitor(source);

		final Block tryBlock = new Block(declaration.explicitDeclarations);
		tryBlock.statements = declaration.statements;
		tryBlock.scope = declaration.scope;
		setGeneratedByVisitor.visit(tryBlock, (BlockScope) null);

		final Block finallyBlock = new Block(0);
		finallyBlock.statements = new Statement[]{endPeriodInvocation};
		finallyBlock.scope = declaration.scope;
		setGeneratedByVisitor.visit(finallyBlock, (BlockScope) null);

		final TryStatement tryStatement = new TryStatement();
		tryStatement.tryBlock = tryBlock;
		tryStatement.finallyBlock = finallyBlock;
		setGeneratedByVisitor.visit(tryStatement, (BlockScope) null);

		declaration.statements = new Statement[]{localLevelArgumentDeclaration,tryStatement};
	}


	private static Statement callPerformanceLogMethod(final EclipseNode annotationNode, final String methodName, final Expression... args) {
		final ASTNode source = annotationNode.get();
		int pS = source.sourceStart, pE = source.sourceEnd;

		final MessageSend perfLogReceiver = new MessageSend();
		final NameReference perfLogClassRef = createNameReference(getPerfClassName(annotationNode), source);

		perfLogReceiver.receiver = perfLogClassRef;
		perfLogReceiver.selector = PerformanceLoggingFramework.INSTANCE_METHOD.toCharArray();
		perfLogReceiver.arguments = new Expression[0];

		final MessageSend logCall = new MessageSend();
		logCall.receiver = perfLogReceiver;
		logCall.selector = methodName.toCharArray();
		logCall.arguments = new Expression[args.length];
		for (int i = 0; i < args.length; i++) {
			Expression arg = args[i];
			if (arg == null) {
				arg = new StringLiteral(("").toCharArray(), pS, pE, 0);
			}
			logCall.arguments[i] = arg;
		}

		logCall.traverse(new SetGeneratedByVisitor(annotationNode.get()), (MethodScope) null);

		return logCall;
	}

	private static String getPerfClassName(final EclipseNode annotationNode) {
		final String configuredClassName = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.PERFLOG_LOG_CLASS);
		if (configuredClassName != null) return configuredClassName;

		return PerformanceLoggingFramework.CLASS_NAME;
	}

	@Provides
	@HandlerPriority(value = 2048)
	@DeferUntilPostDiet
	public static class HandlePerfAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.PerfLog> {
		@Override public void handle(AnnotationValues<lombok.extern.qfs.PerfLog> annotation, Annotation source, EclipseNode annotationNode) {
			try {
				handleFlagUsage(annotationNode, ConfigurationKeys.PERFLOG_FLAG_USAGE, "@Perf");

				processAnnotation(annotation, annotationNode);
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}

	}
}
