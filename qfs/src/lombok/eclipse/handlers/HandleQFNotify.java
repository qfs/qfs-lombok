/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.eclipse.handlers;

import static lombok.core.AST.Kind.TYPE;
import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.eclipse.handlers.EclipseHandlerUtil.isGenerated;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.BinaryExpression;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.IfStatement;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.NameReference;
import org.eclipse.jdt.internal.compiler.ast.OperatorIds;
import org.eclipse.jdt.internal.compiler.ast.QualifiedNameReference;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.ClassScope;
import org.eclipse.jdt.internal.compiler.lookup.MethodScope;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.eclipse.DeferUntilPostDiet;
import lombok.eclipse.EclipseAnnotationHandler;
import lombok.eclipse.EclipseNode;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.NotifierFramework;
import lombok.extern.qfs.intern.__AutoNotifyAnnotation;
import lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil;
import lombok.extern.qfs.intern.eclipse.StatementModifiyingVisitor;
import lombok.spi.Provides;

public class HandleQFNotify {

	private HandleQFNotify() {
		throw new UnsupportedOperationException();
	}

	public static void processNotifyAnnotation(final NotifierFramework framework, final EclipseNode annotationNode) {
		final EclipseNode owner = annotationNode.up();

		if (TYPE.equals(owner.getKind())) {
			final String autoCompleteSpecs = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFY_AUTOCOMPLETE_SPECS);
			framework.addAutocompleteSpecs(autoCompleteSpecs);
			if (annotationNode.isCompleteParse()) { // already in post diet
				processQfNotifyAnnotationPostDiet(framework, annotationNode);
			} else {
				EclipseHandlerUtil.addPostDietAnnotation(annotationNode, __AutoNotifyAnnotation.class);
			}
			owner.rebuild();
		}
	}

	/*
	public static void addNotifierField(final NotifierFramework framework, final Annotation source,
			final EclipseNode owner, final EclipseNode annotationNode, final String notifierClassName) {

		final String notifierFieldName = getFieldName(notifierClassName);
		boolean useStatic = true;

		EclipseNode outmostClass = EclipseHandlerUtil.getOutmostClass(owner);
		boolean isTopLevel = (outmostClass == owner);

		EclipseNode typeToAddLogger = outmostClass;
		{
			typeToAddLogger = EclipseHandlerUtil.getEnclosingTypeDeclaration(owner);
			isTopLevel = (typeToAddLogger == owner);
		}

		final MemberExistsResult fieldExists = fieldExists(notifierFieldName, typeToAddLogger);
		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
			if (isTopLevel) {
				annotationNode.addWarning("Field '" + notifierFieldName + "' already exists.");
			}
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			useStatic = useStatic && EclipseHandlerUtil.outerClassIsStatic(typeToAddLogger);

			FieldDeclaration fieldDeclaration = createField(framework, source, typeToAddLogger, notifierFieldName, useStatic, notifierClassName);
			fieldDeclaration.traverse(new SetGeneratedByVisitor(source), null);
			injectField(typeToAddLogger, fieldDeclaration);
			if (typeToAddLogger != owner) {
				typeToAddLogger.rebuild();
			}
		}
	}
	private static FieldDeclaration createField(NotifierFramework framework, Annotation source, EclipseNode owner, String logFieldName, boolean useStatic, String notifierClassName) {
		int pS = 0, pE = 0;
		FieldDeclaration fieldDecl = new FieldDeclaration(logFieldName.toCharArray(), 0, -1);
		setGeneratedBy(fieldDecl, source);
		fieldDecl.declarationSourceEnd = -1;
		fieldDecl.modifiers = Modifier.PRIVATE | (useStatic ? Modifier.STATIC : 0) | Modifier.FINAL;

		fieldDecl.type = createTypeReference(framework.getTypeName(), source);
		fieldDecl.type.sourceStart = pS;
		fieldDecl.type.sourceEnd = fieldDecl.type.statementEnd = pE;

		MessageSend loggerCreator = new MessageSend();
		setGeneratedBy(loggerCreator, source);
		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		loggerCreator.receiver = createNameReference(framework.getInstanceTypeName(), source);
		loggerCreator.receiver.sourceStart = pS;
		loggerCreator.receiver.sourceEnd = loggerCreator.receiver.statementEnd = pE;

		loggerCreator.selector = "instance".toCharArray();

		notifierClassName = expandNotifierClassName(notifierClassName);

		Expression topicExpression = createNameReference(notifierClassName+".TOPIC", source);
		Expression postLevelExpression = createNameReference(notifierClassName+".LEVEL", source);

		loggerCreator.arguments = new Expression[] {topicExpression, postLevelExpression};

		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		fieldDecl.initialization = loggerCreator;

		return fieldDecl;
	}

*/


 	/**
	 * Handles the {@link lombok.extern.qfs.QFNotify} annotation for Eclipse.
	 */
	@Provides
	@HandlerPriority(value = 650)
	public static class HandleQFNotifyAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.QFNotify> {
		@Override
		public void handle(final AnnotationValues<lombok.extern.qfs.QFNotify> annotation, final Annotation source, final EclipseNode annotationNode) {
			try {
				handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFY_FLAG_USAGE, "@QFNotify");
				processNotifyAnnotation(NotifierFramework.QFNOTIFY, annotationNode);
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}
	}

	// ---------------- Post diet ----------------

	public static void processQfNotifyAnnotationPostDiet(NotifierFramework framework, EclipseNode annotationNode) {
		EclipseNode owner = annotationNode.up();

		boolean[] rebuildRequired = new boolean[1];
		rebuildRequired[0] = false;

		if (isTypeOrMethod(owner.getKind())) {
			EclipseHandlerUtil.removePostDietAnnotation(annotationNode, __AutoNotifyAnnotation.class);

			traverseSource(framework, owner, annotationNode, rebuildRequired);

			if (rebuildRequired[0]) {
				owner.rebuild();
			}
		}
	}

	private static void traverseSource(final NotifierFramework framework,
									final EclipseNode owner,
									final EclipseNode annotationNode,
									final boolean[] rebuildRequired) {
		final String postMethodName = framework.getPostMethodName();

		StatementModifiyingVisitor visitor = new StatementModifiyingVisitor() {

			@Override
			public boolean visit(MessageSend messageSend, BlockScope scope) {
				if (! isQFNotifyMethodCall(postMethodName,messageSend)) return true;
				// this should not happen...
				annotationNode.addError("qfnotify() call at an invalid location: " + messageSend);
				return false;
			}

			@Override
			public Statement visitStatement(Statement statement) {
				if (!(statement instanceof MessageSend)) {
					return null;
				}
				MessageSend messageSend = (MessageSend) statement;
				final boolean isQFNotifyCall = isQFNotifyMethodCall(postMethodName,messageSend);

				if (! isQFNotifyCall) return null;

				rebuildRequired[0] = true;

				Statement wrappingBlock = generateQFNotifyCall(framework, messageSend);

				wrappingBlock.traverse(new SetGeneratedByVisitor(messageSend), (MethodScope) null);
				return wrappingBlock;
			}

		};

		ASTNode astNode = owner.get();
		if (astNode instanceof AbstractMethodDeclaration) {
			((AbstractMethodDeclaration) astNode).traverse(visitor, (ClassScope) null);
		} else if (astNode instanceof TypeDeclaration) {
			((TypeDeclaration) astNode).traverse(visitor, (ClassScope) null);
		}

	}

	private static boolean isQFNotifyMethodCall(final String expectedMethodName, MessageSend messageSend) {

		final String methodName = new String(messageSend.selector);

		if (messageSend.receiver instanceof ThisReference) {
			if ((((ThisReference) messageSend.receiver).bits & ASTNode.IsImplicitThis) == 0) return false;
		} else if (messageSend.receiver != null) return false;
		if (!methodName.equals(expectedMethodName)) return false;
		if (messageSend.arguments == null) return false;
		if (!(messageSend.arguments.length > 0)) return false;
		if (isGenerated(messageSend)) return false;
		return true;
	}

	private static Statement generateQFNotifyCall(NotifierFramework framework, final MessageSend messageSend) {

		ASTNode source = messageSend;
		int pS = source.sourceStart, pE = source.sourceEnd;
		long p = (long) pS << 32 | pE;

		Expression firstOriginalArgument = messageSend.arguments[0];

		String notifierSpecClass = null;
		int parameterStartIndex = 0;
		if (! EclipseHandlerUtil.isStringLiteral(firstOriginalArgument)) { // not a shortcut for Default notifications
			notifierSpecClass = firstOriginalArgument.toString();
			final String lastPart = CommonHandlerUtil.getLastPart(notifierSpecClass);
			final boolean isConstant = lastPart.equals(lastPart.toUpperCase());
			if ((firstOriginalArgument instanceof QualifiedNameReference) && isConstant) {
				notifierSpecClass = CommonHandlerUtil.getFirstParts(notifierSpecClass);
			} else {
				parameterStartIndex++;
			}
		}
		notifierSpecClass = framework.expandNotifierClassName(notifierSpecClass);

		final String notifierRef = notifierSpecClass + "." + framework.getNotifierConstName();
		final String postLevelRef = notifierSpecClass + "." + framework.getPostLevelConstName();

		final Expression levelExpression = createNameReference(postLevelRef, source);

		final List<Expression> parameterList = new ArrayList<Expression>();
		parameterList.add(levelExpression);
		for (int i = parameterStartIndex; i < messageSend.arguments.length; i++) {
			parameterList.add(messageSend.arguments[i]);
		}

		final MessageSend postCall = new MessageSend();
		final NameReference notifierFieldReference = createNameReference(notifierRef, source);
		postCall.receiver = notifierFieldReference;
		postCall.selector = "postNotification".toCharArray();
		postCall.arguments = parameterList.toArray(new Expression[parameterList.size()]);
		postCall.nameSourcePosition = p;
		postCall.sourceStart = pS;
		postCall.sourceEnd = postCall.statementEnd = pE;

		final NameReference levelReference = createNameReference(notifierRef + ".level", source);
		final Expression logLevelComparator = createNameReference(levelExpression.toString(),source);

		BinaryExpression equalExpression = new BinaryExpression(levelReference, logLevelComparator, OperatorIds.GREATER_EQUAL);
		equalExpression.sourceStart = pS;
		equalExpression.statementEnd = equalExpression.sourceEnd = pE;

		return new IfStatement(equalExpression, postCall, 0, 0);
	}

	/**
	 * Handles the {@link lombok.extern.qfs.QFNotify} annotation for Eclipse - Post diet.
	 */
	@Provides
	@DeferUntilPostDiet
	@HandlerPriority(value = 660)
	public static class HandleQFNotifyPostDietAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.intern.__AutoNotifyAnnotation> {
		@Override
		public void handle(AnnotationValues<lombok.extern.qfs.intern.__AutoNotifyAnnotation> annotation, Annotation source, EclipseNode annotationNode) {
			try {
				processQfNotifyAnnotationPostDiet(NotifierFramework.QFNOTIFY, annotationNode);
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}

	}
}
