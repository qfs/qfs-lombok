/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.eclipse.handlers;

import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.eclipse.handlers.EclipseHandlerUtil.*;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.*;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createNameReference;

import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.QualifiedTypeReference;
import org.eclipse.jdt.internal.compiler.ast.StringLiteral;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.lookup.TypeConstants;
import org.eclipse.jdt.internal.compiler.lookup.TypeIds;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.eclipse.Eclipse;
import lombok.eclipse.EclipseAnnotationHandler;
import lombok.eclipse.EclipseNode;
import lombok.eclipse.handlers.EclipseHandlerUtil.MemberExistsResult;
import lombok.extern.qfs.intern.NotifierFramework;
import lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil;
import lombok.spi.Provides;

public class HandleQFNotifierSpec {

	private HandleQFNotifierSpec() {
		throw new UnsupportedOperationException();
	}

	public static void processNotifyAnnotation(final NotifierFramework framework,
			final AnnotationValues<? extends java.lang.annotation.Annotation> annotation,
			final Annotation source, final EclipseNode annotationNode, String topicName, final String[] notificationNames) {
		final EclipseNode owner = annotationNode.up();

		final int postLevel = framework.getLevel(annotation);

		switch (owner.getKind()) {
		case TYPE:
			if (EclipseHandlerUtil.onInterface(owner, annotationNode, "@QFNotifierSpec")) {
				return;
			};

			topicName = addNotifierField(framework, source, owner, annotationNode, topicName);
			addPostLevelField(framework, source, owner, annotationNode, postLevel);
			for (final String notificationName: notificationNames) {
				if (notificationName == null) continue;
				if ("".equals(notificationName)) continue;
				addNameField(framework, source, owner, annotationNode, notificationName, topicName);
			}

			owner.rebuild();
			break;
		default:
			break;
		}
	}

	public static String addNotifierField(final NotifierFramework framework, final Annotation source,
			final EclipseNode owner, final EclipseNode annotationNode, String topic) {

		final String notifierFieldName = framework.getNotifierConstName();


		EclipseNode typeToAddLogger = EclipseHandlerUtil.getEnclosingTypeDeclaration(owner);

		final MemberExistsResult fieldExists = fieldExists(notifierFieldName, typeToAddLogger);
		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
				annotationNode.addWarning("Field '" + notifierFieldName + "' already exists.");
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			final String[] topicArray = new String[] {topic};
			FieldDeclaration fieldDeclaration = createNotifierField(framework, source, typeToAddLogger, notifierFieldName, topicArray);
			topic = topicArray[0];
			fieldDeclaration.traverse(new SetGeneratedByVisitor(source), null);
			injectField(typeToAddLogger, fieldDeclaration);
			if (typeToAddLogger != owner) {
				typeToAddLogger.rebuild();
			}
		}
		return topic;
	}

	private static FieldDeclaration createNotifierField(NotifierFramework framework, Annotation source, EclipseNode owner, String notifierFieldName, final String[] topicArray) {
		int pS = 0, pE = 0;

		FieldDeclaration fieldDecl = new FieldDeclaration(notifierFieldName.toCharArray(), 0, -1);
		setGeneratedBy(fieldDecl, source);
		fieldDecl.declarationSourceEnd = -1;
		fieldDecl.modifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;

		fieldDecl.type = lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createTypeReference(framework.getTypeName(), source);
		fieldDecl.type.sourceStart = pS;
		fieldDecl.type.sourceEnd = fieldDecl.type.statementEnd = pE;

		MessageSend loggerCreator = new MessageSend();
		setGeneratedBy(loggerCreator, source);
		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		loggerCreator.receiver = createNameReference(framework.getInstanceTypeName(), source);
		loggerCreator.receiver.sourceStart = pS;
		loggerCreator.receiver.sourceEnd = loggerCreator.receiver.statementEnd = pE;

		loggerCreator.selector = "instance".toCharArray();

		String parameterString;
		String topic = topicArray[0];
		if (topic == null || topic.trim().length() == 0) {
			parameterString = getFullQualifiedClassName(owner);
		} else {
			parameterString = topic;
		}
		topicArray[0] = parameterString; //second return value...

		Expression parameter = new StringLiteral(parameterString.toCharArray(), pS, pE, 0);

		loggerCreator.arguments = new Expression[] {parameter};

		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		fieldDecl.initialization = loggerCreator;

		return fieldDecl;
	}

	public static void addPostLevelField(final NotifierFramework framework, final Annotation source,
			final EclipseNode owner, final EclipseNode annotationNode, final int postLevel) {

		final String postLevelFieldName = framework.getPostLevelConstName();

		EclipseNode typeToAddLogger = EclipseHandlerUtil.getEnclosingTypeDeclaration(owner);

		final MemberExistsResult fieldExists = fieldExists(postLevelFieldName, typeToAddLogger);
		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
				annotationNode.addWarning("Field '" + postLevelFieldName + "' already exists.");
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			FieldDeclaration fieldDeclaration = createPostLevelField(framework, source, typeToAddLogger, postLevelFieldName, postLevel);
			fieldDeclaration.traverse(new SetGeneratedByVisitor(source), null);
			injectField(typeToAddLogger, fieldDeclaration);
			if (typeToAddLogger != owner) {
				typeToAddLogger.rebuild();
			}
		}
	}

	private static FieldDeclaration createPostLevelField(NotifierFramework framework, Annotation source, EclipseNode owner, String postLevelFieldName, int postLevel) {
		int pS = 0, pE = 0;

		FieldDeclaration fieldDecl = new FieldDeclaration(postLevelFieldName.toCharArray(), 0, -1);
		setGeneratedBy(fieldDecl, source);
		fieldDecl.declarationSourceEnd = -1;
		fieldDecl.modifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;

		fieldDecl.type = TypeReference.baseTypeReference(TypeIds.T_int, pS);
		fieldDecl.type.sourceStart = pS;
		fieldDecl.type.sourceEnd = fieldDecl.type.statementEnd = pE;

		Expression levelExpression = makeIntLiteral(("" + postLevel).toCharArray(), source);

		fieldDecl.initialization = levelExpression;

		return fieldDecl;
	}


	public static String addNameField(final NotifierFramework framework, final Annotation source,
			final EclipseNode owner, final EclipseNode annotationNode, final String notificationName, String topic) {

		String notificationNameFieldName = framework.getFieldNameForNotificationName(notificationName);

		EclipseNode typeToAddLogger = EclipseHandlerUtil.getEnclosingTypeDeclaration(owner);

		final MemberExistsResult fieldExists = fieldExists(notificationNameFieldName, typeToAddLogger);
		if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			FieldDeclaration fieldDeclaration = createNameField(framework, source, typeToAddLogger, notificationNameFieldName, notificationName, topic);
			fieldDeclaration.traverse(new SetGeneratedByVisitor(source), null);
			injectField(typeToAddLogger, fieldDeclaration);
			if (typeToAddLogger != owner) {
				typeToAddLogger.rebuild();
			}
		} else {
			annotationNode.addWarning("Field '" + notificationNameFieldName + "' already exists.");
		}
		return notificationNameFieldName;
	}

	private static FieldDeclaration createNameField(NotifierFramework framework, Annotation source, EclipseNode owner, String notifierFieldName, String notificationName, final String topic) {
		int pS = 0, pE = 0;
		long p = (long)pS << 32 | pE;

		FieldDeclaration fieldDecl = new FieldDeclaration(notifierFieldName.toCharArray(), 0, -1);
		setGeneratedBy(fieldDecl, source);
		fieldDecl.declarationSourceEnd = -1;
		fieldDecl.modifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;

		fieldDecl.type = new QualifiedTypeReference(TypeConstants.JAVA_LANG_STRING, new long[] {p, p, p});
		fieldDecl.type.sourceStart = pS;
		fieldDecl.type.sourceEnd = fieldDecl.type.statementEnd = pE;

		notificationName = topic + "." + notificationName;

		fieldDecl.initialization = new StringLiteral(notificationName.toCharArray(), pS, pE, 0);

		final char[][] annotationFQN = Eclipse.fromQualifiedName(framework.getNotificationNameTypeName());
		fieldDecl.annotations = addAnnotation(source, fieldDecl.annotations, annotationFQN, null);

		return fieldDecl;
	}

 	/**
	 * Handles the {@link lombok.extern.qfs.QFNotifierSpec} annotation for Eclipse.
	 */
	@Provides
	@HandlerPriority(value = 580)
	public static class HandleQFNotifyAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.QFNotifierSpec> {
		@Override public void handle(final AnnotationValues<lombok.extern.qfs.QFNotifierSpec> annotation, final Annotation source, final EclipseNode annotationNode) {
			try {
				handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.QFNOTIFIERSPEC_FLAG_USAGE, "@QFNotifier");

				processNotifyAnnotation(NotifierFramework.QFNOTIFY, annotation, source, annotationNode, annotation.getInstance().value(), annotation.getInstance().names());
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}
	}


}
