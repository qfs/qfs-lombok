/*
 * Copyright (C) 2015 Quality First Software GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package lombok.eclipse.handlers;

import static lombok.core.handlers.HandlerUtil.handleFlagUsage;
import static lombok.eclipse.handlers.EclipseHandlerUtil.*;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.*;
import static lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createNameReference;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.AllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.BinaryExpression;
import org.eclipse.jdt.internal.compiler.ast.Block;
import org.eclipse.jdt.internal.compiler.ast.ConstructorDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.FieldReference;
import org.eclipse.jdt.internal.compiler.ast.IfStatement;
import org.eclipse.jdt.internal.compiler.ast.LocalDeclaration;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.MethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.OperatorIds;
import org.eclipse.jdt.internal.compiler.ast.QualifiedAllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.SingleNameReference;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.StringLiteral;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.ClassScope;
import org.eclipse.jdt.internal.compiler.lookup.CompilationUnitScope;
import org.eclipse.jdt.internal.compiler.lookup.MethodScope;
import org.eclipse.jdt.internal.compiler.lookup.TypeIds;
import lombok.ConfigurationKeys;
import lombok.core.AnnotationValues;
import lombok.core.HandlerPriority;
import lombok.core.configuration.IdentifierName;
import lombok.eclipse.DeferUntilPostDiet;
import lombok.eclipse.EclipseAnnotationHandler;
import lombok.eclipse.EclipseNode;
import lombok.eclipse.handlers.EclipseHandlerUtil.MemberExistsResult;
import lombok.extern.qfs.intern.CommonHandlerUtil;
import lombok.extern.qfs.intern.LoggingFramework;
import lombok.extern.qfs.intern.LoggingFramework.LogCallArgumentType;
import lombok.extern.qfs.intern.__AutoLogAnnotation;
import lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil;
import lombok.extern.qfs.intern.eclipse.StatementModifiyingVisitor;
import lombok.spi.Provides;

public class HandleQFLog {

	private HandleQFLog() {
		throw new UnsupportedOperationException();
	}

	public static void processLogAnnotation(final LoggingFramework framework,
											final AnnotationValues<? extends java.lang.annotation.Annotation> annotation,
											final Annotation source,
											final EclipseNode annotationNode,
											final String loggerTopic,
											final boolean ownLogger,
											final String[] skipMethods,
											final boolean ignoreExistingLogger,
											String loggerTypeName) {
		final EclipseNode owner = annotationNode.up();

		if (isTypeOrMethod(owner.getKind())) {
			if ("".equals(loggerTypeName)) {
				loggerTypeName = framework.getLoggerTypeName();
			}
			addLoggerField(source, owner, annotationNode, loggerTopic, loggerTypeName, ownLogger, ignoreExistingLogger);

			if (annotationNode.isCompleteParse()) { // already in post diet
				processQfLogAnnotationPostDiet(framework, annotation, source, annotationNode, skipMethods);
			} else {
				addPostDietAnnotation(annotationNode, __AutoLogAnnotation.class);
				// addLogMockElements(framework, annotationNode, owner);
			}
			owner.rebuild();
		}
	}

	public static void addLoggerField(final Annotation source,
									final EclipseNode owner,
									final EclipseNode annotationNode,
									final String loggerTopic,
									final String loggerTypeName,
									final boolean ownLogger,
									final boolean ignoreExistingLogger) {

		final String logFieldName = getLogFieldName(annotationNode);

		boolean useStatic = !Boolean.FALSE.equals(annotationNode.getAst().readConfiguration(ConfigurationKeys.LOG_ANY_FIELD_IS_STATIC));

		EclipseNode outmostClass = EclipseHandlerUtil.getOutmostClass(owner);
		boolean isTopLevel = (outmostClass == owner);

		EclipseNode typeToAddLogger = outmostClass;
		if (ownLogger) {
			typeToAddLogger = EclipseHandlerUtil.getEnclosingTypeDeclaration(owner);
			isTopLevel = (typeToAddLogger == owner);
		}

		final MemberExistsResult fieldExists = fieldExists(logFieldName, typeToAddLogger);
		if (fieldExists == MemberExistsResult.EXISTS_BY_USER) {
			if (isTopLevel && !ignoreExistingLogger) {
				annotationNode.addWarning("Field '" + logFieldName + "' already exists.");
			}
		} else if (fieldExists == MemberExistsResult.NOT_EXISTS) {
			useStatic = useStatic && EclipseHandlerUtil.outerClassIsStatic(typeToAddLogger);
			final boolean usePublic = isInterface(typeToAddLogger);

			FieldDeclaration fieldDeclaration = createField(source, typeToAddLogger, logFieldName, loggerTypeName, useStatic, usePublic, loggerTopic);
			// keep source positions at (0,0), so no: fieldDeclaration.traverse(new SetGeneratedByVisitor(source), null);
			injectField(typeToAddLogger, fieldDeclaration);
			if (typeToAddLogger != owner) {
				typeToAddLogger.rebuild();
			}
		}
	}

	protected static String getLogFieldName(EclipseNode annotationNode) {
		IdentifierName logFieldNameConfig = annotationNode.getAst().readConfiguration(ConfigurationKeys.LOG_ANY_FIELD_NAME);
		String logFieldName = null;
		if (logFieldNameConfig != null) {
			logFieldName = logFieldNameConfig.getName();
		}
		if (logFieldName == null) logFieldName = "logger";
		return logFieldName;
	}

	private static FieldDeclaration createField(Annotation source,
												EclipseNode owner,
												String logFieldName,
												String loggerTypeName,
												boolean useStatic,
												final boolean usePublic,
												String loggerTopic) {
		int pS = 0, pE = 0;
		FieldDeclaration fieldDecl = new FieldDeclaration(logFieldName.toCharArray(), 0, -1);
		setGeneratedBy(fieldDecl, source);
		fieldDecl.declarationSourceEnd = -1;
		fieldDecl.modifiers = (usePublic ? Modifier.PUBLIC : Modifier.PRIVATE) | (useStatic ? Modifier.STATIC : 0) | Modifier.FINAL;

		fieldDecl.type = lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createTypeReference(loggerTypeName, source);
		fieldDecl.type.sourceStart = pS;
		fieldDecl.type.sourceEnd = fieldDecl.type.statementEnd = pE;

		AllocationExpression loggerCreator = new AllocationExpression();
		setGeneratedBy(loggerCreator, source);
		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		loggerCreator.type = lombok.extern.qfs.intern.eclipse.EclipseHandlerUtil.createTypeReference(loggerTypeName, source);
		loggerCreator.type.sourceStart = pS;
		loggerCreator.type.sourceEnd = loggerCreator.type.statementEnd = pE;

		String parameterString;
		if (loggerTopic == null || loggerTopic.trim().length() == 0) {
			parameterString = getFullQualifiedClassName(owner);
		} else {
			parameterString = loggerTopic;
		}
		Expression parameter = new StringLiteral(parameterString.toCharArray(), pS, pE, 0);

		loggerCreator.arguments = new Expression[] {parameter};
		loggerCreator.sourceStart = pS;
		loggerCreator.sourceEnd = loggerCreator.statementEnd = pE;

		fieldDecl.initialization = loggerCreator;

		return fieldDecl;
	}

	public static String getFullQualifiedClassName(final EclipseNode owner) {
		return new DynamicWrapping(owner, null).getFullQulifiedNameBuilder().toString();
	}

//	private static void addLogMockElements(LoggingFramework framework, EclipseNode annotationNode, EclipseNode owner) {
//		EclipseNode outmostClass = EclipseHandlerUtil.getOutmostClass(owner);
//		if (!(outmostClass.get() instanceof TypeDeclaration)) return;
//
//		boolean rebuildRequired = false;
//
////		rebuildRequired = addLogMockInterface(framework, owner, annotationNode, outmostClass) || rebuildRequired;
//		rebuildRequired = addLogMockMethod(framework, owner, annotationNode, outmostClass) || rebuildRequired;
//
//		if (rebuildRequired && outmostClass != owner) {
//			outmostClass.rebuild();
//		}
//	}

//	LogMockInterface not evaluated
//
//	private static boolean addLogMockInterface(LoggingFramework framework, EclipseNode owner, EclipseNode sourceNode, EclipseNode outmostClass) {
//		final String logMockInterfaceName = getLogMockInterfaceName();
//		if (interfaceExists(logMockInterfaceName, outmostClass) != MemberExistsResult.NOT_EXISTS) return false;
//
//		TypeDeclaration interf = createLogMockInterface((TypeDeclaration) outmostClass.get(), logMockInterfaceName, sourceNode);
//		injectType(outmostClass, interf);
//		return false;
//	}
//
//	protected static String getLogMockInterfaceName() {
//		return "__LogBuilderInterface";
//	}
//
//	private static TypeDeclaration createLogMockInterface(TypeDeclaration typeDeclaration, String logMockInterfaceName, EclipseNode sourceNode) {
//		final ASTNode source = sourceNode.get();
//
//		TypeDeclaration interf = new TypeDeclaration(typeDeclaration.compilationResult);
//		interf.bits |= Eclipse.ECLIPSE_DO_NOT_TOUCH_FLAG;
//		interf.modifiers |= ClassFileConstants.AccPrivate;
//		interf.modifiers |= ClassFileConstants.AccStatic;
//		interf.modifiers |= ClassFileConstants.AccInterface;
//		interf.name = logMockInterfaceName.toCharArray();
//		interf.traverse(new SetGeneratedByVisitor(source), (ClassScope) null);
//
//		interf.methods = new AbstractMethodDeclaration[VALID_QFLOG_METHOD_NAMES.length];
//		for (int i = 0; i < VALID_QFLOG_METHOD_NAMES.length; i++) {
//			final String methodName = VALID_QFLOG_METHOD_NAMES[i];
//			interf.methods[i] = createLogMockMethod(interf, methodName, false, false, sourceNode);
//		}
//		return interf;
//	}
//
//	public static MemberExistsResult interfaceExists(String interfaceName, EclipseNode node) {
//		while (node != null && !(node.get() instanceof TypeDeclaration)) {
//			node = node.up();
//		}
//
//		if (node != null && node.get() instanceof TypeDeclaration) {
//			TypeDeclaration typeDecl = (TypeDeclaration) node.get();
//
//			if (typeDecl.memberTypes != null) {
//				for (TypeDeclaration decl : typeDecl.memberTypes) {
//					if (interfaceName.equals(new String(decl.name))) {
//						return getGeneratedBy(decl) == null ? MemberExistsResult.EXISTS_BY_USER : MemberExistsResult.EXISTS_BY_LOMBOK;
//					}
//				}
//			}
//		}
//
//		return MemberExistsResult.NOT_EXISTS;
//	}

//	private static boolean addLogMockMethod(LoggingFramework framework, EclipseNode owner, EclipseNode sourceNode, EclipseNode outmostClass) {
//
//		final String logMethodName = framework.getLogMethodName();
//
//		if (methodExists(logMethodName, outmostClass, true, -1) != MemberExistsResult.NOT_EXISTS) return false;
//
//		MethodDeclaration method = createLogMockMethod(framework, (TypeDeclaration) outmostClass.get(), logMethodName, true, true, sourceNode);
//
//		injectMethod(outmostClass, method);
//		return true;
//	}
//
//	private static MethodDeclaration createLogMockMethod(LoggingFramework framework, TypeDeclaration typeDeclaration, String logMethodName, boolean useLevelParameter, boolean generateBody, EclipseNode sourceNode) {
//		ASTNode source = sourceNode.get();
//		int pS = source.sourceStart, pE = source.sourceEnd;
//		long p = (long) pS << 32 | pE;
//
//		MethodDeclaration method = new MethodDeclaration(typeDeclaration.compilationResult);
//
//		if (generateBody) {
//			method.modifiers = ClassFileConstants.AccPrivate | ClassFileConstants.AccStatic;
//		}
//
////		final String logMockInterfaceName = getLogMockInterfaceName();
////		method.returnType = new SingleTypeReference(logMockInterfaceName.toCharArray(), p);
//
//		method.returnType = TypeReference.baseTypeReference(TypeIds.T_void, 0);
//		method.returnType.sourceStart = pS;
//		method.returnType.sourceEnd = pE;
//
//		method.selector = logMethodName.toCharArray();
//		method.thrownExceptions = null;
//		method.typeParameters = null;
//
//		method.bits |= ECLIPSE_DO_NOT_TOUCH_FLAG;
//
//
//		TypeReference objectRef = new ArrayQualifiedTypeReference(TypeConstants.JAVA_LANG_OBJECT, 1, new long[] {0, 0, 0});
//		objectRef.bits |= ASTNode.IsVarArgs;
//
//		Argument messageParam = new Argument("messageParameters".toCharArray(), p, objectRef, Modifier.FINAL);
//		messageParam.sourceStart = pS;
//		messageParam.sourceEnd = pE;
//
//		if (useLevelParameter) {
//			final TypeReference baseTypeReference = TypeReference.baseTypeReference(TypeIds.T_int, 0);
//
//			Argument levelParam = new Argument("level".toCharArray(), p, baseTypeReference, Modifier.FINAL);
//			levelParam.sourceStart = pS;
//			levelParam.sourceEnd = pE;
//			method.arguments = new Argument[] {levelParam, messageParam};
//		} else {
//			method.arguments = new Argument[] {messageParam};
//		}
//		method.bodyStart = method.declarationSourceStart = method.sourceStart = source.sourceStart;
//		method.bodyEnd = method.declarationSourceEnd = method.sourceEnd = source.sourceEnd;
//
////		if (generateBody) {
////			final ReturnStatement returnStatement = new ReturnStatement(new NullLiteral(pS, pE), pS, pE);
////			method.statements = new Statement[]{ returnStatement};
////		} else {
//			method.statements = null;
////		}
//
//		if (generateBody) {
//			method.annotations = addQFLogMtdSkipAnnotation(framework, sourceNode, method, new Annotation[0]);
//		}
//
//		// makes things worse... method.traverse(new SetGeneratedByVisitor(source), (ClassScope) null);
//		return method;
//	}

//	private static Annotation[] addQFLogMtdSkipAnnotation(LoggingFramework framework, EclipseNode node, ASTNode source, Annotation[] originalAnnotationArray) {
//		final MemberValuePair arg = new MemberValuePair("skipMtd".toCharArray(), 0, 0, new TrueLiteral(0, 0));
//		return addAnnotation(source, originalAnnotationArray, Eclipse.fromQualifiedName(framework.getAnnotationClass().getCanonicalName()), arg);
//	}

	/**
	 * Handles the {@link lombok.extern.qfs.QFLog} annotation for Eclipse.
	 */
	@Provides
	@HandlerPriority(value = 500) // < 2^9= 512; Must be run before NonNullhandler
	public static class HandleQFLogAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.QFLog> {
		@Override public void handle(final AnnotationValues<lombok.extern.qfs.QFLog> annotation, final Annotation source, final EclipseNode annotationNode) {
			try {
				handleFlagUsage(annotationNode, lombok.extern.qfs.intern.ConfigurationKeys.LOG_QFLOG_FLAG_USAGE, "@QFLog", ConfigurationKeys.LOG_ANY_FLAG_USAGE, "any @Log");

				final String topic = annotation.getInstance().topic();
				final boolean ownLogger = annotation.getInstance().ownLogger();
				final String[] skipMethods = annotation.getInstance().skipMtdNames();
				final boolean ignoreExistingLogger = annotation.getInstance().ignoreExistingLogger();
				final String loggerClass = annotation.getRawExpression("loggerClass");
				final String loggerTypeName = loggerClass == null ? "" : loggerClass.replaceAll("(?:Void)?\\.class", "");

				processLogAnnotation(LoggingFramework.QFLOG, annotation, source, annotationNode, topic, ownLogger, skipMethods, ignoreExistingLogger, loggerTypeName);
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}
	}

	// ---------------- Post diet ----------------

	public static void processQfLogAnnotationPostDiet(LoggingFramework framework,
													AnnotationValues<? extends java.lang.annotation.Annotation> annotation,
													Annotation source,
													EclipseNode annotationNode,
													String[] skipMethods) {
		EclipseNode owner = annotationNode.up();

		boolean[] rebuildRequired = new boolean[1];
		rebuildRequired[0] = false;

		String logFieldName;
		if (isTypeOrMethod(owner.getKind())) {
			removePostDietAnnotation(annotationNode, __AutoLogAnnotation.class);

			skipMethods = expandSkipMethodsFromConfiguration(annotationNode, skipMethods);

			logFieldName = getLogFieldName(annotationNode);
			traverseSource(framework, owner, annotationNode, logFieldName, skipMethods, rebuildRequired);

			if (rebuildRequired[0]) {
				owner.rebuild();
			}
		}
	}

	protected static String[] expandSkipMethodsFromConfiguration(EclipseNode annotationNode, String[] skipMethods) {
		if (skipMethods.length == 0) {
			java.util.List<String> defaultSkipMethodList = annotationNode.getAst().readConfiguration(lombok.extern.qfs.intern.ConfigurationKeys.LOG_QFLOG_DEFAULT_SKIP_METHODS);
			if (defaultSkipMethodList == null) {
				skipMethods = new String[0];
			} else {
				skipMethods = defaultSkipMethodList.toArray(new String[defaultSkipMethodList.size()]);
			}
		}
		return skipMethods;
	}

	private abstract static class Wrapping {
		final Wrapping wrapping;
		boolean hasLogField = false;

		public Wrapping(Wrapping wrapping) {
			super();
			this.wrapping = wrapping;
		}

		public StringBuilder getFullQulifiedNameBuilder() {
			StringBuilder sb = null;
			if (!hasLogField && wrapping != null && !wrapping.hasLogField) {
				sb = wrapping.getFullQulifiedNameBuilder();
			} else {
				sb = new StringBuilder();
			}
			return sb;
		}

		public abstract boolean shouldSkipMethod(final LoggingFramework framework, final EclipseNode annotationNode);

		protected boolean shouldSkipMethodOnNode(final LoggingFramework framework, final ASTNode astNode, final EclipseNode annotationNode) {
			if (hasSkipMtdAnnotationTrue(framework, astNode, annotationNode)) {
				return true;
			}
			if (hasSkipMtdAnnotationFalse(framework, astNode, annotationNode)) {
				return false;
			}
			if (wrapping != null) {
				return wrapping.shouldSkipMethod(framework, annotationNode);
			}
			return false;
		}
	}

	private static class DynamicWrapping extends Wrapping {
		final EclipseNode startNode;
		final String logFieldName;
		String fullQualifiedName; // cache

		public DynamicWrapping(EclipseNode node, String logFieldName) {
			super(null);
			this.startNode = node;
			this.logFieldName = logFieldName;
		}

		private EclipseNode getNextRelevantParent(EclipseNode node) {
			while (node != null) {
				final ASTNode astNode = node.get();

				if ((astNode instanceof TypeDeclaration) || (astNode instanceof AbstractMethodDeclaration) || (astNode instanceof AllocationExpression)) {
					return node;
				}
				node = node.directUp();
			}
			return null;
		}

		@Override
		public StringBuilder getFullQulifiedNameBuilder() {
			if (fullQualifiedName != null) {
				return new StringBuilder(fullQualifiedName);
			}
			EclipseNode node = getNextRelevantParent(startNode);
			StringBuilder sb = null;
			if (node != null) {
				ASTNode astNode = node.get();
				EclipseNode parentNode = node.directUp();
				if (astNode instanceof AbstractMethodDeclaration) {
					sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
					MethodContext.appendSignature(sb, (AbstractMethodDeclaration) astNode);
				} else if (astNode instanceof TypeDeclaration) {
					if (logFieldName == null || fieldExists(logFieldName, node) == MemberExistsResult.NOT_EXISTS) {
						final AllocationExpression parentAsAllocationExpression = EclipseHandlerUtil.getParentAsAllocationExpression(node);
						if (parentAsAllocationExpression != null) {
							parentNode = parentNode.directUp(); // skip one level
						}
						sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
						TypeDeclaration td = (TypeDeclaration) astNode;
						TypeContext.appendSignature(sb, td, parentAsAllocationExpression);
					}
				} else if (astNode instanceof AllocationExpression) {
					sb = new DynamicWrapping(parentNode, logFieldName).getFullQulifiedNameBuilder();
					TypeContext.appendSignature(sb, (AllocationExpression) astNode);
				}
			}
			if (sb == null) {
				sb = new StringBuilder();
				if (logFieldName == null) {
					final String packageDeclaration = (startNode != null) ? startNode.getPackageDeclaration() : "";
					if (packageDeclaration != null) {
						sb.append(packageDeclaration);
					}
				}
			}
			fullQualifiedName = sb.toString();
			return sb;
		}

		@Override public boolean shouldSkipMethod(LoggingFramework framework, EclipseNode annotationNode) {
			EclipseNode node = getNextRelevantParent(startNode);

			while (node != null) {
				if (hasSkipMtdAnnotationTrue(framework, node.get(), annotationNode)) {
					return true;
				}
				if (hasSkipMtdAnnotationFalse(framework, node.get(), annotationNode)) {
					return false;
				}
				node = getNextRelevantParent(node.directUp());
			}
			return false;
		}
	}

	private static class TypeContext extends Wrapping {
		final TypeDeclaration typeDeclaration;
		private final TypeContext parentTypeContext;
		AllocationExpression allocationExpression;

		public TypeContext(TypeDeclaration typeDeclaration, AllocationExpression allocationExpression, TypeContext parentTypeContext, Wrapping wrapping) {
			super(wrapping);
			this.allocationExpression = allocationExpression;
			this.typeDeclaration = typeDeclaration;
			this.parentTypeContext = parentTypeContext;
		}

		@Override
		public StringBuilder getFullQulifiedNameBuilder() {
			final StringBuilder sb = super.getFullQulifiedNameBuilder();
			appendSignature(sb, typeDeclaration, allocationExpression);
			return sb;
		}

		public static void appendSignature(StringBuilder sb, TypeDeclaration typeDeclaration, AllocationExpression allocationExpression) {
			char[] name = null;

			if (typeDeclaration.name.length == 0) {
				if (allocationExpression == null) {
					name = "<anon>".toCharArray();
				} else {
					appendSignature(sb, allocationExpression);
					return;
				}
			} else {
				name = typeDeclaration.name;
			}
			if (name != null) {
				if (sb.length() > 0) {
					sb.append(".");
				}
				sb.append(name);
			}
		}

		public static void appendSignature(StringBuilder sb, AllocationExpression allocationExpression) {
			if (sb.length() > 0) {
				sb.append(".");
			}
			sb.append(EclipseHandlerUtil.extractConstructorName(allocationExpression));
		}

		@Override
		public boolean shouldSkipMethod(final LoggingFramework framework, final EclipseNode annotationNode) {
			return shouldSkipMethodOnNode(framework, typeDeclaration, annotationNode);
		}
	}

	private static class MethodContext extends Wrapping {
		final AbstractMethodDeclaration methodDeclaration;
		private String methodSignature = null;
		final private MethodContext parentMethodContext;

		public MethodContext(AbstractMethodDeclaration methodDeclaration, MethodContext parentMethodContext, Wrapping wrapping) {
			super(wrapping);
			this.methodDeclaration = methodDeclaration;
			this.parentMethodContext = parentMethodContext;
		}

		@Override
		public StringBuilder getFullQulifiedNameBuilder() {
			if (methodSignature == null) {
				final StringBuilder sb = super.getFullQulifiedNameBuilder();
				appendSignature(sb, methodDeclaration);
				return sb;
			} else {
				return new StringBuilder(methodSignature);
			}
		}

		public static void appendSignature(StringBuilder sb, AbstractMethodDeclaration methodDeclaration) {
			if (sb.length() > 0) {
				sb.append(".");
			}
			sb.append(EclipseHandlerUtil.getMethodSignature(methodDeclaration));
		}

		public String getMethodSignature() {
			if (methodSignature == null) {
				methodSignature = getFullQulifiedNameBuilder().toString();
			}
			return methodSignature;
		}

		@Override
		public boolean shouldSkipMethod(final LoggingFramework framework, final EclipseNode annotationNode) {
			return shouldSkipMethodOnNode(framework, methodDeclaration, annotationNode);
		}
	}

	private static void traverseSource(final LoggingFramework framework, final EclipseNode owner,
			final EclipseNode annotationNode, final String logFieldName, final String[] skipMethods, final boolean[] rebuildRequired) {
		StatementModifiyingVisitor visitor = new StatementModifiyingVisitor() {

			TypeContext currentTypeContext = null;
			MethodContext currentMethodContext = null;
			Wrapping currentWrapping = new DynamicWrapping(owner.up(), logFieldName);
			QualifiedAllocationExpression lastQualifiedAllocationExpression = null;

			@Override public boolean visit(
					QualifiedAllocationExpression qualifiedAllocationExpression,
		    		BlockScope scope) {
				lastQualifiedAllocationExpression = qualifiedAllocationExpression;
				return true; // do nothing by default, keep traversing
			}

			@Override public boolean visit(FieldDeclaration fieldDeclaration, MethodScope scope) {
				if (logFieldName.equals(new String(fieldDeclaration.name))) {
					currentWrapping.hasLogField = true;
				}
				return true;
			}

			@Override public boolean visit(TypeDeclaration typeDeclaration, ClassScope scope) {
				return visitTypeDeclaration(typeDeclaration) || super.visit(typeDeclaration, scope);
			}
			@Override public void endVisit(TypeDeclaration typeDeclaration, ClassScope scope) {
				endVisitTypeDeclaration(typeDeclaration);
			}

			@Override public boolean visit(TypeDeclaration typeDeclaration, BlockScope scope) {
				return visitTypeDeclaration(typeDeclaration) && super.visit(typeDeclaration, scope);
			}

			@Override public void endVisit(TypeDeclaration typeDeclaration, BlockScope scope) {
				endVisitTypeDeclaration(typeDeclaration);
			}

			@Override public boolean visit(TypeDeclaration typeDeclaration, CompilationUnitScope scope) {
				return visitTypeDeclaration(typeDeclaration) && super.visit(typeDeclaration, scope);
			}

			@Override public void endVisit(TypeDeclaration typeDeclaration, CompilationUnitScope scope) {
				endVisitTypeDeclaration(typeDeclaration);
			}

			private boolean visitTypeDeclaration(TypeDeclaration typeDeclaration) {
				currentTypeContext = new TypeContext(typeDeclaration, lastQualifiedAllocationExpression, currentTypeContext, currentWrapping);
				currentWrapping = currentTypeContext;
				return !notOnStartingNodeAndHasLogAnnotation(typeDeclaration);
			}

			private void endVisitTypeDeclaration(TypeDeclaration typeDeclaration) {
				currentWrapping = currentWrapping.wrapping;
				currentTypeContext = currentTypeContext.parentTypeContext;
			}

			protected boolean notOnStartingNodeAndHasLogAnnotation(ASTNode astNode) {
				boolean onStartingNode = (astNode == annotationNode.up().get());
				if (!onStartingNode) {
					boolean hasQFLogAnnotation = hasLogAnnotation(framework, astNode, annotationNode);
					if (hasQFLogAnnotation) return true; // is handled directly
				}
				return false;
			}

			@Override public boolean visit(MethodDeclaration methodDeclaration, ClassScope scope) {
				return visitAbstractMethodDeclaration(methodDeclaration) && super.visit(methodDeclaration, scope);
			}
			@Override public void endVisit(MethodDeclaration methodDeclaration, ClassScope scope) {
				endVisitAbstractMethodDeclaration(methodDeclaration);
			}

			@Override public boolean visit(ConstructorDeclaration methodDeclaration, ClassScope scope) {
				return visitAbstractMethodDeclaration(methodDeclaration) && super.visit(methodDeclaration, scope);
			}
			@Override public void endVisit(ConstructorDeclaration methodDeclaration, ClassScope scope) {
				endVisitAbstractMethodDeclaration(methodDeclaration);
			}

			@Override public boolean visit(MessageSend messageSend, BlockScope scope) {
				if (!isQFLogMethodCall(framework, messageSend)) return true;
				// this should not happen...
				annotationNode.addError("qflog() call at an invalid location: " + messageSend);
				return false;
			}

			private boolean visitAbstractMethodDeclaration(AbstractMethodDeclaration methodDeclaration) {
				currentMethodContext = new MethodContext(methodDeclaration, currentMethodContext, currentWrapping);
				currentWrapping = currentMethodContext;

				if (notOnStartingNodeAndHasLogAnnotation(methodDeclaration)) {
					return false;
				}

				boolean skipMtd = currentWrapping.shouldSkipMethod(framework, annotationNode);

				if (!skipMtd) {
					visitAbstractMethodDeclarationForMtdLogCall(framework, currentMethodContext, annotationNode, skipMethods, rebuildRequired);
				}
				return true;
			}

			private void endVisitAbstractMethodDeclaration(AbstractMethodDeclaration methodDeclaration) {
				currentWrapping = currentWrapping.wrapping;
				currentMethodContext = currentMethodContext.parentMethodContext;
			}

			@Override
			public Statement visitStatement(Statement statement) {
				if (!(statement instanceof MessageSend)) {
					return null;
				}
				MessageSend messageSend = (MessageSend) statement;
				if (!isQFLogMethodCall(framework, messageSend)) return null;

				rebuildRequired[0] = true;
				Statement wrappingBlock = prepareWrappedQFLogCall(messageSend, currentMethodContext, framework, logFieldName, annotationNode);

				wrappingBlock.traverse(new SetGeneratedByVisitor(messageSend), (MethodScope) null);
				return wrappingBlock;
			}

		};

		ASTNode astNode = owner.get();
		if (astNode instanceof AbstractMethodDeclaration) {
			((AbstractMethodDeclaration) astNode).traverse(visitor, (ClassScope) null);
		} else if (astNode instanceof TypeDeclaration) {
			((TypeDeclaration) astNode).traverse(visitor, (ClassScope) null);
		}

	}

	private static boolean visitAbstractMethodDeclarationForMtdLogCall(final LoggingFramework framework,
																		MethodContext methodContext,
																		final EclipseNode annotationNode,
																		final String[] skipMethods,
																		final boolean[] rebuildRequired) {
		AbstractMethodDeclaration methodDeclaration = methodContext.methodDeclaration;

		if (isInSkipMtdNames(methodContext, skipMethods)) {
			if (!hasSkipMtdAnnotationFalse(framework, methodDeclaration, annotationNode)) {
				return true;
			}
		}

		addMethodLogCall(framework, methodContext, annotationNode);
		rebuildRequired[0] = true;
		return true;
	}

	private static boolean isInSkipMtdNames(MethodContext methodContext, String[] skipMethods) {
		if (skipMethods == null) {
			return false;
		}

		final String methodSignature = methodContext.getMethodSignature();

		for (final String skipMethod : skipMethods) {
			if (skipMethod.startsWith("%") && skipMethod.length() > 1) {
				if (methodSignature != null && methodSignature.matches(skipMethod.substring(1))) {
					return true;
				}
			} else if (skipMethod.equals(methodSignature)) {
				return true;
			}
		}
		return false;
	}

	private static boolean hasSkipMtdAnnotationTrue(LoggingFramework framework, ASTNode astNode, final EclipseNode anyNode) {
		return EclipseHandlerUtil.annotationStringContains(astNode, framework.getAnnotationClass(), "skipMtd = true", anyNode);
	}

	private static boolean hasSkipMtdAnnotationFalse(LoggingFramework framework, ASTNode astNode, final EclipseNode anyNode) {
		return EclipseHandlerUtil.annotationStringContains(astNode, framework.getAnnotationClass(), "skipMtd = false", anyNode);
	}

	protected static boolean hasLogAnnotation(LoggingFramework framework, ASTNode astNode, final EclipseNode anyNode) {
		return EclipseHandlerUtil.hasAnnotation(astNode, framework.getAnnotationClass(), anyNode);
	}

	private static boolean isQFLogMethodCall(final LoggingFramework framework, MessageSend messageSend) {
		final String[] validQFLogMethodNames = framework.getValidQFLogMethodNames();

		final String methodName = new String(messageSend.selector);
		if (messageSend.receiver instanceof MessageSend) {
			if (Arrays.binarySearch(validQFLogMethodNames, methodName) < 0) return false;
			return isQFLogMethodCall(framework, (MessageSend) messageSend.receiver);
		}
		if (messageSend.receiver instanceof ThisReference) {
			if ((((ThisReference) messageSend.receiver).bits & ASTNode.IsImplicitThis) == 0) return false;
		} else if (messageSend.receiver != null) return false;
		if (!methodName.equals(framework.getLogMethodName())) return false;
		if (messageSend.arguments == null) return false;
		if (!(messageSend.arguments.length > 0)) return false;
		if (isGenerated(messageSend)) return false;
		return true;
	}

	private static void addMethodLogCall(LoggingFramework framework, MethodContext methodContext, EclipseNode annotationNode) {

		AbstractMethodDeclaration declaration = methodContext.methodDeclaration;

		String logFieldName;
		logFieldName = getLogFieldName(annotationNode);

		Statement logCall = generateMtdLogCall(framework, logFieldName, methodContext, annotationNode);
		logCall.traverse(new SetGeneratedByVisitor(declaration), (MethodScope) null);

		if (declaration.statements == null) {
			declaration.statements = new Statement[] {logCall};
		} else {
			Statement[] newStatements = new Statement[declaration.statements.length + 1];
			System.arraycopy(declaration.statements, 0, newStatements, 1, declaration.statements.length);
			newStatements[0] = logCall;
			declaration.statements = newStatements;
		}
	}

	private static Statement generateMtdLogCall(LoggingFramework framework, String logFieldName, MethodContext methodContext, EclipseNode annotationNode) {
		final ASTNode source = annotationNode.get();
		int pS = source.sourceStart, pE = source.sourceEnd;
		final long p = (long) pS << 32 | pE;

		final AbstractMethodDeclaration declaration = methodContext.methodDeclaration;

		final String methodSignature = methodContext.getMethodSignature();

		Expression levelExpression = new SingleNameReference(framework.getMtdLevelConstant().toCharArray(), p);
		LogCallArgumentProvider logCallArgumentProvider = new AbstractMethodDeclarationBasedLogCallArgumentProvider(source, declaration);

		return generateQFLogCall(framework, levelExpression, logCallArgumentProvider, logFieldName, methodSignature, declaration, annotationNode);
	}

	private static Statement generateQFLogCall(LoggingFramework framework, Expression levelExpression, LogCallArgumentProvider logCallArgumentProvider, String logFieldName, String methodSignature, ASTNode source, EclipseNode annotationNode) {

		int pS = source.sourceStart, pE = source.sourceEnd;
		long p = (long) pS << 32 | pE;

		final String fullLogTypeName = framework.getLogTypeName();
		final String shortLogTypeName = CommonHandlerUtil.getLastPart(fullLogTypeName);

		boolean usesLogConstant = false;

		String firstArgumentAsString = levelExpression.toString();
		usesLogConstant |= firstArgumentAsString.startsWith(fullLogTypeName + ".");
		usesLogConstant |= firstArgumentAsString.startsWith(shortLogTypeName + ".");
		if ((levelExpression instanceof SingleNameReference)) {
			final Expression extendedArgument = expandLogLevel(framework, levelExpression, source, annotationNode);
			if (extendedArgument != null) {
				levelExpression = extendedArgument;
				usesLogConstant = true;
			}
		}

		Expression logLevelParameter, logLevelComparator;
		final char[] levelArgumentVarname = "$level".toCharArray();

		if (usesLogConstant) {
			logLevelParameter = levelExpression;
			logLevelComparator = createNameReference(levelExpression.toString(), source);
		} else {
			logLevelParameter = new SingleNameReference(levelArgumentVarname, p);
			logLevelComparator = new SingleNameReference(levelArgumentVarname, p);
		}

		Expression methodParameter = new StringLiteral(methodSignature.toCharArray(), pS, pE, 0);

		MessageSend builderCall = new MessageSend();
		SingleNameReference logFieldReference = new SingleNameReference(logFieldName.toCharArray(), p);
		builderCall.receiver = logFieldReference;
		builderCall.selector = "lvlBuild".toCharArray();

		int lineNumber = EclipseHandlerUtil.getLineNumber(pS, annotationNode);

		if (lineNumber == 0) {
			builderCall.arguments = new Expression[] {logLevelParameter, methodParameter};
		} else {
			Expression lineNumberParameter = makeIntLiteral(("" + lineNumber).toCharArray(), source);
			builderCall.arguments = new Expression[] {logLevelParameter, methodParameter, lineNumberParameter};
		}

		builderCall.nameSourcePosition = p;
		builderCall.sourceStart = pS;
		builderCall.sourceEnd = builderCall.statementEnd = pE;

		builderCall = addArgumentsToLogCall(builderCall, logCallArgumentProvider);

		MessageSend logCall = new MessageSend();
		logCall.receiver = builderCall;
		logCall.selector = "log".toCharArray();
		logCall.nameSourcePosition = p;
		logCall.sourceStart = pS;
		logCall.sourceEnd = logCall.statementEnd = pE;


		FieldReference levelReference = new FieldReference("level".toCharArray(), p);
		Expression loggerLevelReferenceReceiver = new SingleNameReference(logFieldName.toCharArray(), p);
		levelReference.receiver = loggerLevelReferenceReceiver;

		BinaryExpression equalExpression = new BinaryExpression(levelReference, logLevelComparator, OperatorIds.GREATER_EQUAL);
		equalExpression.sourceStart = pS;
		equalExpression.statementEnd = equalExpression.sourceEnd = pE;

		IfStatement ifStatement = new IfStatement(equalExpression, logCall, 0, 0);

		if (usesLogConstant) {
			return ifStatement;
		} else {

			LocalDeclaration localLevelArgumentDeclaration = new LocalDeclaration(levelArgumentVarname, pS, pE);
			localLevelArgumentDeclaration.modifiers |= ClassFileConstants.AccFinal;
			localLevelArgumentDeclaration.type = TypeReference.baseTypeReference(TypeIds.T_int, 0);
			localLevelArgumentDeclaration.initialization = levelExpression;

			Block wrappingBlock = new Block(2);
			wrappingBlock.statements = new Statement[] {localLevelArgumentDeclaration, ifStatement};

			return wrappingBlock;
		}
	}

	private static Expression expandLogLevel(LoggingFramework framework, Expression levelExpression, ASTNode source, EclipseNode sourceNode) {
		if (!(levelExpression instanceof SingleNameReference)) return null;

		String literal = new String(((SingleNameReference) levelExpression).token);

		if (Arrays.binarySearch(framework.getLoggingConstants(), literal) < 0) return null;

		final String logTypeName = getLogTypeName(framework, sourceNode);
		return createNameReference(logTypeName + "." + literal, source);
	}

	protected static String getLogTypeName(LoggingFramework framework, EclipseNode sourceNode) {
		final String fullQualifiedName = framework.getLogTypeName();
		final String simpleName = CommonHandlerUtil.getLastPart(fullQualifiedName);
		if (fullQualifiedName.equals(sourceNode.getImportList().getFullyQualifiedNameForSimpleName(simpleName))) {
			// is in imports
			return simpleName;
		} else {
			return fullQualifiedName;
		}
	}

	protected static MessageSend addArgumentsToLogCall(MessageSend builderCall, LogCallArgumentProvider logCallArgumentProvider) {
		int pS = logCallArgumentProvider.source.sourceStart, pE = logCallArgumentProvider.source.sourceEnd;
		long p = (long) pS << 32 | pE;

		for (LogCallArgument arg: logCallArgumentProvider) {
			MessageSend addDetailsCall = new MessageSend();
			addDetailsCall.receiver = builderCall;
			switch (arg.type) {
			case DETAIL:
				addDetailsCall.selector = "addDetail".toCharArray();
				break;
			case ALL:
				addDetailsCall.selector = "addAll".toCharArray();
				break;
			default:
				addDetailsCall.selector = "add".toCharArray();
			}

			if (arg.label != null) {
				Expression argNameParameter;
				if (arg.label instanceof Expression) {
					argNameParameter = (Expression) arg.label;
				} else {
					argNameParameter = new StringLiteral(arg.label.toString().toCharArray(), pS, pE, 0);
				}
				addDetailsCall.arguments = new Expression[] {argNameParameter, arg.message};
			} else {
				addDetailsCall.arguments = new Expression[] {arg.message};
			}

			addDetailsCall.nameSourcePosition = p;
			addDetailsCall.sourceStart = pS;
			addDetailsCall.sourceEnd = addDetailsCall.statementEnd = pE;

			builderCall = addDetailsCall;
		}
		return builderCall;
	}

	private static Statement prepareWrappedQFLogCall(MessageSend messageSend, MethodContext methodContext, LoggingFramework framework, String logFieldName, EclipseNode annotationNode) {

		final boolean primaryCall = (messageSend.receiver == null) || messageSend.receiverIsImplicitThis();

		String methodSignature = "static";
		try {
			methodSignature = methodContext.getMethodSignature();
		} catch (NullPointerException e) {
			// ignore
		}

		if (primaryCall) {
			return generateWrappedQFLogCall(messageSend, methodSignature, framework, logFieldName, annotationNode);
		} else {
			Statement parentCall = prepareWrappedQFLogCall((MessageSend) messageSend.receiver, methodContext, framework, logFieldName, annotationNode);

			// We constructed the block ourselves, so we no its structure...
			Statement ifCall = parentCall;

			if (parentCall instanceof Block) {
				ifCall = ((Block) parentCall).statements[1];
			}
			MessageSend logCall = ((MessageSend) ((IfStatement) ifCall).thenStatement);
			MessageSend builderCall = ((MessageSend) logCall.receiver);

			final String methodName = new String(messageSend.selector);

			if (framework.isDumpStackCall(methodName)) {
				logCall.selector = "dumpStack".toCharArray();
			} else {
				LogCallArgumentProvider logCallArgumentProvider = null;
				if (framework.isParamsCall(methodName)) {
					try {
						AbstractMethodDeclaration declaration = methodContext.methodDeclaration;
						logCallArgumentProvider = new AbstractMethodDeclarationBasedLogCallArgumentProvider(messageSend, declaration);
					} catch (NullPointerException ex) {
						// ignore
					}
				} else {
					final LogCallArgumentType addType = framework.getAddType(methodName);
					logCallArgumentProvider = new MessageSendBasedLogCallArgumentProvider(messageSend, 0, addType);
				}
				if (logCallArgumentProvider != null) {
					logCall.receiver = addArgumentsToLogCall(builderCall, logCallArgumentProvider);
				}
			}

			return parentCall;
		}
	}

	private static class LogCallArgument {
		LogCallArgumentType type;
		Object label;
		Expression message;

		public LogCallArgument(final Expression message) {
			this.message = message;
		}

		public LogCallArgument(final Object label, final Expression message) {
			this(message);
			this.label = label;
		}
	}

	private abstract static class LogCallArgumentProvider implements Iterable<LogCallArgument> {
		final ASTNode source;
		final LogCallArgumentType addType;

		public LogCallArgumentProvider(final ASTNode source, final LogCallArgumentType addType) {
			this.source = source;
			this.addType = addType;
		}

		int getStartIndex() {
			return 0;
		}

		abstract boolean hasNextExpression(int index);
		abstract Expression getNextExpression(int index);

		@Override public Iterator<LogCallArgument> iterator() {
			return new Iterator<LogCallArgument>() {

				int index = getStartIndex();
				Queue<LogCallArgument> pufferedArguments = null;

				@Override public boolean hasNext() {
					return (pufferedArguments != null && pufferedArguments.size() > 0) || hasNextExpression(index);
				}

				@Override public LogCallArgument next() {
					if (pufferedArguments != null) {
						final LogCallArgument derivedArg = pufferedArguments.poll();
						if (derivedArg != null) {
							derivedArg.type = addType;
							return derivedArg;
						}
					}
					pufferedArguments = null;

					if (!hasNext()) {
						return null;
					}

					final LogCallArgument logCallArg = nextFromExpression();
					logCallArg.type = addType;
					return logCallArg;
				}

				protected LogCallArgument nextFromExpression() {
					final boolean isFirstArgument = (index == getStartIndex());

					Expression expr = getNextExpression(index);
					index++;

					if (isStringLiteral(expr)) {
						if (isFirstArgument && hasNext()) {
							final LogCallArgument arg = nextFromExpression();
							arg.label = expr;
							return arg;
						}
						pufferedArguments = new LinkedList<LogCallArgument>();
						fillPufferedArguments(expr);
						return pufferedArguments.poll();
					} else {
						return new LogCallArgument(expr.toString(), expr);
					}
				}

				private void fillPufferedArguments(final Expression arg) {
					assert pufferedArguments != null;

					if (arg == null) {
						return;
					}
					if (arg instanceof BinaryExpression && isPlusExpression(arg)) {
						final BinaryExpression binaryArg = (BinaryExpression) arg;
						fillPufferedArguments(binaryArg.left);
						fillPufferedArguments(binaryArg.right);
						return;
					}

					pufferedArguments.offer(new LogCallArgument(arg));
				}

				@Override public void remove() {
					throw new UnsupportedOperationException("Remove not implemented");
				}
			};
		}
	}

	private static final class AbstractMethodDeclarationBasedLogCallArgumentProvider extends LogCallArgumentProvider {
		private final AbstractMethodDeclaration declaration;

		private AbstractMethodDeclarationBasedLogCallArgumentProvider(ASTNode source, AbstractMethodDeclaration declaration) {
			super(source, LogCallArgumentType.DETAIL);
			this.declaration = declaration;
		}

		@Override boolean hasNextExpression(int index) {
			return declaration.arguments != null && index < declaration.arguments.length;
		}

		@Override Expression getNextExpression(int index) {
			int pS = source.sourceStart, pE = source.sourceEnd;
			final long p = (long) pS << 32 | pE;

			return new SingleNameReference(declaration.arguments[index].name, p);
		}
	}

	private static class MessageSendBasedLogCallArgumentProvider extends LogCallArgumentProvider {
		final MessageSend messageSend;
		final int argumentsToIgnore;

		public MessageSendBasedLogCallArgumentProvider(final MessageSend messageSend, final int argumentsToIgnore, final LogCallArgumentType addType) {
			super(messageSend, addType);
			this.messageSend = messageSend;
			this.argumentsToIgnore = argumentsToIgnore;
		}

		@Override int getStartIndex() {
			return argumentsToIgnore;
		}

		@Override boolean hasNextExpression(int index) {
			return messageSend.arguments != null && index < messageSend.arguments.length;
		}

		@Override Expression getNextExpression(int index) {
			return messageSend.arguments[index];
		}

	}

	private static Statement generateWrappedQFLogCall(MessageSend messageSend, String methodSignature, LoggingFramework framework, String logFieldName, EclipseNode annotationNode) {
		final ASTNode source = messageSend;
		Expression levelExpression = messageSend.arguments[0];
		LogCallArgumentProvider logCallArgumentProvider = new MessageSendBasedLogCallArgumentProvider(messageSend, 1, LogCallArgumentType.STANDARD);
		return generateQFLogCall(framework, levelExpression, logCallArgumentProvider, logFieldName, methodSignature, source, annotationNode);
	}

	/**
	 * Handles the {@link lombok.extern.qfs.QFLog} annotation for Eclipse - Post diet.
	 */
	@Provides
	@DeferUntilPostDiet
	@HandlerPriority(value = 510) // < 2^9=512; Must be run before NonNullhandler
	public static class HandleQFLogPostDietAnnotation extends EclipseAnnotationHandler<lombok.extern.qfs.intern.__AutoLogAnnotation> {
		@Override public void handle(AnnotationValues<lombok.extern.qfs.intern.__AutoLogAnnotation> annotation, Annotation source, EclipseNode annotationNode) {
			try {
				String[] skipMethods = annotation.getInstance().skipMtdNames();

				processQfLogAnnotationPostDiet(LoggingFramework.QFLOG, annotation, source, annotationNode, skipMethods);
			} catch (final RuntimeException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}

	}
}
