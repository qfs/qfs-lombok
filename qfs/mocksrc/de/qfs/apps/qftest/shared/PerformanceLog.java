package de.qfs.apps.qftest.shared;


public class PerformanceLog {
	public final static PerformanceLog instance()
    {
        return new PerformanceLog();
    }

	public void startPeriod(String tag) {}
	public void endPeriod(String tag) {}
	public void reset() {}

}
