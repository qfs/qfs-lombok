package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog(skipMtd = true) enum LoggerQFLogEnum {
  FIRST(),
  SECOND(),
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnum");
  static {
    if ((logger.level >= de.qfs.lib.log.Log.WRN))
        logger.lvlBuild(de.qfs.lib.log.Log.WRN, "static", 10).add("Should work").log();
  }
  <clinit>() {
  }
  LoggerQFLogEnum() {
    super();
  }
}