package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog(skipMtd = true) class LoggerQFLogEnclosing {
  @QFLog(ownLogger = true,skipMtd = true) class LoggerQFLogInner {
    private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.LoggerQFLogInner");
    LoggerQFLogInner() {
      super();
    }
  }
  public static @QFLog(ownLogger = true,skipMtd = true) class LoggerQFLogStaticInner {
    private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.LoggerQFLogStaticInner");
    <clinit>() {
    }
    public LoggerQFLogStaticInner() {
      super();
    }
  }
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing");
  <clinit>() {
  }
  LoggerQFLogEnclosing() {
    super();
  }
  public void someFunc(int[] p) {
    @QFLog(ownLogger = true,skipMtd = false) class LoggerQFLogInFct {
      private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.someFunc(int[]).LoggerQFLogInFct");
      LoggerQFLogInFct() {
        super();
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogInFct()", 16).log();
      }
      public void inFct() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "inFct()", 17).log();
        if ((logger.level >= de.qfs.lib.log.Log.ERR))
            logger.lvlBuild(de.qfs.lib.log.Log.ERR, "inFct()", 18).add("log here").log();
      }
    }
  }
}