import lombok.extern.qfs.PerfLog;
class Parent {
  Parent(String s) {
    super();
  }
}
class PerfTest extends Parent {
  @PerfLog(value = "Constructor",reset = true) PerfTest() {
    super("Child");
    final java.lang.String $performanceLogTagName = "Constructor";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().reset();
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println("Const");
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  @PerfLog(reset = true) void measuredMethod() {
    final java.lang.String $performanceLogTagName = "";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().reset();
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
}