package de.qfs.bla;
import lombok.extern.qfs.QFNotifierSpec;
@QFNotifierSpec(value = "de.xyz.abc.Bla",level = de.qfs.lib.notifications.Notifier.DEBUG) class QFNotifierHandlingConfigured {
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.xyz.abc.Bla");
  public static final int POST_LEVEL = 9;
  <clinit>() {
  }
  QFNotifierHandlingConfigured() {
    super();
  }
}
@QFNotifierSpec("de.qfs.bla.Schnitzel") class QFNotifierHandlingSimpleValue {
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.qfs.bla.Schnitzel");
  public static final int POST_LEVEL = 1;
  <clinit>() {
  }
  QFNotifierHandlingSimpleValue() {
    super();
  }
}
@QFNotifierSpec(level = 4) class QFNotifierHandlingOnlyLevel {
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.qfs.bla.QFNotifierHandlingOnlyLevel");
  public static final int POST_LEVEL = 4;
  <clinit>() {
  }
  QFNotifierHandlingOnlyLevel() {
    super();
  }
}
@QFNotifierSpec(value = "names",names = {"bla!?!bla.bla0", "1BlubberBla", "bla_bla_bla0"}) class QFNotifierHandlingNames {
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("names");
  public static final int POST_LEVEL = 1;
  public static final @de.qfs.lib.notifications.NotificationName java.lang.String BLA_BLA_BLA0 = "names.bla!?!bla.bla0";
  public static final @de.qfs.lib.notifications.NotificationName java.lang.String _BLUBBER_BLA = "names.1BlubberBla";
  <clinit>() {
  }
  QFNotifierHandlingNames() {
    super();
  }
}
@QFNotifierSpec class QFNotifierHandlingAuto {
  static @QFNotifierSpec(names = {"Value 1", "Value 2"}) class InnerClass {
    public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.qfs.bla.QFNotifierHandlingAuto.InnerClass");
    public static final int POST_LEVEL = 1;
    public static final @de.qfs.lib.notifications.NotificationName java.lang.String VALUE_1 = "de.qfs.bla.QFNotifierHandlingAuto.InnerClass.Value 1";
    public static final @de.qfs.lib.notifications.NotificationName java.lang.String VALUE_2 = "de.qfs.bla.QFNotifierHandlingAuto.InnerClass.Value 2";
    <clinit>() {
    }
    InnerClass() {
      super();
    }
  }
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.qfs.bla.QFNotifierHandlingAuto");
  public static final int POST_LEVEL = 1;
  <clinit>() {
  }
  QFNotifierHandlingAuto() {
    super();
  }
}
@QFNotifierSpec class QFNotifierHandlingWithPredefinedNotifier {
  public static final int POST_LEVEL = 1;
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = null;
  <clinit>() {
  }
  QFNotifierHandlingWithPredefinedNotifier() {
    super();
  }
}
@QFNotifierSpec class QFNotifierHandlingWithPredefinedPostLevel {
  public static final de.qfs.lib.notifications.Notifier NOTIFIER = de.qfs.lib.notifications.Notifier.instance("de.qfs.bla.QFNotifierHandlingWithPredefinedPostLevel");
  public static final int POST_LEVEL = 10;
  <clinit>() {
  }
  QFNotifierHandlingWithPredefinedPostLevel() {
    super();
  }
}