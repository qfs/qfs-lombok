package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog interface LoggerQFLogInterface {
  public static @QFLog(ownLogger = true,skipMtd = true) class innerClass {
    private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogInterface.innerClass");
    <clinit>() {
    }
    public innerClass() {
      super();
    }
    public static @QFLog(skipMtd = true) boolean innerClassMethod(boolean valueToReturn) {
      if ((logger.level >= de.qfs.lib.log.Log.ERR))
          logger.lvlBuild(de.qfs.lib.log.Log.ERR, "innerClassMethod(boolean)", 33).log();
      return valueToReturn;
    }
  }
  public static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogInterface");
  <clinit>() {
  }
  default int methodWithMeatViaAnnotation(String meat) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "methodWithMeatViaAnnotation(String)", 8).addDetail("meat", meat).log();
    System.out.println(meat);
    return 0;
  }
  default @QFLog(skipMtd = true) int methodWithMeatViaQflog(String meat) {
    if ((logger.level >= de.qfs.lib.log.Log.WRN))
        logger.lvlBuild(de.qfs.lib.log.Log.WRN, "methodWithMeatViaQflog(String)", 15).add("In methodWithMeatViaQflog.").log();
    System.out.println(meat);
    return 0;
  }
  default void withInner() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "withInner()", 20).log();
    new Runnable() {
  x() {
    super();
  }
  public @QFLog(skipMtd = true) void run() {
    if ((logger.level >= de.qfs.lib.log.Log.DBG))
        logger.lvlBuild(de.qfs.lib.log.Log.DBG, "withInner().Runnable.run()", 24).log();
  }
}.run();
  }
}
