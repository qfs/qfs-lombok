package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog(skipMtdNames = {"testSkip()", "%get[A-Z]\\(\\)"}) class ClassWithQFLogMtdAndSkipMethods {
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.ClassWithQFLogMtdAndSkipMethods");
  String s;
  <clinit>() {
  }
  ClassWithQFLogMtdAndSkipMethods() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "ClassWithQFLogMtdAndSkipMethods()", 6).log();
  }
  public void testSkip() {
  }
  public void testSkip(float f) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "testSkip(float)", 10).addDetail("f", f).log();
  }
  public void getA() {
  }
  public void getB() {
  }
}
