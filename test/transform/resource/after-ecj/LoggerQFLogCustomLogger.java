package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog(loggerClass = CustomLogger.class) class LoggerQFLogCustomLogger {
  private static final CustomLogger logger = new CustomLogger("de.qfs.bla.LoggerQFLogCustomLogger");
  <clinit>() {
  }
  LoggerQFLogCustomLogger() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogCustomLogger()", 6).log();
  }
  public void test(float fluss) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(float)", 8).addDetail("fluss", fluss).log();
  }
}
class CustomLogger extends de.qfs.lib.log.QFLogger {
  public CustomLogger(String topic) {
    super(topic);
  }
}