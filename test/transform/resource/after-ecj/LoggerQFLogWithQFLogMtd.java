package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog class ClassWithQFLogMtd {
  public static @QFLog(skipMtd = true) class ClassWithSkipMtd {
    public ClassWithSkipMtd() {
      super();
    }
    public void mtdShouldBeSkipped() {
    }
    public @QFLog void mtdShouldAlsoBeSkipped() {
    }
  }
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.ClassWithQFLogMtd");
  static {
    if ((logger.level >= de.qfs.lib.log.Log.DBG))
        logger.lvlBuild(de.qfs.lib.log.Log.DBG, "static", 56).add("In static initializer").log();
  }
  <clinit>() {
  }
  ClassWithQFLogMtd() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "ClassWithQFLogMtd()", 6).log();
  }
  public void test(String blubb, boolean flag) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String,boolean)", 7).addDetail("blubb", blubb).addDetail("flag", flag).log();
  }
  public Object test(String... args) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[])", 9).addDetail("args", args).log();
    class InnerClass {
      InnerClass() {
        super();
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).InnerClass.InnerClass()", 10).log();
      }
      public void methodInInnerClass() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).InnerClass.methodInInnerClass()", 11).log();
      }
    }
    Object o = new Object() {
      x() {
        super();
      }
      public String toString() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).Object.toString()", 15).log();
        return "anonymousToString";
      }
      public @QFLog void withOwnStatement() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).Object.withOwnStatement()", 20).log();
      }
    };
    System.out.println(o);
    if ((Integer.valueOf(new Object() {
  x() {
    super();
  }
  public String toString() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).Object.toString()", 27).log();
    if ((logger.level >= de.qfs.lib.log.Log.DBG))
        logger.lvlBuild(de.qfs.lib.log.Log.DBG, "test(String[]).Object.toString()", 28).add("pass").log();
    return "1";
  }
}.toString()) == 1))
        {
          System.out.println("pass");
        }
    return new Object() {
  x() {
    super();
  }
  public @QFLog void methodInReturnedObject(final int args) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(String[]).Object.methodInReturnedObject(int)", 37).addDetail("args", args).log();
  }
};
  }
  public @QFLog(skipMtd = true) void methodShouldBeSkipped() {
  }
  public void methodWithNullCheck(@lombok.NonNull String s) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "methodWithNullCheck(String)", 46).addDetail("s", s).log();
    if ((s == null))
        {
          throw new java.lang.NullPointerException("s is marked non-null but is null");
        }
  }
  public @QFLog(skipMtd = true) void skippedWithManualParams(@lombok.NonNull String str, int i) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "skippedWithManualParams(String,int)", 51).addDetail("str", str).addDetail("i", i).log();
    if ((str == null))
        {
          throw new java.lang.NullPointerException("str is marked non-null but is null");
        }
    if ((logger.level >= de.qfs.lib.log.Log.DBG))
        logger.lvlBuild(de.qfs.lib.log.Log.DBG, "skippedWithManualParams(String,int)", 52).add("Debug").addDetail("str", str).addDetail("i", i).log();
  }
}
