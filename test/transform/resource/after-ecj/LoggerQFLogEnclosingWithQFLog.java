package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog class LoggerQFLogEnclosingWithQFLog {
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithQFLog");
  <clinit>() {
  }
  LoggerQFLogEnclosingWithQFLog() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogEnclosingWithQFLog()", 6).log();
  }
  public void withInner() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "withInner()", 7).log();
    new Runnable() {
  x() {
    super();
  }
  public @QFLog(skipMtd = true) void run() {
    if ((logger.level >= de.qfs.lib.log.Log.DBG))
        logger.lvlBuild(de.qfs.lib.log.Log.DBG, "withInner().Runnable.run()", 11).log();
  }
}.run();
  }
}
