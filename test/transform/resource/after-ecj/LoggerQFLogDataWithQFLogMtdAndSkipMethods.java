package de.qfs.bla;
import lombok.extern.qfs.QFLog;
import lombok.Data;
@Data @QFLog(skipMtdNames = {"%get[A-Z][^\\(]*\\(\\)", "hashCode()", "equals(Object)", "canEqual(Object)", "toString()"}) class DataWithQFLogMtdAndSkipMethods {
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.DataWithQFLogMtdAndSkipMethods");
  String i;
  <clinit>() {
  }
  public @java.lang.SuppressWarnings("all") String getI() {
    return this.i;
  }
  public @java.lang.SuppressWarnings("all") void setI(final String i) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "setI(String)", 6).addDetail("i", i).log();
    this.i = i;
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") boolean equals(final java.lang.Object o) {
    if ((o == this))
        return true;
    if ((! (o instanceof DataWithQFLogMtdAndSkipMethods)))
        return false;
    final DataWithQFLogMtdAndSkipMethods other = (DataWithQFLogMtdAndSkipMethods) o;
    if ((! other.canEqual((java.lang.Object) this)))
        return false;
    final java.lang.Object this$i = this.getI();
    final java.lang.Object other$i = other.getI();
    if (((this$i == null) ? (other$i != null) : (! this$i.equals(other$i))))
        return false;
    return true;
  }
  protected @java.lang.SuppressWarnings("all") boolean canEqual(final java.lang.Object other) {
    return (other instanceof DataWithQFLogMtdAndSkipMethods);
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final java.lang.Object $i = this.getI();
    result = ((result * PRIME) + (($i == null) ? 43 : $i.hashCode()));
    return result;
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") java.lang.String toString() {
    return (("DataWithQFLogMtdAndSkipMethods(i=" + this.getI()) + ")");
  }
  public @java.lang.SuppressWarnings("all") DataWithQFLogMtdAndSkipMethods() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "DataWithQFLogMtdAndSkipMethods()", 6).log();
  }
}
