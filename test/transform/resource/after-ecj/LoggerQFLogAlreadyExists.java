package de.qfs.bla;
import lombok.extern.qfs.QFLog;
import de.qfs.lib.log.QFLogger;
@QFLog class LoggerQFLogAlreadyExists {
  public static final QFLogger logger = new QFLogger("my special logger");
  <clinit>() {
  }
  LoggerQFLogAlreadyExists() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogAlreadyExists()", 7).log();
  }
  public void test(float fluss) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(float)", 11).addDetail("fluss", fluss).log();
  }
}
