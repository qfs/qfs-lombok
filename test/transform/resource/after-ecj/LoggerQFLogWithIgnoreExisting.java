package de.qfs.bla;
import lombok.extern.qfs.QFLog;
import de.qfs.lib.log.QFLogger;
@QFLog(ignoreExistingLogger = true) class LoggerQFLogWithIgnoreExisting {
  public static final QFLogger logger = new QFLogger("my special logger");
  <clinit>() {
  }
  LoggerQFLogWithIgnoreExisting() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogWithIgnoreExisting()", 7).log();
  }
  public void test(float fluss) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(float)", 11).addDetail("fluss", fluss).log();
  }
}
