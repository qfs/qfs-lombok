package de.qfs.bla;
import lombok.extern.qfs.QFLog;
import lombok.Data;
@Data @QFLog class DataWithQFLogMtd {
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.DataWithQFLogMtd");
  @lombok.NonNull String s;
  <clinit>() {
  }
  public @lombok.NonNull @java.lang.SuppressWarnings("all") String getS() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "getS()", 6).log();
    return this.s;
  }
  public @java.lang.SuppressWarnings("all") void setS(final @lombok.NonNull String s) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "setS(String)", 6).addDetail("s", s).log();
    if ((s == null))
        {
          throw new java.lang.NullPointerException("s is marked non-null but is null");
        }
    this.s = s;
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") boolean equals(final java.lang.Object o) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "equals(Object)", 6).addDetail("o", o).log();
    if ((o == this))
        return true;
    if ((! (o instanceof DataWithQFLogMtd)))
        return false;
    final DataWithQFLogMtd other = (DataWithQFLogMtd) o;
    if ((! other.canEqual((java.lang.Object) this)))
        return false;
    final java.lang.Object this$s = this.getS();
    final java.lang.Object other$s = other.getS();
    if (((this$s == null) ? (other$s != null) : (! this$s.equals(other$s))))
        return false;
    return true;
  }
  protected @java.lang.SuppressWarnings("all") boolean canEqual(final java.lang.Object other) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "canEqual(Object)", 6).addDetail("other", other).log();
    return (other instanceof DataWithQFLogMtd);
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") int hashCode() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "hashCode()", 6).log();
    final int PRIME = 59;
    int result = 1;
    final java.lang.Object $s = this.getS();
    result = ((result * PRIME) + (($s == null) ? 43 : $s.hashCode()));
    return result;
  }
  public @java.lang.Override @java.lang.SuppressWarnings("all") java.lang.String toString() {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "toString()", 6).log();
    return (("DataWithQFLogMtd(s=" + this.getS()) + ")");
  }
  public @java.lang.SuppressWarnings("all") DataWithQFLogMtd(final @lombok.NonNull String s) {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "DataWithQFLogMtd(String)", 6).addDetail("s", s).log();
    if ((s == null))
        {
          throw new java.lang.NullPointerException("s is marked non-null but is null");
        }
    this.s = s;
  }
}

