package de.qfs.bla;
import lombok.extern.qfs.QFLog;
class LoggerQFLogEnclosingWithoutQFLog {
  @QFLog class InnerLoggerQFLogWithoutLogger {
    InnerLoggerQFLogWithoutLogger() {
      super();
      if ((logger.level >= de.qfs.lib.log.Log.MTD))
          logger.lvlBuild(de.qfs.lib.log.Log.MTD, "InnerLoggerQFLogWithoutLogger.InnerLoggerQFLogWithoutLogger()", 55).log();
    }
    public void inFct() {
      if ((logger.level >= de.qfs.lib.log.Log.MTD))
          logger.lvlBuild(de.qfs.lib.log.Log.MTD, "InnerLoggerQFLogWithoutLogger.inFct()", 56).log();
      if ((logger.level >= de.qfs.lib.log.Log.WRN))
          logger.lvlBuild(de.qfs.lib.log.Log.WRN, "InnerLoggerQFLogWithoutLogger.inFct()", 57).add("log statement").log();
    }
  }
  private static de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("ManualSetOwnerName");
  <clinit>() {
  }
  LoggerQFLogEnclosingWithoutQFLog() {
    super();
  }
  public void someFunc(int[] is) {
    @QFLog(ownLogger = true) class LoggerQFLogInFct {
      private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithoutQFLog.someFunc(int[]).LoggerQFLogInFct");
      LoggerQFLogInFct() {
        super();
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogInFct()", 9).log();
      }
      public void inFct() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "inFct()", 10).log();
        if ((logger.level >= de.qfs.lib.log.Log.ERR))
            logger.lvlBuild(de.qfs.lib.log.Log.ERR, "inFct()", 11).add("log here again").log();
      }
    }
    @QFLog class LoggerQFLogInFctWithoutLogger {
      LoggerQFLogInFctWithoutLogger() {
        super();
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).LoggerQFLogInFctWithoutLogger.LoggerQFLogInFctWithoutLogger()", 16).log();
      }
      public void inFct() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).LoggerQFLogInFctWithoutLogger.inFct()", 17).log();
        if ((logger.level >= de.qfs.lib.log.Log.WRN))
            logger.lvlBuild(de.qfs.lib.log.Log.WRN, "someFunc(int[]).LoggerQFLogInFctWithoutLogger.inFct()", 18).add("log statement").log();
      }
    }
    Object o = new RuntimeException("xx") {
      static final long serialVersionUID = 42L;
      x(<no type> $anonymous0) {
        super($anonymous0);
      }
      <clinit>() {
      }
      public @QFLog String toString() {
        if ((logger.level >= de.qfs.lib.log.Log.MTD))
            logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).RuntimeException.toString()", 25).log();
        return "blaXX";
      }
    };
    System.out.println(o);
    Object p = new Object() {
      @QFLog class AnonLoggerQFLogInFctWithoutLogger {
        AnonLoggerQFLogInFctWithoutLogger() {
          super();
          if ((logger.level >= de.qfs.lib.log.Log.MTD))
              logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).Object.AnonLoggerQFLogInFctWithoutLogger.AnonLoggerQFLogInFctWithoutLogger()", 33).log();
        }
        public String blubb(char c) {
          if ((logger.level >= de.qfs.lib.log.Log.MTD))
              logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).Object.AnonLoggerQFLogInFctWithoutLogger.blubb(char)", 34).addDetail("c", c).log();
          return "blubb";
        }
      }
      x() {
        super();
      }
      public void newMethod() {
        Object p = new Object() {
          @QFLog(ownLogger = true) class DeeplyEmbeddedClassWithLogger {
            private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithoutQFLog.someFunc(int[]).Object.newMethod().Object.DeeplyEmbeddedClassWithLogger");
            DeeplyEmbeddedClassWithLogger() {
              super();
              if ((logger.level >= de.qfs.lib.log.Log.MTD))
                  logger.lvlBuild(de.qfs.lib.log.Log.MTD, "DeeplyEmbeddedClassWithLogger()", 46).log();
            }
          }
          x() {
            super();
          }
          public @QFLog int getI() {
            if ((logger.level >= de.qfs.lib.log.Log.MTD))
                logger.lvlBuild(de.qfs.lib.log.Log.MTD, "someFunc(int[]).Object.newMethod().Object.getI()", 41).log();
            return 42;
          }
        };
      }
    };
  }
}
