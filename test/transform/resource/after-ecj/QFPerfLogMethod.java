import lombok.extern.qfs.PerfLog;
class Parent {
  Parent(String s) {
    super();
  }
}
class PerfTest extends Parent {
  public static final String LABEL = "abc";
  <clinit>() {
  }
  @PerfLog("Constructor") PerfTest() {
    super("Child");
    final java.lang.String $performanceLogTagName = "Constructor";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println("Const");
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  @PerfLog void measuredMethod() {
    final java.lang.String $performanceLogTagName = "";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  @PerfLog(LABEL) void measuredMethod2() {
    final java.lang.String $performanceLogTagName = LABEL;
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  @PerfLog(value = "123") void measuredMethod3() {
    final java.lang.String $performanceLogTagName = "123";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
}