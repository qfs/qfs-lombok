package de.qfs.bla;
import lombok.extern.qfs.QFNotify;
import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;
@QFNotify class QFNotifyHandling {
  private static class User {
    public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Blubber");
    public static final int POST_LEVEL = Notifier.BASIC;
    public static final @NotificationName String TEST_NAME = "de.qfs.bla.Blubber.TestName";
    <clinit>() {
    }
    private User() {
      super();
    }
  }
  public QFNotifyHandling(int x) {
    super();
    if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
        de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In constructor");
  }
  public void simpleNotify() {
    String[] blubb = new String[0];
    int flag = 0;
    if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
        de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "schnitzel");
    if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
        de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "fqnSpecName");
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, "blubb", this, "detail", 2);
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, "bla");
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, User.TEST_NAME);
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, User.TEST_NAME, this, "more", true);
    System.out.println("Output!");
    if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
        de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In method");
    {
      if ((User.NOTIFIER.level >= User.POST_LEVEL))
          User.NOTIFIER.postNotification(User.POST_LEVEL, "In block");
    }
    do
      if ((User.NOTIFIER.level >= User.POST_LEVEL))
          User.NOTIFIER.postNotification(User.POST_LEVEL, "In do");
while (false);
    for (String s : blubb)
      if ((User.NOTIFIER.level >= User.POST_LEVEL))
          User.NOTIFIER.postNotification(User.POST_LEVEL, "In foreach");
    for (int i = 0;; (i < 1); i ++)
      if ((User.NOTIFIER.level >= User.POST_LEVEL))
          User.NOTIFIER.postNotification(User.POST_LEVEL, "In for");
    if ((flag == 0))
        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In if");
    else
        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In Else");
    if ((flag == 0))
        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In if without else");
    label: if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
    de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "Labeled Statement");
    while ((flag != 1))      if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
          de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In while");
    switch (flag) {
    case 1 :
        System.out.println("Output!");
        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In switch case");
        break;
    default :
        System.out.println("Output!");
        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In switch default");
        System.out.println("Output!");
        break;
    }
    int j = 0;
    all: do
  {
    if (true)
        for (String s : blubb)
          for (int i = 0;; (i < 1); i ++)
            while ((j < 2))              {
                switch (flag) {
                case 1 :
                    if ((j == 0))
                        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
                            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "in all true");
                    else
                        if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
                            de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "in all false");
                }
                j ++;
              }
  }
while ((j < 1));
  }
}