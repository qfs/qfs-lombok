import lombok.extern.qfs.PerfLog;
interface QFPrefLogInInterface {
  public static final String LABEL = "abc";
  <clinit>() {
  }
  default @PerfLog void measuredMethod() {
    final java.lang.String $performanceLogTagName = "";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  default @PerfLog(LABEL) void measuredMethod2() {
    final java.lang.String $performanceLogTagName = LABEL;
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
  default @PerfLog(value = "123") void measuredMethod3() {
    final java.lang.String $performanceLogTagName = "123";
    try
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod($performanceLogTagName);
        System.out.println();
      }
    finally
      {
        de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod($performanceLogTagName);
      }
  }
}