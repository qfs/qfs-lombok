package de.qfs.bla;
import lombok.extern.qfs.QFNotify;
import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;
@QFNotify interface QFNotifyHandling {
  public static class User {
    public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Blubber");
    public static final int POST_LEVEL = Notifier.BASIC;
    public static final @NotificationName String TEST_NAME = "de.qfs.bla.Blubber.TestName";
    <clinit>() {
    }
    public User() {
      super();
    }
  }
  default void simpleNotify() {
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, "blubb", this, "detail", 2);
    if ((User.NOTIFIER.level >= User.POST_LEVEL))
        User.NOTIFIER.postNotification(User.POST_LEVEL, User.TEST_NAME, this, "more", true);
    System.out.println("Output!");
    if ((de.qfs.lib.notifications.Notifier.Default.NOTIFIER.level >= de.qfs.lib.notifications.Notifier.Default.POST_LEVEL))
        de.qfs.lib.notifications.Notifier.Default.NOTIFIER.postNotification(de.qfs.lib.notifications.Notifier.Default.POST_LEVEL, "In method");
  }
}