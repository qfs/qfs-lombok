package de.qfs.bla;
import lombok.extern.qfs.QFLog;
@QFLog(topic = "DifferentName") class LoggerQFLogWithDifferentName {
  private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("DifferentName");
  <clinit>() {
  }
  LoggerQFLogWithDifferentName() {
    super();
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "LoggerQFLogWithDifferentName()", 6).log();
  }
  public void test(float fluss) {
    if ((logger.level >= de.qfs.lib.log.Log.MTD))
        logger.lvlBuild(de.qfs.lib.log.Log.MTD, "test(float)", 8).addDetail("fluss", fluss).log();
  }
}
