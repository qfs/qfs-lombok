class Parent {
	Parent(String s) {
	}
}

class PerfTest extends Parent {
	public static final String LABEL = "abc";
	PerfTest() {
		super("Child");
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("Constructor");
			System.out.println("Const");
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("Constructor");
		}
	}

	void measuredMethod() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("");
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("");
		}
	}

	void measuredMethod2() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod(LABEL);
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod(LABEL);
		}
	}

	void measuredMethod3() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("123");
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("123");
		}
	}
}