package de.qfs.bla;

import de.qfs.lib.log.Log;

class ClassWithQFLogMtdAndSkipMethods {
    @java.lang.SuppressWarnings("all")
    private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.ClassWithQFLogMtdAndSkipMethods");
    String s;

    public void testSkip() {
    }

    public void testSkip(float f) {
        if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "testSkip(float)", 10).addDetail("f", f).log();
    }

    public void getA() {
    }

    public void getB() {
    }
}
