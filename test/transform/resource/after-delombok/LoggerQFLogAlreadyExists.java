package de.qfs.bla;

import de.qfs.lib.log.Log;
import de.qfs.lib.log.QFLogger;

class LoggerQFLogAlreadyExists {
	public static final QFLogger logger = new QFLogger("my special logger");

	public void test(float fluss) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(float)", 11).addDetail("fluss", fluss).log();
	}
}
