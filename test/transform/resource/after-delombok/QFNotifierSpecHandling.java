package de.qfs.bla;

import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;
import lombok.extern.qfs.QFNotifierSpec;

class QFNotifierHandlingConfigured {
	public static final Notifier NOTIFIER = Notifier.instance("de.xyz.abc.Bla");
	public static final int POST_LEVEL = Notifier.DEBUG;
}

class QFNotifierHandlingSimpleValue {
	public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Schnitzel");
	public static final int POST_LEVEL = Notifier.FUNDAMENTAL;
}

class QFNotifierHandlingOnlyLevel {
	public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.QFNotifierHandlingOnlyLevel");
	public static final int POST_LEVEL = Notifier.BASIC_DETAILS;
}

class QFNotifierHandlingNames {
	public static final Notifier NOTIFIER = Notifier.instance("names");
	public static final int POST_LEVEL = Notifier.FUNDAMENTAL;
	@NotificationName
	public static final String BLA_BLA_BLA0 = "names.bla!?!bla.bla0";
	@NotificationName
	public static final String _BLUBBER_BLA = "names.1BlubberBla";
}

class QFNotifierHandlingAuto {
	public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.QFNotifierHandlingAuto");
	public static final int POST_LEVEL = Notifier.FUNDAMENTAL;


	static class InnerClass {
		public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.QFNotifierHandlingAuto.InnerClass");
		public static final int POST_LEVEL = Notifier.FUNDAMENTAL;
		@NotificationName
		public static final String VALUE_1 = "de.qfs.bla.QFNotifierHandlingAuto.InnerClass.Value 1";
		@NotificationName
		public static final String VALUE_2 = "de.qfs.bla.QFNotifierHandlingAuto.InnerClass.Value 2";
	}
}

class QFNotifierHandlingWithPredefinedNotifier {
	public static final int POST_LEVEL = Notifier.FUNDAMENTAL;
	public static final de.qfs.lib.notifications.Notifier NOTIFIER = null;
}

class QFNotifierHandlingWithPredefinedPostLevel {
	public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.QFNotifierHandlingWithPredefinedPostLevel");
	public static final int POST_LEVEL = 10;
}