package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLogCustomLogger {
	@java.lang.SuppressWarnings("all")
	private static final CustomLogger logger = new CustomLogger("de.qfs.bla.LoggerQFLogCustomLogger");

	public void test(float fluss) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(float)", 8).addDetail("fluss", fluss).log();
	}
}

class CustomLogger extends de.qfs.lib.log.QFLogger {
	public CustomLogger(String topic) {
		super(topic);
	}
}