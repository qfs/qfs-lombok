package de.qfs.bla;

import static de.qfs.lib.notifications.Notifier.Default;
import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;

interface QFNotifyHandling {

	class User {
		public static final Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Blubber");
		public static final int POST_LEVEL = Notifier.BASIC;
		@NotificationName
		public static final String TEST_NAME = "de.qfs.bla.Blubber.TestName";
	}

	default void simpleNotify() {
		if (User.NOTIFIER.level >= User.POST_LEVEL) User.NOTIFIER.postNotification(User.POST_LEVEL, "blubb", this, "detail", 2);
		if (User.NOTIFIER.level >= User.POST_LEVEL) User.NOTIFIER.postNotification(User.POST_LEVEL, User.TEST_NAME, this, "more", true);
		System.out.println("Output!");
		if (Default.NOTIFIER.level >= Default.POST_LEVEL) Default.NOTIFIER.postNotification(Default.POST_LEVEL, "In method");
	}
}