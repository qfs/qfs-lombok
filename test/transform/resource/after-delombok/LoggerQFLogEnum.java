package de.qfs.bla;

import de.qfs.lib.log.Log;

enum LoggerQFLogEnum {
	FIRST, SECOND;
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnum");

	static {
		if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "static", 10).add("Should work").log();
	}
}