class Parent {
	Parent(String s) {
	}
}

class PerfTest extends Parent {
	PerfTest() {
		super("Child");
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().reset();
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("Constructor");
			System.out.println("Const");
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("Constructor");
		}
	}

	void measuredMethod() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().reset();
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("");
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("");
		}
	}
}