package de.qfs.bla;

import de.qfs.lib.log.Log;

class DataWithQFLogMtdAndSkipMethods {
    @java.lang.SuppressWarnings("all")
    private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.DataWithQFLogMtdAndSkipMethods");
    String i;

    @java.lang.SuppressWarnings("all")
    public DataWithQFLogMtdAndSkipMethods() {
        if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "DataWithQFLogMtdAndSkipMethods()", 6).log();
    }

    @java.lang.SuppressWarnings("all")
    public String getI() {
        return this.i;
    }

    @java.lang.SuppressWarnings("all")
    public void setI(final String i) {
        if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "setI(String)", 6).addDetail("i", i).log();
        this.i = i;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("all")
    public boolean equals(final java.lang.Object o) {
        if (o == this) return true;
        if (!(o instanceof DataWithQFLogMtdAndSkipMethods)) return false;
        final DataWithQFLogMtdAndSkipMethods other = (DataWithQFLogMtdAndSkipMethods) o;
        if (!other.canEqual((java.lang.Object) this)) return false;
        final java.lang.Object this$i = this.getI();
        final java.lang.Object other$i = other.getI();
        if (this$i == null ? other$i != null : !this$i.equals(other$i)) return false;
        return true;
    }

    @java.lang.SuppressWarnings("all")
    protected boolean canEqual(final java.lang.Object other) {
        return other instanceof DataWithQFLogMtdAndSkipMethods;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("all")
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final java.lang.Object $i = this.getI();
        result = result * PRIME + ($i == null ? 43 : $i.hashCode());
        return result;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("all")
    public java.lang.String toString() {
        return "DataWithQFLogMtdAndSkipMethods(i=" + this.getI() + ")";
    }
}
