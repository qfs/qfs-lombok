package de.qfs.bla;

import de.qfs.lib.log.Log;

class DataWithQFLogMtd {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.DataWithQFLogMtd");
	@lombok.NonNull
	String s;

	@java.lang.SuppressWarnings("all")
	public DataWithQFLogMtd(@lombok.NonNull final String s) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "DataWithQFLogMtd(String)", 6).addDetail("s", s).log();
		if (s == null) {
			throw new java.lang.NullPointerException("s is marked non-null but is null");
		}
		this.s = s;
	}

	@lombok.NonNull
	@java.lang.SuppressWarnings("all")
	public String getS() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "getS()", 9).log();
		return this.s;
	}

	@java.lang.SuppressWarnings("all")
	public void setS(@lombok.NonNull final String s) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "setS(String)", 6).addDetail("s", s).log();
		if (s == null) {
			throw new java.lang.NullPointerException("s is marked non-null but is null");
		}
		this.s = s;
	}

	@java.lang.Override
	@java.lang.SuppressWarnings("all")
	public boolean equals(final java.lang.Object o) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "equals(Object)", 6).addDetail("o", o).log();
		if (o == this) return true;
		if (!(o instanceof DataWithQFLogMtd)) return false;
		final DataWithQFLogMtd other = (DataWithQFLogMtd) o;
		if (!other.canEqual((java.lang.Object) this)) return false;
		final java.lang.Object this$s = this.getS();
		final java.lang.Object other$s = other.getS();
		if (this$s == null ? other$s != null : !this$s.equals(other$s)) return false;
		return true;
	}

	@java.lang.SuppressWarnings("all")
	protected boolean canEqual(final java.lang.Object other) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "canEqual(Object)", 6).addDetail("other", other).log();
		return other instanceof DataWithQFLogMtd;
	}

	@java.lang.Override
	@java.lang.SuppressWarnings("all")
	public int hashCode() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "hashCode()", 6).log();
		final int PRIME = 59;
		int result = 1;
		final java.lang.Object $s = this.getS();
		result = result * PRIME + ($s == null ? 43 : $s.hashCode());
		return result;
	}

	@java.lang.Override
	@java.lang.SuppressWarnings("all")
	public java.lang.String toString() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "toString()", 6).log();
		return "DataWithQFLogMtd(s=" + this.getS() + ")";
	}
}

