interface QFPrefLogInInterface {
	String LABEL = "abc";

	default void measuredMethod() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("");
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("");
		}
	}

	default void measuredMethod2() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod(LABEL);
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod(LABEL);
		}
	}

	default void measuredMethod3() {
		try {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().startPeriod("123");
			System.out.println();
		} finally {
			de.qfs.apps.qftest.shared.PerformanceLog.instance().endPeriod("123");
		}
	}
}