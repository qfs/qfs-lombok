package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLogWithDifferentName {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("DifferentName");

	public void test(float fluss) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(float)", 8).addDetail("fluss", fluss).log();
	}
}
