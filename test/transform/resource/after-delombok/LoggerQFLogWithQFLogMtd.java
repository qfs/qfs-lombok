package de.qfs.bla;

import de.qfs.lib.log.Log;

class ClassWithQFLogMtd {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.ClassWithQFLogMtd");

	public void test(String blubb, boolean flag) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String,boolean)", 7).addDetail("blubb", blubb).addDetail("flag", flag).log();
	}

	public Object test(String... args) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[])", 9).addDetail("args", args).log();

		class InnerClass {
			public void methodInInnerClass() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[]).InnerClass.methodInInnerClass()", 11).log();
			}
		}
		Object o = new Object() {
			public String toString() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[]).Object.toString()", 15).log();
				return "anonymousToString";
			}
			public void withOwnStatement() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[]).Object.withOwnStatement()", 20).log();
			}
		};
		System.out.println(o);
		if (Integer.valueOf(new Object() {
			public String toString() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[]).Object.toString()", 27).log();
				if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "test(String[]).Object.toString()", 28).add("pass").log();
				return "1";
			}
		}.toString()) == 1) {
			System.out.println("pass");
		}
		return new Object() {
			public void methodInReturnedObject(final int args) {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String[]).Object.methodInReturnedObject(int)", 37).addDetail("args", args).log();
			}
		};
	}

	public void methodShouldBeSkipped() {
	}

	public void methodWithNullCheck(@lombok.NonNull String s) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "methodWithNullCheck(String)", 46).addDetail("s", s).log();
		if (s == null) {
			throw new java.lang.NullPointerException("s is marked non-null but is null");
		}
	}

	public void skippedWithManualParams(@lombok.NonNull String str, int i) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "skippedWithManualParams(String,int)", 51).addDetail("str", str).addDetail("i", i).log();
		if (str == null) {
			throw new java.lang.NullPointerException("str is marked non-null but is null");
		}
		if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "skippedWithManualParams(String,int)", 52).add("Debug").addDetail("str", str).addDetail("i", i).log();
	}
    
	static {
		if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "static", 56).add("In static initializer").log();
	}
	
	public static class ClassWithSkipMtd {
		public void mtdShouldBeSkipped() {
		}

		public void mtdShouldAlsoBeSkipped() {
		}
	}
}
