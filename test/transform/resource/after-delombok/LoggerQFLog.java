package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLog {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLog");

	public LoggerQFLog(int x) {
		super();
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "LoggerQFLog(int)", 8).addDetail("x", x).log();
		if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "LoggerQFLog(int)", 10).add("In constructor").log();
	}

	public LoggerQFLog() {
		this("bla");
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "LoggerQFLog()", 14).log();
	}

	public LoggerQFLog(String blubb) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "LoggerQFLog(String)", 19).addDetail("blubb", blubb).log();
	}

	public void test(String blubb, boolean flag) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "test(String,boolean)", 23).addDetail("blubb", blubb).addDetail("flag", flag).log();
	}

	protected final int methodWithMeat() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "methodWithMeat()", 27).log();
		System.out.println("In methodWithMeat.");
		return 0;
	}

	public void loggerWithDetails() {
		int loglevel = 1;
		{
			final int $level = loglevel;
			if (logger.level >= $level) logger.lvlBuild($level, "loggerWithDetails()", 35).add("blubb").addDetail("this is a detail").add("normal").addDetail("detail 2").log();
		}
		if (logger.level >= de.qfs.lib.log.Log.MSG) logger.lvlBuild(de.qfs.lib.log.Log.MSG, "loggerWithDetails()", 36).addDetail("this is a detail").log();
		if (logger.level >= de.qfs.lib.log.Log.MSG) logger.lvlBuild(de.qfs.lib.log.Log.MSG, "loggerWithDetails()", 37).add("to dump").dumpStack();
		if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "loggerWithDetails()", 37).addAll("all-list", new Integer[] {1, 2, 3}).log();
	}

	public void loggerWrapping(int flag, String... blubb) {
		System.out.println("Output!");
		if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "loggerWrapping(int,String[])", 43).add("In method").addDetail("the flag", flag).add("-> math: ").add(22 / 11).add("blubb", blubb).add("42 + 23", 42 + 23).log();
		{
			if (logger.level >= Log.ERRDETAIL) logger.lvlBuild(Log.ERRDETAIL, "loggerWrapping(int,String[])", 45).add("In block").log();
		}
		do if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "loggerWrapping(int,String[])", 47).add("In do").log();
 while (false);
		for (String s : blubb) if (logger.level >= Log.WRNDETAIL) logger.lvlBuild(Log.WRNDETAIL, "loggerWrapping(int,String[])", 49).add("In foreach").log();
		for (int i = 0; i < 1; i++) if (logger.level >= Log.MSG) logger.lvlBuild(Log.MSG, "loggerWrapping(int,String[])", 51).add("In for").log();
		if (flag == 0) if (logger.level >= Log.MSGDETAIL) logger.lvlBuild(Log.MSGDETAIL, "loggerWrapping(int,String[])", 54).add("In if").log();
		 else if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "loggerWrapping(int,String[])", 56).add("In Else").log();
		if (flag == 0) if (logger.level >= Log.MTDDETAIL) logger.lvlBuild(Log.MTDDETAIL, "loggerWrapping(int,String[])", 59).add("In if without else").log();
		label:
		if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "loggerWrapping(int,String[])", 61).add("Labeled Statement").log();
		while (flag != 1) if (logger.level >= Log.DBGDETAIL) logger.lvlBuild(Log.DBGDETAIL, "loggerWrapping(int,String[])", 63).add("In while").log();
		switch (flag) {
		case 1: 
			System.out.println("Output!");
			if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "loggerWrapping(int,String[])", 68).add("In switch case").log();
			break;

		default: 
			System.out.println("Output!");
			if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "loggerWrapping(int,String[])", 72).add("In switch default").log();
			System.out.println("Output!");
			break;
		}
		int j = 0;
		all:
		do {
			if (true) for (String s : blubb) for (int i = 0; i < 1; i++) while (j < 2) {
				switch (flag) {
				case 1: 
					if (j == 0) if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "loggerWrapping(int,String[])", 86).add("in all true").log();
					 else if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "loggerWrapping(int,String[])", 88).add("in all false").log();
				}
				j++;
			}
		} while (j < 1);
	}
}
