package de.qfs.bla;

import de.qfs.lib.log.Log;

interface LoggerQFLogInterface {
	@java.lang.SuppressWarnings("all")
	de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogInterface");

	default int methodWithMeatViaAnnotation(String meat) {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "methodWithMeatViaAnnotation(String)", 8).addDetail("meat", meat).log();
		System.out.println(meat);
		return 0;
	}
	
	default int methodWithMeatViaQflog(String meat) {
		if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "methodWithMeatViaQflog(String)", 15).add("In methodWithMeatViaQflog.").log();
		System.out.println(meat);
		return 0;
	}
	
	default void withInner() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "withInner()", 20).log();
		new Runnable() {
			public void run() {
				if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "withInner().Runnable.run()", 24).log();
			}
		}.run();
	}
	
	class innerClass {
		@java.lang.SuppressWarnings("all")
		private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogInterface.innerClass");

		public static boolean innerClassMethod(boolean valueToReturn) {
			if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "innerClassMethod(boolean)", 33).log();
			return valueToReturn;
		}
	}

}
