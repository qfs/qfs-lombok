package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLogEnclosingWithoutQFLog {
	private static de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("ManualSetOwnerName");

	public void someFunc(int[] is) {

		class LoggerQFLogInFct {
			@java.lang.SuppressWarnings("all")
			private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithoutQFLog.someFunc(int[]).LoggerQFLogInFct");

			public void inFct() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "inFct()", 10).log();
				if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "inFct()", 11).add("log here again").log();
			}
		}

		class LoggerQFLogInFctWithoutLogger {
			public void inFct() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "someFunc(int[]).LoggerQFLogInFctWithoutLogger.inFct()", 17).log();
				if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "someFunc(int[]).LoggerQFLogInFctWithoutLogger.inFct()", 18).add("log statement").log();
			}
		}
		Object o = new RuntimeException("xx") {
			static final long serialVersionUID = 42L;
			public String toString() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "someFunc(int[]).RuntimeException.toString()", 25).log();
				return "blaXX";
			}
		};
		System.out.println(o);
		Object p = new Object() {

			class AnonLoggerQFLogInFctWithoutLogger {
				public String blubb(char c) {
					if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "someFunc(int[]).Object.AnonLoggerQFLogInFctWithoutLogger.blubb(char)", 34).addDetail("c", c).log();
					return "blubb";
				}
			}
			public void newMethod() {
				Object p = new Object() {
					public int getI() {
						if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "someFunc(int[]).Object.newMethod().Object.getI()", 41).log();
						return 42;
					}

					class DeeplyEmbeddedClassWithLogger {
						@java.lang.SuppressWarnings("all")
						private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithoutQFLog.someFunc(int[]).Object.newMethod().Object.DeeplyEmbeddedClassWithLogger");
					}
				};
			}
		};
	}

	class InnerLoggerQFLogWithoutLogger {
		public void inFct() {
			if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "InnerLoggerQFLogWithoutLogger.inFct()", 56).log();
			if (logger.level >= Log.WRN) logger.lvlBuild(Log.WRN, "InnerLoggerQFLogWithoutLogger.inFct()", 57).add("log statement").log();
		}
	}
}