package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLogEnclosing {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing");

    
	class LoggerQFLogInner {
		@java.lang.SuppressWarnings("all")
		private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.LoggerQFLogInner");
	}


	public static class LoggerQFLogStaticInner {
		@java.lang.SuppressWarnings("all")
		private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.LoggerQFLogStaticInner");
	}

	public void someFunc(int[] p) {

		class LoggerQFLogInFct {
			@java.lang.SuppressWarnings("all")
			private final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosing.someFunc(int[]).LoggerQFLogInFct");

			public void inFct() {
				if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "inFct()", 17).log();
				if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "inFct()", 18).add("log here").log();
			}
		}
	}
}
