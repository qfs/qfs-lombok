package de.qfs.bla;

import de.qfs.lib.log.Log;

class LoggerQFLogEnclosingWithQFLog {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogEnclosingWithQFLog");

	public void withInner() {
		if (logger.level >= Log.MTD) logger.lvlBuild(Log.MTD, "withInner()", 7).log();
		new Runnable() {
			public void run() {
				if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "withInner().Runnable.run()", 11).log();
			}
		}.run();
	}
}
