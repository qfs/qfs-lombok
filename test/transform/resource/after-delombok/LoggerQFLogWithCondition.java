package de.qfs.bla;

import de.qfs.lib.log.Log;
import de.qfs.lib.util.Misc;

class LoggerQFLogWithCondition {
	@java.lang.SuppressWarnings("all")
	private static final de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("de.qfs.bla.LoggerQFLogWithCondition");

	public LoggerQFLogWithCondition() {
		super();
		if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "LoggerQFLogWithCondition()", 12).add("Test").add(((((0 == hashCode() ? "Bad" : "Good: " + 1))))).log();
		if (logger.level >= Log.ERR) logger.lvlBuild(Log.ERR, "LoggerQFLogWithCondition()", 13).add("(0 == hashCode() ? \"Bad\" : \"Good: \" + 2)", (0 == hashCode() ? "Bad" : "Good: " + 2)).log();
		String val = "";
		if (logger.level >= Log.DBG) logger.lvlBuild(Log.DBG, "LoggerQFLogWithCondition()", 15).add("val: ").add((val == null ? null : val)).add(Misc.toHex(val.getBytes())).log();
	}
}