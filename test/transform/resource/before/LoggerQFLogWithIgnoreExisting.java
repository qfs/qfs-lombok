package de.qfs.bla;

import lombok.extern.qfs.QFLog;
import de.qfs.lib.log.QFLogger;

@QFLog(ignoreExistingLogger=true)
class LoggerQFLogWithIgnoreExisting {
	
	public static final QFLogger logger = new QFLogger("my special logger");
	
	public void test(float fluss) {
	}
}