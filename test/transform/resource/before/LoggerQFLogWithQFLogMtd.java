package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog
class ClassWithQFLogMtd {
    public void test(String blubb, boolean flag) {
    }
    public Object test(String... args) {
    	class InnerClass {
    		public void methodInInnerClass() {
    		}
    	}
    	Object o = new Object() {
    		public String toString() {
    			return "anonymousToString";
    		}
    		
    		@QFLog
    		public void withOwnStatement() {
    			
    		}
    	};
    	System.out.println(o);
    	
    	if (Integer.valueOf(new Object(){
            public String toString() {
                qflog(DBG,"pass");
                return "1";
            }
        }.toString()) == 1) {
            System.out.println("pass");
        }
    	
    	return new Object() {
    		@QFLog
            public void methodInReturnedObject(final int args) {
            }
        };
    }
    
    @QFLog(skipMtd=true)
    public void methodShouldBeSkipped() {
    }

    public void methodWithNullCheck(@lombok.NonNull String s) {
    }

    @QFLog(skipMtd=true)
    public void skippedWithManualParams(@lombok.NonNull String str, int i) {
    	qflog(MTD).params();
    	qflog(DBG,"Debug").params();
    }
	
	static {
		qflog(DBG,"In static initializer");
	}
	
	@QFLog(skipMtd=true)
    public static class ClassWithSkipMtd {
		public void mtdShouldBeSkipped() {
		}
		@QFLog
		public void mtdShouldAlsoBeSkipped() {
		}
	}
}