package de.qfs.bla;

import lombok.extern.qfs.QFNotify;
import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;

@QFNotify
interface QFNotifyHandling {

	public static class User {
		public final static Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Blubber");
		public final static int POST_LEVEL = Notifier.BASIC;
		@NotificationName public final static String TEST_NAME = "de.qfs.bla.Blubber.TestName";
	}

	default void simpleNotify() {
		qfnotify(User,"blubb",this,"detail",2);
		qfnotify(User.TEST_NAME,this,"more",true);
		
		System.out.println("Output!");
		qfnotify("In method");
	}
}