package de.qfs.bla;

import lombok.extern.qfs.QFLog;

class LoggerQFLogEnclosingWithoutQFLog {
	private static de.qfs.lib.log.QFLogger logger = new de.qfs.lib.log.QFLogger("ManualSetOwnerName"); 
	public void someFunc(int[] is) {
		@QFLog(ownLogger = true)
		class LoggerQFLogInFct {
			public void inFct() {
				qflog(ERR,"log here again");
			}
		}
		
		@QFLog
		class LoggerQFLogInFctWithoutLogger {
			public void inFct() {
				qflog(WRN,"log statement");
			}
		}
		
		Object o = new RuntimeException("xx") {
			static final long serialVersionUID = 42L;
			@QFLog
			public String toString() {
				return "blaXX";
			}
		};
		System.out.println(o);

		Object p = new Object() {
			@QFLog
			class AnonLoggerQFLogInFctWithoutLogger {
				public String blubb(char c) {
					return "blubb";
				}
			}
			public void newMethod() {
				Object p = new Object() {
					@QFLog
					public int getI() {
						return 42;
					}
					
					@QFLog(ownLogger = true)
					class DeeplyEmbeddedClassWithLogger {
						
					}
				};
			}
		};
	}
	
	@QFLog
	class InnerLoggerQFLogWithoutLogger {
		public void inFct() {
			qflog(WRN,"log statement");
		}
	}
}