package de.qfs.bla;

import lombok.extern.qfs.QFNotifierSpec;

@QFNotifierSpec(value="de.xyz.abc.Bla", level=de.qfs.lib.notifications.Notifier.DEBUG)
class QFNotifierHandlingConfigured {
}

@QFNotifierSpec("de.qfs.bla.Schnitzel")
class QFNotifierHandlingSimpleValue {
}

@QFNotifierSpec(level=4)
class QFNotifierHandlingOnlyLevel {
}

@QFNotifierSpec(value="names", names={"bla!?!bla.bla0","1BlubberBla","bla_bla_bla0"})
class QFNotifierHandlingNames {
}

@QFNotifierSpec
class QFNotifierHandlingAuto {
	@QFNotifierSpec(names={"Value 1","Value 2"})
	static class InnerClass {}
}

@QFNotifierSpec
class QFNotifierHandlingWithPredefinedNotifier {
	public static final de.qfs.lib.notifications.Notifier NOTIFIER = null;
}

@QFNotifierSpec
class QFNotifierHandlingWithPredefinedPostLevel {
	public static final int POST_LEVEL = 10;
}