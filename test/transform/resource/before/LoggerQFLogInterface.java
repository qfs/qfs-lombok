package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog
interface LoggerQFLogInterface {

	default int methodWithMeatViaAnnotation(String meat) {
		System.out.println(meat);
		return 0;
	}
  	
	@QFLog(skipMtd=true)
  	default int methodWithMeatViaQflog(String meat) {
  		qflog(WRN, "In methodWithMeatViaQflog.");
  		System.out.println(meat);
		return 0;
	}
	
	default void withInner() {
		new Runnable() {
			@QFLog(skipMtd=true)
			public void run() {
				qflog(DBG);
			}
		}.run();
	}
	
	@QFLog(ownLogger = true, skipMtd = true)
	public static class innerClass {
		@QFLog(skipMtd=true)
		public static boolean innerClassMethod(boolean valueToReturn) {
			qflog(ERR);
			return valueToReturn;
		}
	}
}