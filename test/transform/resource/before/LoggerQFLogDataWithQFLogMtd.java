package de.qfs.bla;

import lombok.extern.qfs.QFLog;
import lombok.Data;

@Data
@QFLog
class DataWithQFLogMtd {
	@lombok.NonNull String s;
}