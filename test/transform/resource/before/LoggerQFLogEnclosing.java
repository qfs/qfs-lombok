package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog(skipMtd = true)
class LoggerQFLogEnclosing {
	@QFLog(ownLogger = true, skipMtd = true)
	class LoggerQFLogInner {
	}
	@QFLog(ownLogger = true, skipMtd = true)
	public static class LoggerQFLogStaticInner {
	}
	
	public void someFunc(int[] p) {
		@QFLog(ownLogger = true, skipMtd = false)
		class LoggerQFLogInFct {
			public void inFct() {
				qflog(ERR,"log here");
			}
		}
	}
}