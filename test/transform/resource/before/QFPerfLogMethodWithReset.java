import lombok.extern.qfs.PerfLog;

class Parent {
	Parent(String s) {}
}

class PerfTest extends Parent {
	
	@PerfLog(value="Constructor", reset=true)
	PerfTest() {
		super("Child");
		System.out.println("Const");
	}
	
	@PerfLog(reset=true)
	void measuredMethod() {
		System.out.println();
	}
}