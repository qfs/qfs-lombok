package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog(skipMtdNames={"testSkip()","%get[A-Z]\\(\\)"})
class ClassWithQFLogMtdAndSkipMethods {
    String s;
    public void testSkip() {
    }
    public void testSkip(float f) {
    }
    public void getA() {
    }
    public void getB() {
    }
}