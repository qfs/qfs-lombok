package de.qfs.bla;

import lombok.extern.qfs.QFNotify;
import de.qfs.lib.notifications.NotificationName;
import de.qfs.lib.notifications.Notifier;

@QFNotify
class QFNotifyHandling {

	private static class User {
		public final static Notifier NOTIFIER = Notifier.instance("de.qfs.bla.Blubber");
		public final static int POST_LEVEL = Notifier.BASIC;
		@NotificationName public final static String TEST_NAME = "de.qfs.bla.Blubber.TestName";
	}

	public QFNotifyHandling(int x) {
		qfnotify(Default,"In constructor");
	}

	public void simpleNotify() {
		String[] blubb = new String[0];
		int flag = 0; 
		qfnotify(Default,"schnitzel");
		qfnotify(de.qfs.lib.notifications.Notifier.Default,"fqnSpecName");
		qfnotify(User,"blubb",this,"detail",2);
		qfnotify(User,"bla");
		qfnotify(User.TEST_NAME);
		qfnotify(User.TEST_NAME,this,"more",true);
		
		System.out.println("Output!");
		qfnotify("In method");
		
		{ qfnotify(User,"In block"); }
		
		do qfnotify(User,"In do"); while (false);
		
		for (String s: blubb) qfnotify(User,"In foreach");
		
		for (int i=0; i < 1; i++) qfnotify(User,"In for");
		
		if (flag == 0) 
			qfnotify("In if");
		else 
			qfnotify("In Else");

		if (flag == 0) 
			qfnotify("In if without else");
		
		label: qfnotify("Labeled Statement");

		while (flag != 1) qfnotify("In while");
		
		switch (flag) {
		case 1:
			System.out.println("Output!");
			qfnotify("In switch case");
			break;
		default:
			System.out.println("Output!");
			qfnotify("In switch default");
			System.out.println("Output!");
			break;
		}
		
		int j = 0;
		all: do {
			if (true) 
				for (String s: blubb) 
					for (int i=0; i < 1; i++)
						while (j < 2) {
							switch (flag) {
							case 1:
								if (j == 0) 
									qfnotify("in all true"); 
								else 
									qfnotify("in all false"); 
							}
							j++;
						}
		}
		while(j < 1);
	}
}