package de.qfs.bla;

import lombok.extern.qfs.QFLog;

class LoggerQFLog {

	@lombok.extern.qfs.QFLog
	public LoggerQFLog(int x) {
		super();
		qflog(DBG,"In constructor");
	}

	@lombok.extern.qfs.QFLog
	public LoggerQFLog() {
		this("bla");
	}

	@lombok.extern.qfs.QFLog
	public LoggerQFLog(String blubb) {
	}

	@lombok.extern.qfs.QFLog
	public void test(String blubb, boolean flag) {
	}

	@lombok.extern.qfs.QFLog
	protected final int methodWithMeat() {
		System.out.println("In methodWithMeat.");
		return 0;
	}

	@lombok.extern.qfs.QFLog(skipMtd=true)
	public void loggerWithDetails() {
		int loglevel = 1;
		qflog(loglevel,"blubb").detail("this is a detail").log("normal").detail("detail 2");
		qflog(de.qfs.lib.log.Log.MSG).detail("this is a detail");
		qflog(de.qfs.lib.log.Log.MSG,"to dump").dump(); qflog(WRN).addAll("all-list", new Integer[]{1, 2, 3});
	}

	@lombok.extern.qfs.QFLog(skipMtd=true)
	public void loggerWrapping(int flag, String... blubb) {
		System.out.println("Output!");
		qflog(ERR,"In method").detail("the flag", flag).add("-> math: " + 22/11).add(blubb, 42+23);
		
		{ qflog(ERRDETAIL,"In block"); }
		
		do qflog(WRN,"In do"); while (false);
		
		for (String s: blubb) qflog(WRNDETAIL,"In foreach");
		
		for (int i=0; i < 1; i++) qflog(MSG,"In for");
		
		if (flag == 0) 
			qflog(MSGDETAIL,"In if");
		else 
			qflog(MTD,"In Else");

		if (flag == 0) 
			qflog(MTDDETAIL,"In if without else");
		
		label: qflog(DBG,"Labeled Statement");

		while (flag != 1) qflog(DBGDETAIL,"In while");
		
		switch (flag) {
		case 1:
			System.out.println("Output!");
			qflog(DBG, "In switch case");
			break;
		default:
			System.out.println("Output!");
			qflog(DBG, "In switch default");
			System.out.println("Output!");
			break;
		}
		
		int j = 0;
		all: do {
			if (true) 
				for (String s: blubb) 
					for (int i=0; i < 1; i++)
						while (j < 2) {
							switch (flag) {
							case 1:
								if (j == 0) 
									qflog(DBG,"in all true"); 
								else 
									qflog(DBG,"in all false"); 
							}
							j++;
						}
		}
		while(j < 1);
	}
}