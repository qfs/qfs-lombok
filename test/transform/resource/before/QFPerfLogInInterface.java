import lombok.extern.qfs.PerfLog;

interface QFPrefLogInInterface {
	
	public static final String LABEL = "abc";
	
	@PerfLog
	default void measuredMethod() {
		System.out.println();
	}

	@PerfLog(LABEL)
	default void measuredMethod2() {
		System.out.println();
	}

	@PerfLog(value="123")
	default void measuredMethod3() {
		System.out.println();
	}
}