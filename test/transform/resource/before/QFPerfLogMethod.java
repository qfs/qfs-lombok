import lombok.extern.qfs.PerfLog;

class Parent {
	Parent(String s) {}
}

class PerfTest extends Parent {
	
	public static final String LABEL = "abc";
	
	@PerfLog("Constructor")
	PerfTest() {
		super("Child");
		System.out.println("Const");
	}
	
	@PerfLog
	void measuredMethod() {
		System.out.println();
	}

	@PerfLog(LABEL)
	void measuredMethod2() {
		System.out.println();
	}

	@PerfLog(value="123")
	void measuredMethod3() {
		System.out.println();
	}
}