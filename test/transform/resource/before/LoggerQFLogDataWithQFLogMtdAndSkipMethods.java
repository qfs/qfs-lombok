package de.qfs.bla;

import lombok.extern.qfs.QFLog;
import lombok.Data;

@Data
@QFLog(skipMtdNames={"%get[A-Z][^\\(]*\\(\\)","hashCode()","equals(Object)","canEqual(Object)","toString()"})
class DataWithQFLogMtdAndSkipMethods {
    String i;
}