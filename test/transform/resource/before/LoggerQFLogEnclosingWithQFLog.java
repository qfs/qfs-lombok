package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog
class LoggerQFLogEnclosingWithQFLog {
	public void withInner() {
		new Runnable() {
			@QFLog(skipMtd=true)
			public void run() {
				qflog(DBG);
			}
		}.run();
	}
}