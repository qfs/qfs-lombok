package de.qfs.bla;

import lombok.extern.qfs.QFLog;

@QFLog(loggerClass=CustomLogger.class)
class LoggerQFLogCustomLogger {
	
	public void test(float fluss) {
	}
}

class CustomLogger extends de.qfs.lib.log.QFLogger {
	public CustomLogger(String topic) {
		super(topic);
	}
}