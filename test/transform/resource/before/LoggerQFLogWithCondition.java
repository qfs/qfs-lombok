package de.qfs.bla;

import de.qfs.lib.log.Log;
import de.qfs.lib.util.Misc;
import lombok.extern.qfs.QFLog;

class LoggerQFLogWithCondition {

	@lombok.extern.qfs.QFLog(skipMtd=true)
	public LoggerQFLogWithCondition() {
		super();
		qflog(ERR, "Test" + ((((0 == hashCode() ? "Bad" : "Good: " + 1)))));
		qflog(ERR, (0 == hashCode() ? "Bad" : "Good: " + 2));
		String val = "";
        qflog(DBG, "val: " +
                (val == null ? null : val) +
                Misc.toHex(val.getBytes()));
	}
}